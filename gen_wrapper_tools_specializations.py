# This script generates specializations for the classes in
# wrapper_tools.h. The maximum arity of the specializations to be
# generated should be provided as a command line argument:
#   python gen_wrapper_tools_specializations.py <max_arity>
# The default maximum arity is 9.

from sys import argv

max_arity = 9

def build_access(dim, var, var_is_array):
    if var_is_array:
        return '{0}[{1}]'.format(var, dim)
    else:
        return '{0}{1}'.format(var, dim)

def simple_dim(dim, var, var_is_array, rebasable=True):
    access = build_access(dim, var, var_is_array)
    baser = 'UPCXXA_ARRAY_BASE({0})' if rebasable else '- base[{0}]'
    return '({0} {1})'.format(access, baser.format(dim))

def unstrided_dim(dim, var, var_is_array, rebasable=True):
    simple = simple_dim(dim, var, var_is_array, rebasable)
    return '({0}*side_factors[{1}])'.format(simple, dim)

def strided_dim(dim, var, var_is_array):
    unstrided = unstrided_dim(dim, var, var_is_array, False)
    return 'UPCXXA_PFAST_DIVIDE({0}, stride[{1}])'.format(unstrided,
                                                          dim)

def gen_class_head(name, arity, stridedness):
    head = 'template<> struct {0}<{1}, {2}>'
    return head.format(name, arity, stridedness)

def gen_signature(name, arity, var, var_is_array):
    head = '  inline static size_t ' + name + '('
    indent = ' ' * len(head)
    args = 'const cint_t base[{0}],\n{1}' + \
           'const cint_t stride[{0}],\n{1}' + \
           'const cint_t side_factors[{0}],\n{1}' + \
           'const char *where'
    if var_is_array:
        args += ',\n{1}const cint_t ' + var + '[{0}]'
    else:
        for i in range(arity):
            args += ',\n{1}const cint_t ' + var + str(i)
    return head +  args.format(arity, indent) + ')'

def gen_class(cname, arity, stridedness, fname, var, var_is_array,
              gen_first, gen_middle, gen_last, gen_single):
    cls = gen_class_head(cname, arity, stridedness)
    sig = gen_signature(fname, arity, var, var_is_array)
    body = ' {\n    return\n      ((size_t)\n       '
    body += (gen_single if arity == 1
             else gen_first)(0, var, var_is_array)
    for i in range(1, arity-1):
        body += '\n       + ' + gen_middle(i, var, var_is_array)
    if arity != 1:
        body += '\n       + ' + gen_last(arity-1, var, var_is_array)
    body += ');\n  }'
    return cls + ' {\n' + sig + body + '\n};\n'

stridedness_map = {'strided' : (strided_dim, strided_dim,
                                strided_dim, strided_dim),
                   'unstrided' : (unstrided_dim, unstrided_dim,
                                  unstrided_dim, unstrided_dim),
                   'row' : (unstrided_dim, unstrided_dim,
                            simple_dim, simple_dim),
                   'column' : (simple_dim, unstrided_dim,
                               unstrided_dim, simple_dim)}

def gen_indexers(cname, fname, var, var_is_array):
    for i in range(1, max_arity+1):
        for s in ('strided', 'unstrided', 'row', 'column'):
            print(gen_class(cname, i, s, fname, var, var_is_array,
                            *stridedness_map[s]))

def gen_rebased_indexers():
    gen_indexers('rebased_indexer', 'compute', 'px', True)

def gen_var_indexers():
    gen_indexers('var_indexer_spec', 'index', 'i', False)

if __name__ == '__main__':
    if len(argv) > 1:
        max_arity = int(argv[1])
    gen_rebased_indexers()
    gen_var_indexers()
