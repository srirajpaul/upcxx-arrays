// This tests almost all operations on 2D points, and also some
// operations on 2D rectdomains.

#include "../tests.h"

static int testCount = 0;

static void printp1(point<1> p)
{
  print("[");
  print(p[1]);
  print("]");
}

static void printp2(point<2> p)
{
  print("[");
  print(p[1]);
  print(", ");
  print(p[2]);
  print("]");
}

static void printp3(point<3> p)
{
  print("[");
  print(p[1]);
  print(", ");
  print(p[2]);
  print(", ");
  print(p[3]);
  print("]");
}

static void printrd2(rectdomain<2> r)
{
  print("[");
  printp2(r.lwb());
  print(" : ");
  printp2(r.upb());
  if (r.stride() != POINT(1, 1)) {
    print(" : ");
    printp2(r.stride());
  }
  print("]");
}

static void test2(const char *testName, point<2> a, point<2> b)
{
  testCount++;
  if (a != b) {
    print("Failure on test ");
    print(testCount);
    print(" \"");
    print(testName);
    print("\": ");
    printp2(a);
    print(" != ");
    printp2(b);
    println("");
  }
}

static void testr2(const char *testName, rectdomain<2> a, rectdomain<2> b)
{
  testCount++;
  if (a.is_empty() && b.is_empty()) return;
  if (a != b) {
    print("Failure on test ");
    print(testCount);
    print(" \"");
    print(testName);
    print("\": ");
    printrd2(a);
    print(" != ");
    printrd2(b);
    println("");
  }
}

static void testIntersection_UnitStride(rectdomain<2> r,
                                        rectdomain<2> s)
{
  point<2> min = {(r.min()[1] < s.min()[1]) ? s.min()[1] : r.min()[1],
                  (r.min()[2] < s.min()[2]) ? s.min()[2] : r.min()[2]};
  point<2> max = {(r.max()[1] > s.max()[1]) ? s.max()[1] : r.max()[1],
                  (r.max()[2] > s.max()[2]) ? s.max()[2] : r.max()[2]};
  rectdomain<2> ans = rectdomain<2>(min, max + POINT(1, 1)); 
  if (!(r * s).is_empty()) {
    test2("rd<2> intersection lower bound", min, (r * s).min());
    test2("rd<2> intersection upper bound", max, (r * s).max());
    if (min != (r * s).min() || max != (r * s).max()) {
      print("r = ");
      printrd2(r);
      print(" s = ");
      printrd2(s);
      print("; min = ");
      printp2(min);
      print(" max = ");
      printp2(max);
      print("; r * s = ");
      printrd2(r * s);
      println("");
    }
  }
  testr2("intersection", ans, r * s);
}    

static void testSize(rectdomain<2> r)
{
  size_t i = 0;
  foreach (p, r) { i++; };
  if (i != r.size()) {
    print("Size test failed: ");
    printrd2(r);
    print(".size() => ");
    print(r.size());
    print(" (should be ");
    print(i);
    println(")");
  }
}

static void testContains(rectdomain<2> r, point<2> q)
{
  bool contains = false;
  foreach (p, r) {
    if (p == q) contains = true;
  };
  if (contains != r.contains(q)) println("Contains test failed!");
}

static void testSubset(rectdomain<2> r, rectdomain<2> s)
{
  if ((r <= s) != (r * s == r)) println("Subset test failed!");
  if ((s <= r) != (s * r == s)) println("Subset test failed!");
}

int main()
{
  point<2> p = {2, 3};
  rectdomain<2> r, empty = rectdomain<2>(POINT(2, 1), POINT(1, 1));

  // Test 2d points
  p = p + p;
  test2("+", p, POINT(4, 6));
  p = p - p;
  test2("-", p, POINT(0, 0));
  p = point<2>::all(1);
  test2("all", p, POINT(1, 1));
  p = point<2>::direction(1);
  test2("direction1", p, POINT(1, 0));
  p = point<2>::direction(1, 1);
  test2("direction2", p, POINT(1, 0));
  p = point<2>::direction(2, 2);
  test2("direction3", p, POINT(0, 2));
  p = point<2>::direction(-2, 2);
  test2("direction4", p, POINT(0, -2));
  p = p.all(8);
  test2("all", p, POINT(8, 8));
  p = POINT(-1, -2) / 2;
  test2("div", p, POINT(-1, -1));
  p = POINT(1, 4) / -2;
  test2("div2", p, POINT(-1, -2));
  p = POINT(2, 3) < POINT(3, 4) ? POINT(2, 3) : POINT(3, 4);
  test2("<", p, POINT(2, 3));
  p = POINT(2, 3) > POINT(3, 4) ? POINT(2, 3) : POINT(3, 4);
  test2(">", p, POINT(3, 4));
  p = point<2>::all(p.arity);
  test2("arity", p, POINT(2, 2));
  test2("+", POINT(1, 4) + POINT(5, 7), POINT(6, 11));
  test2("-", POINT(1, 4) - POINT(5, 9), POINT(-4, -5));
  test2("*", POINT(2, 11) * POINT(6, 2), POINT(12, 22));
  test2("/", POINT(2, -11) / POINT(6, 2), POINT(0, -6));
  test2("unary-", -POINT(2, -11), POINT(-2, 11));
  test2("unary-", -POINT(0, -4), POINT(0, 4));
  /*
    p = POINT(-1, -1);
    p = p.set(1, 8);
    test2("set", p, POINT(8, -1));
    p = p.set(2, 5);
    test2("set2", p, POINT(8, 5));
  */

  println("Finished testing 2d points.");

  r = rectdomain<2>(POINT(-3, -4), POINT(4, 9));
  test2("lwb", r.lwb(), POINT(-3, -4));
  test2("upb", r.upb(), POINT(4, 9));
  test2("min", r.min(), POINT(-3, -4));
  test2("max", r.max(), POINT(3, 8));
  p = point<2>::all(0);
  for (int i = -3; i < 4; i++)
    for (int j = -4; j < 9; j++)
      p = p + POINT(i, j);
  foreach (q, r) {
    p = p - q;
  };
  test2("foreach+/for-", p, POINT(0, 0));
    
  testSubset(r, r);
  testSubset(r, empty);
  testIntersection_UnitStride(r, r);
  testIntersection_UnitStride(r, empty);
  foreach (lo, rectdomain<2>(POINT(-5, -5), POINT(6, 11), POINT(2, 2))) {
    foreach (size, rectdomain<2>(POINT(0, 0), POINT(16, 16), POINT(2, 2))) {
      testIntersection_UnitStride(r, rectdomain<2>(lo, lo + size + POINT(1, 1)));
      testSize(rectdomain<2>(lo, lo + size + POINT(1, 1)));
      testSize(r * rectdomain<2>(lo, lo + size + POINT(1, 1)));
      testContains(r, lo);
      testSubset(r, rectdomain<2>(lo, lo + size + POINT(1, 1)));
    };
  };
    
  println("Finished testing 2d rectdomains.");

  return 0;
}
