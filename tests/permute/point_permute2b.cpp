// -------------------------------------------------------------------
//
// Name: *permute*.tst
//
// Version: 1.0
//
// Creator: miyamoto
//
// Date: 990209
//
// Purpose:
//   Test the permute method on domains, rectdomains and points.
//
// Change Log:
//
// -------------------------------------------------------------------

// Permute on points

#include "../tests.h"

int main() {
  print("(1,2) permuted (0,1) = ");
  println((POINT(1,2)).permute(POINT(0,1)));
  return 0;
}
