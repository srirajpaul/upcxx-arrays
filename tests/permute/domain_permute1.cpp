// -------------------------------------------------------------------
//
// Name: *permute*.tst
//
// Version: 1.0
//
// Creator: miyamoto
//
// Date: 990209
//
// Purpose:
//   Test the permute method on domains, rectdomains and points.
//
// Change Log:
//
// -------------------------------------------------------------------

// Permute on domains

#include "../tests.h"

int main() {
  println("rectdomain:");
  print((rectdomain<2>(POINT(1,2),POINT(21,31),POINT(1,2))) << " permuted " << (POINT(1,2)) << " = ");
  println((rectdomain<2>(POINT(1,2),POINT(21,31),POINT(1,2))).permute(POINT(1,2)));
  print((rectdomain<2>(POINT(1,2),POINT(21,31),POINT(1,2))) << " permuted " << (POINT(2,1)) << " = ");
  println((rectdomain<2>(POINT(1,2),POINT(21,31),POINT(1,2))).permute(POINT(2,1)));

  domain<2> d = rectdomain<2>(POINT(1,2),POINT(21,31),POINT(1,2));
  println("Domain:");
  print(d << " permuted " << POINT(1,2) << " = ");
  println(d.permute(POINT(1,2)));
  print(d << " permuted " << POINT(2,1) << " = ");
  println(d.permute(POINT(2,1)));

  return 0;
}
