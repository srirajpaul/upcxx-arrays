// -------------------------------------------------------------------
//
// Name: d*.tst
//
// Version: 1.0
//
// Creator: miyamoto
//
// Date: 980205
//
// Purpose:
//   Sanity test for domains. It will create the necessary files for
//   an upper level tester to create an executable, run it, and
//   compare the output to an expected output.
//
// Change Log:
//
// -------------------------------------------------------------------

// Rect Domain

#include "../tests.h"

int main() {
  rectdomain<3> rd3;
  rectdomain<2> rd2;
  rectdomain<1> rd1;
  point<3> p3;

  p3 = POINT(7,8,9);
  rd3 = rectdomain<3>(p3, POINT(10,13,16), POINT(1,2,3));
  rd2 = rd3.slice(2);
  foreach (p, rd2) {
    println("p = " << p);
  };
  println("---");
  rd1 = rd3.slice(2).slice(1);
  foreach (p, rd1) {
    println("p = " << p);
  };
  return 0;
}
