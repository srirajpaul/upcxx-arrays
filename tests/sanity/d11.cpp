// -------------------------------------------------------------------
//
// Name: d*.tst
//
// Version: 1.0
//
// Creator: kamil
//
// Date: 131003
//
// Purpose:
//   Sanity test for domains. Tests rectdomains with 0 strides.
//
// Change Log:
//
// -------------------------------------------------------------------

// Rect Domain

#include "../tests.h"

int main() {
  rectdomain<1> rd1 = rectdomain<1>(0, 2, 0);
  rectdomain<1> rd1b = rectdomain<1>(POINT(0), POINT(2), POINT(0));
  rectdomain<2> rd2 = rectdomain<2>(POINT(0, 0), POINT(2, 2), POINT(0, 1));

  print("rd1 = ");
  println(rd1);
  foreach (pt, rd1) {
    point<1> q;
    println("p = " << pt);
    q = pt;
  };

  print("rd1b = ");
  println(rd1b);
  foreach (pt, rd1b) {
    point<1> q;
    println("p = " << pt);
    q = pt;
  };

  print("rd2 = ");
  println(rd2);
  foreach (pt, rd2) {
    point<2> q;
    println("p = " << pt);
    q = pt;
  };

  return 0;
}
