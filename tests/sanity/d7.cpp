// -------------------------------------------------------------------
//
// Name: d*.tst
//
// Version: 1.0
//
// Creator: miyamoto
//
// Date: 980205
//
// Purpose:
//   Sanity test for domains. It will create the necessary files for
//   an upper level tester to create an executable, run it, and
//   compare the output to an expected output.
//
// Change Log:
//
// -------------------------------------------------------------------

// Rect Domain

#include "../tests.h"

int main() {
  rectdomain<3> rd;
  rectdomain<3> rd2;
  point<3> p_div;
  point<3> p_lwb;
  point<3> p_upb;
  point<3> p_stride;

  domain<3> x = (domain<3>)(rectdomain<3>(POINT(0, 0, 0), POINT(0, 0, 0)));

  rd = rectdomain<3>(POINT(4,3,3), POINT(5,5,5), POINT(0,1,1));
  p_div = POINT(2,2,2);
  rd = rd / p_div;
  p_lwb = rd.lwb();
  p_upb = rd.upb();
  p_stride = rd.stride();
  print("rd = ((");
  print(p_lwb[1]); print(",");
  print(p_lwb[2]); print(",");
  print(p_lwb[3]); print("),(");
  print(p_upb[1]); print(",");
  print(p_upb[2]); print(",");
  print(p_upb[3]); print("),(");
  print(p_stride[1]); print(",");
  print(p_stride[2]); print(",");
  print(p_stride[3]); print("))");
  println("");
  foreach (p, rd) {
    println("p = " << p);
  };
  println("-----");
  rd2 = rectdomain<3>(POINT(4,3,3), POINT(5,9,9), POINT(0,2,2)) / POINT(3,3,3);
  foreach (p, rd2) {
    println("p = " << p);
  };
  println("-----");
  rd2 = rectdomain<3>(POINT(4,0,0), POINT(5,9,9), POINT(0,4,4)) / POINT(2,2,2);
  foreach (p, rd2) {
    println("p = " << p);
  };

  return 0;
}
