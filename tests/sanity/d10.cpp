// -------------------------------------------------------------------
//
// Name: d*.tst
//
// Version: 1.0
//
// Creator: miyamoto
//
// Date: 980205
//
// Purpose:
//   Sanity test for domains. It will create the necessary files for
//   an upper level tester to create an executable, run it, and
//   compare the output to an expected output.
//
// Change Log:
//
// -------------------------------------------------------------------

#include <algorithm>
#include "../tests.h"

static void printDomain(domain<3> d) {
  println("Type: domain<3>");
  foreach (p, d) {
    println(p);
  };//end foreach
}

static void printDomain(rectdomain<3> rd) {
  println("Type rectdomain<3>");
  foreach (p, rd){
    println(p);
  };//end foreach
  //  printDomain((domain<3>) rd); //it's small enough not to need this...
}

int main() {
  point<3> lb = POINT(0,0,0);
  point<3> ub = point<3>::all(3);

  rectdomain<3> rd = rectdomain<3>(lb,ub);
  rectdomain<3> rd1;

  println("Points in rd");
  printDomain(rd);

  println("rd.accrete(3, 2, 1)");
  printDomain(rd.accrete(3,2,1));

  println("rd.accrete(3,point<3>.all(1))");
  printDomain(rd.accrete(3,point<3>::all(1)));

  println("rd.accrete(3, 1, 1)...");
  rd1 = rd.accrete(3, 1, 1);
  rd1 = rd1.accrete(3, -1, 1);
  rd1 = rd1.accrete(3, 2, 1);
  rd1 = rd1.accrete(3, -2, 1);
  rd1 = rd1.accrete(3, 3, 1);
  rd1 = rd1.accrete(3, -3, 1);
  printDomain(rd1);
  
  println("rd.shrink(1, 2)");
  printDomain(rd.shrink(1,2));

  println("rd.shrink(1)");
  printDomain(rd.shrink(1));

  return 0;
}//end main
