// -------------------------------------------------------------------
//
// Name: d*.tst
//
// Version: 1.0
//
// Creator: miyamoto
//
// Date: 980205
//
// Purpose:
//   Sanity test for domains. It will create the necessary files for
//   an upper level tester to create an executable, run it, and
//   compare the output to an expected output.
//
// Change Log:
//
// -------------------------------------------------------------------

#include "../tests.h"

int main() {
  rectdomain<1> R = rectdomain<1>(POINT(1), POINT(4));
  point<1> mip = R.min(), mxp = R.max();
  int mi = mip[1], mx = mxp[1];

  println("R = " << R);
  println(mi);
  println(mx);
  foreach (i, R) {
    println(i[1]);
  };

  return 0;
}
