// -------------------------------------------------------------------
//
// Name: d*.tst
//
// Version: 1.0
//
// Creator: miyamoto
//
// Date: 980205
//
// Purpose:
//   Sanity test for domains. It will create the necessary files for
//   an upper level tester to create an executable, run it, and
//   compare the output to an expected output.
//
// Change Log:
//
// -------------------------------------------------------------------

// Rect Domain

#include "../tests.h"

int main() {
  domain<3> d1;
  domain<3> d2;
  domain<3> d3;
  rectdomain<3> r;

  d1 = rectdomain<3>(POINT(4,4,4), POINT(7,7,7)) +
    rectdomain<3>(POINT(6,6,6), POINT(9,9,9));
  // d2 = rectdomain<3>(POINT(3,5,6),POINT(7,9,10)) + rectdomain<3>(POINT(5,7,8),POINT(9,11,12));
  d2 = rectdomain<3>(POINT(4,4,4), POINT(8,8,8)) +
    rectdomain<3>(POINT(5,5,5), POINT(9,9,9));

  d3 = d1 * d2;
  d3 = d3 * d2;
  d3 = d3 * d1;
  foreach (p, d3) {
    println("p = " << p);
  };
  println("-----");
  r = rectdomain<3>(POINT(5,5,4), POINT(6,7,5), POINT(0,1,0)) *
    rectdomain<3>(POINT(5,4,4), POINT(8,5,8), POINT(1,0,1));
  foreach (p, r) {
    println("p = " << p);
  };

  return 0;
}
