// -------------------------------------------------------------------
//
// Name: literals.tst
//
// Version: 1.0
//
// Creator: liblit
//
// Date: 981001
//
// Purpose:
//   Sanity test for domain literal syntaxes. It will create the
//   necessary files for an upper level tester to create an
//   executable, run it, and compare the output to an expected output.
//
// Change Log:
//
// -------------------------------------------------------------------

#include "../tests.h"

static void test_rd_macro() {
  println(RD(1, 102, 10));
  println(RD(1, 102    ));
  println(RD(PT(1, 2, 3), PT(102, 203, 304), PT(10, 20, 30)));
  println(RD(PT(1, 2, 3), PT(102, 203, 304)              ));
}

int main() {
  println(POINT(1));
  println(POINT(1, 2, 3));
  test_rd_macro();
#if __cplusplus >= 201103L && defined(UPCXXA_USE_CXX11_INITIALIZER_LIST)
  rectdomain<1> rd1;
  rectdomain<3> rd3;
  rd1 = {{1}, {102}, {10}};
  println(rd1);
  rd1 = {{1}, {102}};
  println(rd1);
  rd3 = {{1, 2, 3}, {102, 203, 304}, {10, 20, 30}};
  println(rd3);
  rd3 = {{1, 2, 3}, {102, 203, 304}};
  println(rd3);
#else
  test_rd_macro();
#endif
  return 0;
}
