// -------------------------------------------------------------------
//
// Name: d*.tst
//
// Version: 1.0
//
// Creator: miyamoto
//
// Date: 980205
//
// Purpose:
//   Sanity test for domains. It will create the necessary files for
//   an upper level tester to create an executable, run it, and
//   compare the output to an expected output.
//
// Change Log:
//
// -------------------------------------------------------------------

// Rect Domain

#include "../tests.h"

int main() {
  rectdomain<3> rd1;
  rectdomain<3> rd2;
  domain<3> d1;
  domain<3> d2;

  rd1 = rectdomain<3>(POINT(7,8,9), POINT(10,13,16), POINT(1,2,3));
  rd2 = rectdomain<3>(POINT(7,8,9), POINT(6,7,8), POINT(1,2,3));
  d1 = rd1 - rd1;
  print("rd1.is_empty() = ");
  println_bool(rd1.is_empty());
  print("rd1.size = ");
  println(rd1.size());
  print("rd2.is_empty() = ");
  println_bool(rd2.is_empty());
  print("rd2.size = ");
  println(rd2.size());
  print("d1.is_empty() = ");
  println_bool(d1.is_empty());
  print("d1.size = ");
  println(d1.size());

  foreach (p, rd1) {
    print("p = " << p << " rd1.contains(p) = ");
    println_bool(rd1.contains(p));
  };

  d2 = (domain<3>) rd1;
  foreach (p, d2) {
    print("p = " << p << " d2.contains(p) = ");
    println_bool(d2.contains(p));
  };

  return 0;
}
