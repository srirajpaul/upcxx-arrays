// -------------------------------------------------------------------
//
// Name: d*.tst
//
// Version: 1.0
//
// Creator: miyamoto
//
// Date: 980205
//
// Purpose:
//   Sanity test for domains. It will create the necessary files for
//   an upper level tester to create an executable, run it, and
//   compare the output to an expected output.
//
// Change Log:
//
// -------------------------------------------------------------------

// Rect Domain

#include "../tests.h"

int main() {
  rectdomain<1> rdx1;
  rectdomain<3> rd, rd2, rd3;
  domain<3> x;
  point<3> p;
  point<3> p2;

  p = PT(7,7,7);
  p2 = p + PT(7,7,7);
  rdx1 = RD(7, 9);
  rd2 = RD(p, PT(10,10,10), PT(1,1,1));
  rd3 = RD(PT(10,1,1), PT(20,10,10), PT(1,1,1));
  x = rd2 + rd3;
  rd = rd3.accrete(5);
  p = point<3>::direction(1, 1);
  foreach (pt, x) {
    point<3> q;
    println("p = " << pt);
    q = pt;
  };

  return 0;
}
