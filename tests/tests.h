#include <iostream>
#include <string>
#ifdef INCLUDE_ARRAYS
# include "../array.h"
#else
# include "../domain.h"
#endif

#define print(item) cout << item
#define println(item) cout << item << endl
#define print_bool(b) print((b ? "true" : "false"))
#define println_bool(b) println((b ? "true" : "false"))

#define POINT upcxx::PT
#define foreach upcxx_foreach
#define foreachd(var, dom, dim)                         \
  foreachd_(var, (dom), dim, UPCXXA_CONCAT_(var, _upb), \
            UPCXXA_CONCAT_(var, _stride),               \
            UPCXXA_CONCAT_(var, _done))
#define foreachd_(var, dom, dim, u_, s_, d_)            \
  for (upcxx::cint_t u_ = (dom).upb()[dim],             \
	 s_ = (dom).raw_stride()[dim],                  \
	 var = (dom).lwb()[dim]; var < u_; var += s_)

using namespace std;
using namespace upcxx;

template<int N> bool rd_compare(const rectdomain<N> &rd1,
                                const rectdomain<N> &rd2) {
  return point<N>::compare(rd1.min(), rd2.min());
}
