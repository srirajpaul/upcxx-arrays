#!/bin/sh

./runtests.sh -DUPCXXA_BOUNDS_CHECKING=1 $*
./runtests.sh -DUPCXXA_BOUNDS_CHECKING=1 -O3 $*
./runtests.sh $*
./runtests.sh -O3 $*
./runtests.sh -DUPCXXA_BOUNDS_CHECKING=1 -DUPCXXA_USE_CXX11 -std=c++11 $*
./runtests.sh -DUPCXXA_BOUNDS_CHECKING=1 -DUPCXXA_USE_CXX11 -O3 -std=c++11 $*
./runtests.sh -DUPCXXA_USE_CXX11 -std=c++11 $*
./runtests.sh -DUPCXXA_USE_CXX11 -O3 -std=c++11 $*
