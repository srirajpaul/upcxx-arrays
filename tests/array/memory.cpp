/* Test array creation and destruction with custom de/allocators. */

#include <cstdlib>
#include "arraytests.h"

struct test {
  test() {
    println("called test constructor");
  }
  ~test() {
    println("called test destructor");
  }
};

void *my_malloc(size_t bytes) {
  println("called malloc");
  return malloc(bytes);
}

void my_free(void *mem) {
  println("called free");
  free(mem);
}

int main() {
  ndarray<int, 1> a1(RD(0, 5), my_malloc);
  a1.destroy(my_free);
  ndarray<int *, 1> a2(RD(0, 5), my_malloc);
  a2.destroy(my_free);
  ndarray<test, 1> a3(RD(0, 5), my_malloc);
  a3.destroy(my_free);
  ndarray<test, 2, global> a4(RD(PT(0, 0), PT(2, 2)), my_malloc);
  ndarray<test, 1, unstrided, global> a5 = a4.slice(1, 1);
  a5.destroy(my_free);
  return 0;
}
