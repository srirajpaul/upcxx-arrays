/* Test integer-index-based array slicing */
#include "arraytests.h"

#ifndef ArrayType
# define ArrayType ndarray
#endif

#if __cplusplus >= 201103L
// test passing raw initializer lists to array ops
# define POINTLIT(...) { __VA_ARGS__ }
#else
# define POINTLIT POINT
#endif

int main() {
  cout << "Running array operations tests.." << endl;

  /* some basic array copy tests */
  { /* contiguous copy with differing base */
    ArrayType<int, 1> x(RD(1, 101));
    ArrayType<int, 1> y(RD(50, 151));

    foreachd (p, x.domain(), 1) { x[p] = p; }
    foreachd (p, y.domain(), 1) { y[p] = p+1000; }
    x.copy(y);
    foreach (p, x.domain()) {
      int expected;
      if (p[1] < 50) expected = p[1];
      else expected = p[1]+1000;
      if (x[p] != expected) 
        cout << "Mismatch detected in copy test 1 at " << p <<
          ": expected: " << expected <<
          "  got: " << x[p] << endl;
    };
  }

  { /* contiguous copy with non-trivial stride */
    ArrayType<int, 1, strided> x(RD(10, 101, 10));
    ArrayType<int, 1, strided> y = ArrayType<int, 1>(RD(1, 11)).inject(POINTLIT(10));
    foreachd (p, x.domain(), 1) { x[p] = p; }
    foreachd (p, y.domain(), 1) { y[p] = p+1000; }
    x.copy(y);
    foreach (p, x.domain()) {
      int expected = p[1]+1000;
      if (x[p] != expected) 
        cout << "Mismatch detected in copy test 2 at " << p <<
          ": expected: " << expected <<
          "  got: " << x[p] << endl;
    };
  }

  { /* tranpose from contiguous -> contiguous */
    ArrayType<int, 2> x(RD(PT(1, 11), PT(6, 21)));
    ArrayType<int, 2, unstrided> y = ArrayType<int, 2>(RD(PT(11, 1), PT(21, 6))).permute(POINTLIT(2,1));
    foreachd (p, x.domain(), 1) {
      foreachd(q, x.domain(), 2) {
        x(p, q) = p*100+q;
      }
    }
    foreachd (p, y.domain(), 1) {
      foreachd(q, y.domain(), 2) {
        y(p, q) = p*100+q+1000;
      }
    }
    x.copy(y);
    foreach (p, x.domain()) {
      int expected = p[1]*100+p[2]+1000;
      if (x[p] != expected) 
        cout << "Mismatch detected in copy test 3 at " << p <<
          ": expected: " << expected <<
          "  got: " << x[p] << endl;
    };
  }

  { /* equal sideFactor/stride ratio */
    ArrayType<int, 2, strided> x(RD(PT(1, 11), PT(6, 21), PT(2, 1)));
    ArrayType<int, 2> y(RD(PT(1, 11), PT(6, 16)));
    foreachd (p, x.domain(), 1) {
      foreachd(q, x.domain(), 2) {
        x(p, q) = p*100+q;
      }
    }
    foreachd (p, y.domain(), 1) {
      foreachd(q, y.domain(), 2) {
        y(p, q) = p*100+q+1000;
      }
    }
    x.copy(y);
    foreach (p, x.domain()) {
      int expected;
      if (p[2] > 15) expected = p[1]*100+p[2];
      else expected = p[1]*100+p[2]+1000;
      if (x[p] != expected) 
        cout << "Mismatch detected in copy test 4 at " << p <<
          ": expected: " << expected <<
          "  got: " << x[p] << endl;
    };
  }

  {
    // test overlap (contiguous)
    ArrayType<int, 1> x(RD(0, 7));
    foreachd (p, x.domain(), 1) { x[p] = 100+p; }
    ArrayType<int, 1> y = x.translate(POINTLIT(2));
    x.copy(y);
    foreach (p, x.domain()) {
      int expected;
      if (p[1] < 2) expected = 100+p[1];
      else expected = 100+p[1]-2;
      if (x[p] != expected) 
        cout << "Mismatch detected in copy overlap test 1 at " << p <<
          ": expected: " << expected <<
          "  got: " << x[p] << endl;
    };
  }

  {
    // test overlap (non-contiguous)
    ArrayType<int, 1> x = ArrayType<int, 1>(RD(0, 61)).constrict(RD(0, 61, 10));
    foreachd (p, x.domain(), 1) { x[p] = 100+p; }
    ArrayType<int, 1> y = x.translate(POINTLIT(20));
    x.copy(y);
    foreach (p, x.domain()) {
      int expected;
      if (p[1] < 20) expected = 100+p[1];
      else expected = 100+p[1]-20;
      if (x[p] != expected) 
        cout << "Mismatch detected in copy overlap test 2 at " << p <<
          ": expected: " << expected <<
          "  got: " << x[p] << endl;
    };
  }

  cout << "done." << endl;
  return 0;
}
