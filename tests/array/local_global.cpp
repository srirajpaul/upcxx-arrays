// Test local-global operations.

#include "arraytests.h"

enum convertibility { conv_left, conv_right, conv_both };

template<class A, class B, convertibility C> struct iconv_tester {
  static void test(A &arrA1, B &arrB1) {
    A arrA2 = arrB1;
    println(arrA2[0]);
    B arrB2 = arrA1;
    println(arrB2[0]);
  }
};

template<class A, class B> struct iconv_tester<A, B, conv_left> {
  static void test(A &arrA1, B &arrB1) {
    A arrA2 = arrB1;
    println(arrA2[0]);
  }
};

template<class A, class B> struct iconv_tester<A, B, conv_right> {
  static void test(A &arrA1, B &arrB1) {
    B arrB2 = arrA1;
    println(arrB2[0]);
  }
};

template<class A, class B, convertibility C> void test_ops() {
  rectdomain<1> rd1(0, 10);
  rectdomain<1> rd2(0, 10, 2);
  domain<1> d = rd2 + RD(0, 5);
  A arrA1(rd1);
  B arrB1(rd1);

  foreach (p, rd1) {
    arrA1[p] = 100 + p[1];
    arrB1[p] = 200 + p[1];
  };

  // test implicit conversions
  println("testing implicit conversions...");
  iconv_tester<A, B, C>::test(arrA1, arrB1);

  // test explicit conversions
  println("testing explicit conversions...");
  {
    A arrA2 = (A) arrB1;
    B arrB2 = (B) arrA1;
    println(arrA2[0]);
    println(arrB2[0]);
  }

  // test copy operations
  println("testing copy operations...");
  {
    A arrA2(rd1);
    arrA2.copy(arrB1);
    foreach (p, rd1) {
      if (arrA2[p] != arrB1[p]) {
        println("error: mismatch at " << p << ", " << arrA2[p] <<
                " != " << arrB1[p]);
      }
    };
  }
  {
    A arrA2(rd1);
    arrA2.copy(arrB1, rd2);
    foreach (p, rd1) {
      if (rd2.contains(p) && arrA2[p] != arrB1[p]) {
        println("error: mismatch at " << p << ", " << arrA2[p] <<
                " != " << arrB1[p]);
      }
      if (!rd2.contains(p) && arrA2[p] == arrB1[p]) {
        println("error: unexpected match at " << p << ", " <<
                arrA2[p] << " == " << arrB1[p]);
      }
    };
  }
  {
    A arrA2(rd1);
    arrA2.copy(arrB1, d);
    foreach (p, rd1) {
      if (d.contains(p) && arrA2[p] != arrB1[p]) {
        println("error: mismatch at " << p << ", " << arrA2[p] <<
                " != " << arrB1[p]);
      }
      if (!d.contains(p) && arrA2[p] == arrB1[p]) {
        println("error: unexpected match at " << p << ", " <<
                arrA2[p] << " == " << arrB1[p]);
      }
    };
  }
  {
    A arrA2(rd1);
    ndarray<point<1>, 1> parr(RD(0, (int) d.size()));
    int idx = 0;
    foreach (p, d) {
      parr[idx++] = p;
    };
    arrA2.copy(arrB1, parr);
    foreach (p, rd1) {
      if (d.contains(p) && arrA2[p] != arrB1[p]) {
        println("error: mismatch at " << p << ", " << arrA2[p] <<
                " != " << arrB1[p]);
      }
      if (!d.contains(p) && arrA2[p] == arrB1[p]) {
        println("error: unexpected match at " << p << ", " <<
                arrA2[p] << " == " << arrB1[p]);
      }
    };
  }

  // test gather
  println("testing gather...");
  {
    ndarray<point<1>, 1> parr(RD(0, (int) d.size()));
    A arrA2(parr.domain());
    int idx = 0;
    int total = 0;
    foreach (p, d) {
      parr[idx++] = p;
      total += arrB1[p];
    };
    arrB1.gather(arrA2, parr);
    foreach (p, arrA2.domain()) {
      total -= arrA2[p];
    };
    if (total != 0) {
      println("error: failed gather, residual = " << total);
    }
  }

  // test scatter
  println("testing scatter...");
  {
    ndarray<point<1>, 1> parr(RD(0, (int) d.size()));
    A arrA2(parr.domain());
    int idx = 0;
    int total = 0;
    foreach (p, d) {
      arrA2[idx] = 300 + p[1];
      total += arrA2[idx];
      parr[idx++] = p;
    };
    arrB1.scatter(arrA2, parr);
    foreach (p, d) {
      total -= arrB1[p];
    };
    if (total != 0) {
      println("error: failed scatter, residual = " << total);
    }
  }
}

int main() {
  println("Testing local-local ops:");
  test_ops<ndarray<int, 1>,                 ndarray<int, 1>,  conv_both>();

  println("Testing local-global ops:");
  test_ops<ndarray<int, 1>,         ndarray<int, 1, global>,  conv_right>();

  println("Testing global-local ops:");
  test_ops<ndarray<int, 1, global>,         ndarray<int, 1>,  conv_left>();

  println("Testing global-global ops:");
  test_ops<ndarray<int, 1, global>, ndarray<int, 1, global>,  conv_both>();

  println("done.");

  return 0;
}
