// Test null comparisons on global arrays.
#if __cplusplus >= 201103L
# define ArrayType global_ndarray
#else
# define GLOBAL , global
#endif
#include "unstrided.cpp"
