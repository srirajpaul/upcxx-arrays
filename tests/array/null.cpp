// Test null comparisons.

#include "arraytests.h"
#define null NULL

#ifndef ArrayType
# define ArrayType ndarray
#endif
#ifndef GLOBAL
# define GLOBAL
#endif

int main() {
  ArrayType<int, 1 GLOBAL> x; // int [1d] x;
  ArrayType<double, 2 GLOBAL> y; // double [2d] local y;
  ArrayType<int, 1 GLOBAL> z; //int [1d] local z;

  if (x == null)
    println("x==null");
  if (y == null)
    println("y==null");
  if (z == null)
    println("z==null");
  if (x != null)
    println("x!=null");
  if (y != null)
    println("y!=null");
  if (z != null)
    println("z!=null");

  x = ArrayType<int, 1>(RD(0, 4));
  y = ArrayType<double, 2>(RD(PT(0, 0), PT(4, 6)));
  z = ArrayType<int, 1>(RD(0, 6));

  if (x == null)
    println("x==null");
  if (y == null)
    println("y==null");
  if (z == null)
    println("z==null");
  if (x != null)
    println("x!=null");
  if (y != null)
    println("y!=null");
  if (z != null)
    println("z!=null");

  x = null;
  x = (ArrayType<int, 1 GLOBAL>) null;
  y = null;
  z = null;

  if (x == null)
    println("x==null");
  if (y == null)
    println("y==null");
  if (z == null)
    println("z==null");
  if (x != null)
    println("x!=null");
  if (y != null)
    println("y!=null");
  if (z != null)
    println("z!=null");

  println("done.");

  return 0;
}
