/* Test unstrided arrays */
#include "arraytests.h"

#ifndef ArrayType
# define ArrayType ndarray
#endif
#ifndef GLOBAL
# define GLOBAL
#endif

#if __cplusplus >= 201103L
// test passing raw initializer lists to array ops
# define POINTLIT(...) { __VA_ARGS__ }
#else
# define POINTLIT POINT
#endif

int main() {
  cout << "Running array operations tests.." << endl;

  /* some basic array copy tests */
  { /* contiguous copy with differing base, using integer indexing */
    ArrayType<int, 1, row GLOBAL> x(RD(1, 101));
    ArrayType<int, 1, row GLOBAL> y(RD(50, 151));

    foreachd (p, x.domain(), 1) { x[p] = p; }
    foreachd (p, y.domain(), 1) { y[p] = p+1000; }
    x.copy(y);
    foreach (p, x.domain()) {
      int expected;
      if (p[1] < 50) expected = p[1];
      else expected = p[1]+1000;
      if (x[p] != expected) 
        cout << "Mismatch detected in copy test 0 at " << p <<
          ": expected: " << expected <<
          "  got: " << x[p] << endl;
    };
  }

  { /* contiguous copy with differing base, using integer indexing */
    ArrayType<int, 1, unstrided GLOBAL> x(RD(1, 101));
    ArrayType<int, 1, unstrided GLOBAL> y(RD(50, 151));

    foreachd (p, x.domain(), 1) { x[p] = p; }
    foreachd (p, y.domain(), 1) { y[p] = p+1000; }
    x.copy(y);
    foreach (p, x.domain()) {
      int expected;
      if (p[1] < 50) expected = p[1];
      else expected = p[1]+1000;
      if (x[p] != expected) 
        cout << "Mismatch detected in copy test 0b at " << p <<
          ": expected: " << expected <<
          "  got: " << x[p] << endl;
    };
  }

  { /* contiguous copy with differing base, using point indexing */
    ArrayType<int, 1, row GLOBAL> x(RD(PT(1), PT(101)));
    ArrayType<int, 1, row GLOBAL> y(RD(PT(50), PT(151)));

    foreach (p, x.domain()) { x[p] = p[1]; };
    foreach (p, y.domain()) { y[p] = p[1]+1000; };
    x.copy(y);
    foreach (p, x.domain()) {
      int expected;
      if (p[1] < 50) expected = p[1];
      else expected = p[1]+1000;
      if (x[p] != expected) 
        cout << "Mismatch detected in copy test 1 at " << p <<
          ": expected: " << expected <<
          "  got: " << x[p] << endl;
    };
  }

  { /* tranpose from contiguous -> contiguous, using integer indexing */
    ArrayType<int, 2, row GLOBAL> x(RD(PT(1, 11), PT(6, 21)));
    ArrayType<int, 2, unstrided> y = ArrayType<int, 2>(RD(PT(11, 1), PT(21, 6))).permute(POINTLIT(2,1));
    foreachd (p, x.domain(), 1) {
      foreachd(q, x.domain(), 2) {
        x(p, q) = p*100+q;
      }
    }
    foreachd (p, y.domain(), 1) {
      foreachd(q, y.domain(), 2) {
        y(p, q) = p*100+q+1000;
      }
    }
    x.copy(y);
    foreach (p, x.domain()) {
      int expected = p[1]*100+p[2]+1000;
      if (x[p] != expected) 
        cout << "Mismatch detected in copy test 2 at " << p <<
          ": expected: " << expected <<
          "  got: " << x[p] << endl;
    };
  }

  { /* tranpose from contiguous -> contiguous, using point indexing */
    ArrayType<int, 2, row GLOBAL> x(RD(PT(1, 11), PT(6, 21)));
    ArrayType<int, 2, unstrided> y = ArrayType<int, 2>(RD(PT(11, 1), PT(21, 6))).permute(POINTLIT(2,1));
    foreach (p, x.domain()) { x[p] = p[1]*100+p[2]; };
    foreach (p, y.domain()) { y[p] = p[1]*100+p[2]+1000; };
    x.copy(y);
    foreach (p, x.domain()) {
      int expected = p[1]*100+p[2]+1000;
      if (x[p] != expected) 
        cout << "Mismatch detected in copy test 3 at " << p <<
          ": expected: " << expected <<
          "  got: " << x[p] << endl;
    };
  }

  { /* equal sideFactor/stride ratio */
    ArrayType<int, 2, strided GLOBAL> x(RD(PT(1, 11), PT(6, 21), PT(2, 1)));
    ArrayType<int, 2, row GLOBAL> y(RD(PT(1, 11), PT(6, 16)));
    foreachd (p, x.domain(), 1) {
      foreachd(q, x.domain(), 2) {
        x(p, q) = p*100+q;
      }
    }
    foreachd (p, y.domain(), 1) {
      foreachd(q, y.domain(), 2) {
        y(p, q) = p*100+q+1000;
      }
    }
    x.copy(y);
    foreach (p, x.domain()) {
      int expected;
      if (p[2] > 15) expected = p[1]*100+p[2];
      else expected = p[1]*100+p[2]+1000;
      if (x[p] != expected) 
        cout << "Mismatch detected in copy test 4 at " << p <<
          ": expected: " << expected <<
          "  got: " << x[p] << endl;
    };
  }

  {
    // test overlap (contiguous)
    ArrayType<int, 1, row GLOBAL> x(RD(0, 7));
    foreachd (p, x.domain(), 1) { x[p] = 100+p; }
    ArrayType<int, 1, row GLOBAL> y = x.translate(POINTLIT(2));
    x.copy(y);
    foreach (p, x.domain()) {
      int expected;
      if (p[1] < 2) expected = 100+p[1];
      else expected = 100+p[1]-2;
      if (x[p] != expected) 
        cout << "Mismatch detected in copy overlap test 5 at " << p <<
          ": expected: " << expected <<
          "  got: " << x[p] << endl;
    };
  }

  {
    // test overlap (non-contiguous)
    ArrayType<int, 1, row GLOBAL> x = ArrayType<int, 1, row GLOBAL>(RD(0, 61)).constrict(RD(0, 61, 10));
    foreachd (p, x.domain(), 1) { x[p] = 100+p; }
    ArrayType<int, 1, row GLOBAL> y = x.translate(POINTLIT(20));
    x.copy(y);
    foreach (p, x.domain()) {
      int expected;
      if (p[1] < 20) expected = 100+p[1];
      else expected = 100+p[1]-20;
      if (x[p] != expected) 
        cout << "Mismatch detected in copy overlap test 6 at " << p <<
          ": expected: " << expected <<
          "  got: " << x[p] << endl;
    };
  }

  cout << "done." << endl;
  return 0;
}
