#include "arraytests.h"
#include <ctime>

int main(int argc, char **argv) {
  int output = 0;
  int S = 16;
  int iters = 10;
  if (argc > 1) output = atoi(argv[1]);
  if (argc > 2) S = atoi(argv[2]);
  if (argc > 3) iters = atoi(argv[3]);
  cout << "4-D Transpose test: sz=" << POINT(S,S,S,S) <<
    "(" << (S*S*S*S) << " elements, " << (S*S*S*8) << " bytes)  iters=" << iters << endl;
  ndarray<double, 4> src(RD(PT(0, 0, 0, 0), PT(S, S, S, S)));
  ndarray<double, 4> dst(RD(PT(0, 0, 0, 0), PT(S, S, S, S)));
  typedef ndarray<double, 4> double4d;
  ndarray<ndarray<double, 4, unstrided>, 1> srcs(RD(0, 24));
  ndarray<ndarray<double, 4, unstrided>, 1> dsts(RD(0, 24));
  ndarray<point<4>, 1> ps(RD(0, 24));
  ps[0] = POINT(1,2,3,4);
  ps[1] = POINT(1,2,4,3);
  ps[2] = POINT(1,3,2,4);
  ps[3] = POINT(1,3,4,2);
  ps[4] = POINT(1,4,2,3);
  ps[5] = POINT(1,4,3,2);
  ps[6] = POINT(2,1,3,4);
  ps[7] = POINT(2,1,4,3);
  ps[8] = POINT(2,3,1,4);
  ps[9] = POINT(2,3,4,1);
  ps[10] = POINT(2,4,1,3);
  ps[11] = POINT(2,4,3,1);
  ps[12] = POINT(3,1,2,4);
  ps[13] = POINT(3,1,4,2);
  ps[14] = POINT(3,2,1,4);
  ps[15] = POINT(3,2,4,1);
  ps[16] = POINT(3,4,1,2);
  ps[17] = POINT(3,4,2,1);
  ps[18] = POINT(4,1,2,3);
  ps[19] = POINT(4,1,3,2);
  ps[20] = POINT(4,2,1,3);
  ps[21] = POINT(4,2,3,1);
  ps[22] = POINT(4,3,1,2);
  ps[23] = POINT(4,3,2,1);
  foreach (i, ps.domain()) {
    srcs[i] = src.permute(ps[i]);
    dsts[i] = dst.permute(ps[i]);
  };
  foreach (p, src.domain()) {
    src[p] = 1000000000*p[1] + 1000000*p[2] + 1000*p[3] + p[4];
  };

  foreach (i, ps.domain()) {
    foreach (j, ps.domain()) {
      clock_t t;
  
      dst.set(0); 
      t = clock();
      for (int x=0;x<iters;x++) {
        dsts[j].copy(srcs[i]);
      }
      t = clock() - t;
      cout << "copy/transpose " << ps[i] << "->" << ps[j] << ": ";
      if (output) cout << (((double) t)*1000/CLOCKS_PER_SEC/iters) << " ms/iter" << endl;
      else cout << "<output suppressed>" << endl;

      foreach (p, dsts[j].domain()) {
        if (srcs[i][p] != dsts[j][p]) {
          cout << "VERIFICATION FAILED: srcs[" << i << "]" << p << "=" << srcs[i][p] <<
            "  dsts["<< i << "]" << p << "=" << dsts[i][p] << endl;
        }
      };
    };
  };
          
}
