// -------------------------------------------------------------------
//
// Name: pd_list.tst
//
// Version: 1.0
//
// Creator: miyamoto
//
// Date: 980205
//
// Purpose:
//   Test PointList and rectdomainList
//
// Change Log:
//
// -------------------------------------------------------------------

// Test rectdomainList and PointList

#include <algorithm>
#include "../tests.h"

int main() {
  domain<3> d1 = rectdomain<3>(point<3>::all(0), point<3>::all(0));
  rectdomain<3> *da1;
  point<3> *pa1;
  size_t da1s, pa1s;

  d1 = d1 + rectdomain<3>(point<3>::all(1), point<3>::all(4));
  d1 = d1 + rectdomain<3>(point<3>::all(2), point<3>::all(5));
  d1 = d1 + rectdomain<3>(point<3>::all(3), point<3>::all(6));

  da1 = d1.rectdomain_list();
  da1s = d1.num_rectdomains();
  pa1 = d1.point_list();
  pa1s = d1.size();

  sort(da1, da1 + da1s, rd_compare<3>);
  sort(pa1, pa1 + pa1s, point<3>::compare);

  println("da1:");
  for (size_t i = 0; i < da1s; i++) {
    println(da1[i]);
  }
  println("pa1:");
  for (size_t i = 0; i < pa1s; i++) {
    println(pa1[i]);
  }
}
