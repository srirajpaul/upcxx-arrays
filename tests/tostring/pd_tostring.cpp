// -------------------------------------------------------------------
//
// Name: pd_tostring.tst
//
// Version: 1.0
//
// Creator: miyamoto
//
// Date: 990209
//
// Purpose:
//   Test string representation of domains, rectdomans and points.
//
// Change Log:
//
// -------------------------------------------------------------------
// Test string representation of points, domains, and rectdomains

#include "../tests.h"

int main() {
  println(POINT(1,2));
  println(rectdomain<2>(POINT(1,1), POINT(3,3)));
  println(rectdomain<2>(POINT(1,1), POINT(3,3)) +
          rectdomain<2>(POINT(2,2), POINT(4,4)));
  print("(1,2) = ");
  println(POINT(1,2));
  return 0;
}
