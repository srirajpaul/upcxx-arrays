// -------------------------------------------------------------------
//
// Name: dotEquals.tst
//
// Version: 1.1
//
// Creator: ihaque
//
// Date: 040713
//
// Purpose:
//   Test the .equals method on domains and rectdomains.
//
// Change Log:
//
// 1.1: 7-16-04: removed calls to domain<N>.op== and
//               rectdomain<N>.op==(domain<N>)
// -------------------------------------------------------------------

// Test .equals on Domains and rectdomains

#include "../tests.h"

#define PL(info, test) do {                     \
    print(info);                                \
    println(test);                              \
  } while(0)

int main() {
  rectdomain<1> rd1=rectdomain<1>(1,6);
  rectdomain<1> rd2=rectdomain<1>(0,0);
  PL("RD false equality: ",(((rd1==rd2)==(rd1.equals(rd2)))?"PASS":"FAIL"));
  PL("RD false equality reversed: ",(((rd1==rd2)==(rd2.equals(rd1)))?"PASS":"FAIL"));
  PL("RD true nonnull equality: ",(((rd1==rd1)==(rd1.equals(rd1)))?"PASS":"FAIL"));
  PL("RD true null equality: ",(((rd2==rd2)==(rd2.equals(rd2)))?"PASS":"FAIL"));

  domain<1> d1=rd1;
  domain<1> d2=rd2;
  domain<1> d3=rd1+rectdomain<1>(7,11);
  domain<1> d4=rectdomain<1>(1,4)+rectdomain<1>(2,6);

  PL("D false nonnull-null equality: ",((d1.equals(d2)==false)?"PASS":"FAIL"));
  PL("D false null-nonnull equality: ",((d2.equals(d1)==false)?"PASS":"FAIL"));
  PL("D true nonnull equality: ",      ((d1.equals(d1)==true)?"PASS":"FAIL"));
  PL("D true null equality: ",         ((d2.equals(d2)==true)?"PASS":"FAIL"));
  PL("D true multirect nonnull equality: ",((d3.equals(d3)==true)?"PASS":"FAIL"));
  PL("D false multirect nonnull equality: ",((d3.equals(d4)==false)?"PASS":"FAIL"));
  PL("D true RD-multirect equality: ",((rd1.equals(d4)==true)?"PASS":"FAIL"));
  PL("RD true RD-multirect equality: ",((rd1.equals(d4)==true)?"PASS":"FAIL"));
  PL("D false rect-multi equality: ",((d1.equals(d3)==false)?"PASS":"FAIL"));
  PL("D false multi-rect equality: ",((d3.equals(d1)==false)?"PASS":"FAIL"));

  return 0;
}

