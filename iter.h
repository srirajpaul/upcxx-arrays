#pragma once

namespace upcxx {

  template<int N> class rectdomain;
  template<int N> class domain;

#ifdef UPCXXA_USE_CXX11
# ifdef UPCXXA_DEFAULT_CMAJOR
#  define UPCXXA_ITER_DIR(i) (-(i))
# else
#  define UPCXXA_ITER_DIR(i) (+(i))
# endif

  template<int N> struct rdloop {
    template<class F, class... Is>
    static void loop(const F &func, const cint_t *lwb,
                     const cint_t *upb, const cint_t* stride,
                     Is... is) {
      lwb += UPCXXA_ITER_DIR(1);
      upb += UPCXXA_ITER_DIR(1);
      stride += UPCXXA_ITER_DIR(1);
      for (cint_t x = lwb[UPCXXA_ITER_DIR(-1)],
             u = upb[UPCXXA_ITER_DIR(-1)],
             s = stride[UPCXXA_ITER_DIR(-1)]; x < u; x += s) {
# ifdef UPCXXA_DEFAULT_CMAJOR
        rdloop<N-1>::loop(func, lwb, upb, stride, x, is...);
# else
        rdloop<N-1>::loop(func, lwb, upb, stride, is..., x);
# endif
      }
    }
  };

  template<> struct rdloop<1> {
    template<class F, class... Is>
    static void loop(const F &func, const cint_t *lwb,
                     const cint_t *upb, const cint_t* stride,
                     Is... is) {
      for (cint_t x = lwb[0], u = upb[0], s = stride[0]; x < u; x += s) {
# ifdef UPCXXA_DEFAULT_CMAJOR
        func(PT(x, is...));
# else
        func(PT(is..., x));
# endif
      }
    }
  };

# ifdef UPCXXA_SPEC_FOREACH
  template<> struct rdloop<2> {
    template<class F, class... Is>
    static void loop(const F &func, const cint_t *lwb,
                     const cint_t *upb, const cint_t* stride,
                     Is... is) {
      for (cint_t x0 = lwb[0], u0 = upb[0], s0 = stride[0]; x0 < u0; x0 += s0) {
        for (cint_t x1 = lwb[UPCXXA_ITER_DIR(1)],
               u1 = upb[UPCXXA_ITER_DIR(1)],
               s1 = stride[UPCXXA_ITER_DIR(1)]; x1 < u1; x1 += s1) {
#  ifdef UPCXXA_DEFAULT_CMAJOR
          func(PT(x1, x0, is...));
#  else
          func(PT(is..., x0, x1));
#  endif
        }
      }
    }
  };

  template<> struct rdloop<3> {
    template<class F, class... Is>
    static void loop(const F &func, const cint_t *lwb,
                     const cint_t *upb, const cint_t* stride,
                     Is... is) {
      for (cint_t x0 = lwb[0], u0 = upb[0], s0 = stride[0]; x0 < u0; x0 += s0) {
        for (cint_t x1 = lwb[UPCXXA_ITER_DIR(1)],
               u1 = upb[UPCXXA_ITER_DIR(1)],
               s1 = stride[UPCXXA_ITER_DIR(1)]; x1 < u1; x1 += s1) {
          for (cint_t x2 = lwb[UPCXXA_ITER_DIR(2)],
                 u2 = upb[UPCXXA_ITER_DIR(2)],
                 s2 = stride[UPCXXA_ITER_DIR(2)]; x2 < u2; x2 += s2) {
#  ifdef UPCXXA_DEFAULT_CMAJOR
            func(PT(x2, x1, x0, is...));
#  else
            func(PT(is..., x0, x1, x2));
#  endif
          }
        }
      }
    }
  };
# endif // UPCXXA_SPEC_FOREACH
#endif

  template<int N> class point_iter {
    typedef typename domain<N>::vsize_t vsize_t;

  public:
    point_iter(const rectdomain<N> &rd) : delete_rds(false) {
      if (!rd.is_empty()) {
        cached = rd;
        rds = &cached;
        num_rds = 1;
        crnt_rd = 0;
        done = false;
      } else {
        num_rds = 0;
        done = true;
      }
    }

    point_iter(const domain<N> &d) : delete_rds(false) {
      if (!d.is_empty()) {
        rds = d.rectdomain_list();
        delete_rds = true;
        num_rds = d.num_rectdomains();
        crnt_rd = 0;
        done = false;
      } else {
        num_rds = 0;
        done = true;
      }
    }

    ~point_iter() {
      if (delete_rds)
        delete[] rds; // only true when rds is an array
    }

    point<N> start() {
      if (num_rds) {
        stride = rds[crnt_rd].stride();
        lwb = rds[crnt_rd].lwb();
        upb = rds[crnt_rd].upb();
        point<N> res = lwb;
        res.x[N-1] -= stride.x[N-1];
        return res;
      }
      return point<N>::all(0);
    }

    bool next(point<N> &p) {
      for (int i = N-1; i >= 0; i--) {
        p.x[i] += stride.x[i];
        if (p.x[i] < upb.x[i])
          return true; 
        p.x[i] = lwb.x[i];
      }
      crnt_rd += 1;
      if (crnt_rd < num_rds) {
        p = start();
        return next(p);
      }
      return false;
    }

    point<N> dummy_pt() {
      return lwb;
    }

    bool set_pt(point<N> &pt_) {
      pt = pt_.x;
      return true;
    }

#ifdef UPCXXA_USE_CXX11
    template<class F> point_iter &operator=(const F &func) {
      for (vsize_t i = 0; i < num_rds; i++) {
# ifdef UPCXXA_DEFAULT_CMAJOR
        rdloop<N>::loop(func, rds[i].p0.x+N-1, rds[i].p1.x+N-1,
                        rds[i].loop_stride.x+N-1);
# else
        rdloop<N>::loop(func, rds[i].p0.x, rds[i].p1.x,
                        rds[i].loop_stride.x);
# endif
      }
      return *this;
    }
#endif

    bool done;

  private:
    point<N> stride, upb, lwb;
    rectdomain<N> cached;
    const rectdomain<N> *rds;
    bool delete_rds;
    vsize_t num_rds;
    vsize_t crnt_rd;
    cint_t *pt;
  };

} // namespace upcxx
