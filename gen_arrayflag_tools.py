# This script generates macros to combine multiple array flags.

from itertools import product;

default_flags = ['arraydefault']
globalness_flags = ['local', 'global']
stridedness_flags = ['strided', 'unstrided', 
                     'row', 'column']

globalness_default = 'default_globalness'
stridedness_default = 'default_stridedness'

globalness_combinator = 'globalness'
stridedness_combinator = 'stridedness'

globalness_conv_name = 'globalness'
stridedness_conv_name = 'stridedness'

globalness_joins = {('local', 'global') : 'global'};
stridedness_joins = {('strided', 'unstrided') : 'strided',
                     ('strided', 'row') : 'strided',
                     ('strided', 'column') : 'strided',
                     ('unstrided', 'row') : 'unstrided',
                     ('unstrided', 'column') : 'unstrided',
                     ('row', 'column') : 'unstrided'}

combo_macro = 'UPCXXA_ARRAYFLAG_COMBO'
conv_macro = 'UPCXXA_ARRAYFLAG_ALLOW_CONV'

def emit_macro(macro, arg1, *args):
    result = macro + '(' + arg1
    for arg in args:
        result += ', ' + arg
    result += ');'
    print(result)

def gen_combos(combinator, relevant_flags, other_flags, joiner,
               default):
    for x, y in product(relevant_flags + other_flags, repeat=2):
        if x not in relevant_flags and y not in relevant_flags:
            emit_macro(combo_macro, combinator, x, y, default)
        elif x not in relevant_flags:
            emit_macro(combo_macro, combinator, x, y, y)
        elif y not in relevant_flags:
            emit_macro(combo_macro, combinator, x, y, x)
        elif x == y:
            emit_macro(combo_macro, combinator, x, y, x)
        elif (x, y) in joiner:
            emit_macro(combo_macro, combinator, x, y, joiner[(x, y)])
        elif (y, x) in joiner:
            emit_macro(combo_macro, combinator, x, y, joiner[(y, x)])
        else:
            raise LookupError("unknown combination: " + x + ", " + y)

def gen_convs(name, relevant_flags, joiner):
    for x, y in product(relevant_flags, repeat=2):
        if x == y:
            emit_macro(conv_macro, name, x, y)
        elif (x, y) in joiner:
            if joiner[(x, y)] == y:
                emit_macro(conv_macro, name, x, y)
            elif joiner[(x, y)] == x:
                emit_macro(conv_macro, name, y, x)

def gen_all():
    gen_combos(globalness_combinator, globalness_flags,
               default_flags + stridedness_flags, globalness_joins,
               globalness_default)
    gen_combos(stridedness_combinator, stridedness_flags,
               default_flags + globalness_flags, stridedness_joins,
               stridedness_default)
    gen_convs(globalness_conv_name, globalness_flags,
              globalness_joins)
    gen_convs(stridedness_conv_name, stridedness_flags,
              stridedness_joins)

if __name__ == '__main__':
    gen_all()
