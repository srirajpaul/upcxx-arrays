#pragma once

#include <iostream>
#include "defs.h"

namespace upcxx {

template<int N> struct point {
  static point all(cint_t x);
  static point direction(int k, cint_t x=1);

  cint_t x[N];

  point operator-() const;
  point operator+(const point &p) const;
  point &operator+=(const point &p);
  point operator-(const point &p) const;
  point &operator-=(const point &p);
  point operator*(const point &p) const;
  point &operator*=(const point &p);
  point operator/(const point &p) const;
  point &operator/=(const point &p);
  point operator+(cint_t n) const;
  point &operator+=(cint_t n);
  point operator-(cint_t n) const;
  point &operator-=(cint_t n);
  point operator*(cint_t n) const;
  point &operator*=(cint_t n);
  point operator/(cint_t n) const;
  point &operator/=(cint_t n);
  point operator<<(cint_t n) const;
  point operator>>(cint_t n) const;
  bool operator==(const point &p) const;
  bool operator!=(const point &p) const;
  bool operator<(const point &p) const;
  bool operator<=(const point &p) const;
  bool operator>(const point &p) const;
  bool operator>=(const point &p) const;

  static bool compare(const point &p1, const point &p2); // lexicographic <=
  enum { arity = N };

  point permute(const point &p) const;

  cint_t operator[](int index) const;

  point lower_bound(const point &p) const;
  point upper_bound(const point &p) const;
  point replace(int index, cint_t value) const;

  // undocumented methods
  cint_t get0(int index) const; // 0-based indexing!!!
  // P.set(i,v) returns new point NP with NP[i+1] == v, 
  // and other components the same as P
  point set0(int index, cint_t value) const; // 0-based indexing!!!
  // P.get_lcm(P2) returns new point NP with NP[i] == leastCommonMultiple(P[i], P2[i])
  point get_lcm(const point &p) const;

  // Print a string representation to the given stream.
  friend std::ostream& operator<<(std::ostream& os, const point& p) {
    os << '(' << p.x[0];
    for (int i = 1; i < N; i++)
      os << "," << p.x[i];
    os << ')';
    return os;
  }
};

template<> class point<0> {};

template<int N> point<N> operator+(cint_t n, const point<N> &p);
template<int N> point<N> operator-(cint_t n, const point<N> &p);
template<int N> point<N> operator*(cint_t n, const point<N> &p);
template<int N> point<N> operator/(cint_t n, const point<N> &p);

template<int N> point<N> make_point(const cint_t px[N]) {
  point<N> res;
  for (int i = 0; i < N; i++)
    res.x[i] = px[i];
  return res;
}

#ifdef UPCXXA_USE_CXX11
// Point construction functions
template<class... Is>
inline point<sizeof...(Is)> PT(Is... is) {
  return point<sizeof...(Is)>{{ is... }};
}
#else
# include "point_aux.h" // PT specializations
#endif

} // namespace upcxx

#include "point.tpp"
