/* array_impl.c */

#include <iostream>
#include <cstdio>

#define UPCXXA_PRINT(val, stream) std::c##stream << val

#ifndef UPCXXA_GLOBAL_ARRAY
#define UPCXXA_GLOBAL_ARRAY 1
#endif

#ifndef UPCXXA_DEBUG_ARRAYS
#define UPCXXA_DEBUG_ARRAYS 0
#endif

#define UPCXXA_ASSIGNABLE 1


#define UPCXXA__FORALL2ORDERED3(e,x,f,y,lo,hi,stride_,rdmin,dim0,dim1,dim2,body,TPTR,INDEXFN,CVT) \
  do {                                                                  \
    const cint_t len0 = UPCXXA_PFAST_DIVIDE((hi[dim0] - lo[dim0]),      \
                                            stride_[dim0]) + 1;         \
    const cint_t len1 = UPCXXA_PFAST_DIVIDE((hi[dim1] - lo[dim1]),      \
                                            stride_[dim1]) + 1;         \
    const cint_t len2 = UPCXXA_PFAST_DIVIDE((hi[dim2] - lo[dim2]),      \
                                            stride_[dim2]) + 1;         \
    const cint_t xIncrement2 = x.side_factors[dim2] *                   \
      UPCXXA_PFAST_DIVIDE(stride_[dim2], x.stride[dim2]);               \
    const cint_t xIncrement1t= x.side_factors[dim1] *                   \
      UPCXXA_PFAST_DIVIDE(stride_[dim1], x.stride[dim1]);               \
    const cint_t xIncrement1 = xIncrement1t - xIncrement2*len2;         \
    const cint_t xIncrement0 = x.side_factors[dim0] *                   \
      UPCXXA_PFAST_DIVIDE(stride_[dim0], x.stride[dim0])                \
      - xIncrement1t*len1;                                              \
    const cint_t yIncrement2 = y.side_factors[dim2] *                   \
      UPCXXA_PFAST_DIVIDE(stride_[dim2], y.stride[dim2]);               \
    const cint_t yIncrement1t= y.side_factors[dim1] *                   \
      UPCXXA_PFAST_DIVIDE(stride_[dim1], y.stride[dim1]);               \
    const cint_t yIncrement1 = yIncrement1t - yIncrement2*len2;         \
    const cint_t yIncrement0 = y.side_factors[dim0] *                   \
      UPCXXA_PFAST_DIVIDE(stride_[dim0], y.stride[dim0])                \
      - yIncrement1t*len1;                                              \
    cint_t _i0,_i1,_i2;                                                 \
    TPTR e; TPTR f;                                                     \
    INDEXFN(e, CVT(x.A), x.compute_index("copy()", rdmin));             \
    INDEXFN(f, CVT(y.A), y.compute_index("copy()", rdmin));             \
    for(_i0=len0;_i0;_i0--) {                                           \
      for(_i1=len1;_i1;_i1--) {                                         \
        for(_i2=len2;_i2;_i2--) {                                       \
          body;                                                         \
          INDEXFN(e,e,xIncrement2);INDEXFN(f,f,yIncrement2);            \
        }                                                               \
        INDEXFN(e,e,xIncrement1);INDEXFN(f,f,yIncrement1);              \
      }                                                                 \
      INDEXFN(e,e,xIncrement0);INDEXFN(f,f,yIncrement0);                \
    }                                                                   \
  } while(0)

#if UPCXXA_GLOBAL_ARRAY
# define UPCXXA_FORALL2ORDERED3(e,x,f,y,lo,hi,stride,rdmin,dim0,dim1,dim2,body,localbody) \
  do {                                                                  \
    if (UPCXXA_IS_DIRECTLY_ADDRESSABLE(x.A) &&                          \
        UPCXXA_IS_DIRECTLY_ADDRESSABLE(y.A))                            \
      UPCXXA__FORALL2ORDERED3(e,x,f,y,lo,hi,stride,rdmin,dim0,dim1,     \
                              dim2,localbody,T*,UPCXXA_INDEX_LOCAL,     \
                              UPCXXA_TO_LOCAL);                         \
    else                                                                \
      UPCXXA__FORALL2ORDERED3(e,x,f,y,lo,hi,stride,rdmin,dim0,dim1,     \
                              dim2,body,UPCXXA_PTR_TO_T,UPCXXA_INDEX,   \
                              UPCXXA__IDENTITY);                        \
  } while(0)
#else
# define UPCXXA_FORALL2ORDERED3(e,x,f,y,lo,hi,stride,rdmin,dim0,dim1,dim2,body,localbody) \
  UPCXXA__FORALL2ORDERED3(e,x,f,y,lo,hi,stride,rdmin,dim0,dim1,dim2,    \
                          localbody,T*,UPCXXA_INDEX_LOCAL,              \
                          UPCXXA__IDENTITY)
#endif


#define UPCXXA__FORALL2ORDERED2(e,x,f,y,lo,hi,stride_,rdmin,dim0,dim1,body,TPTR,INDEXFN,CVT) \
  do {                                                                  \
    const cint_t len0 = UPCXXA_PFAST_DIVIDE((hi[dim0] - lo[dim0]),      \
                                            stride_[dim0]) + 1;         \
    const cint_t len1 = UPCXXA_PFAST_DIVIDE((hi[dim1] - lo[dim1]),      \
                                            stride_[dim1]) + 1;         \
    const cint_t xIncrement1 = x.side_factors[dim1] *                   \
      UPCXXA_PFAST_DIVIDE(stride_[dim1], x.stride[dim1]);               \
    const cint_t xIncrement0 = x.side_factors[dim0] *                   \
      UPCXXA_PFAST_DIVIDE(stride_[dim0], x.stride[dim0])                \
      - xIncrement1*len1;                                               \
    const cint_t yIncrement1 = y.side_factors[dim1] *                   \
      UPCXXA_PFAST_DIVIDE(stride_[dim1], y.stride[dim1]);               \
    const cint_t yIncrement0 = y.side_factors[dim0] *                   \
      UPCXXA_PFAST_DIVIDE(stride_[dim0], y.stride[dim0])                \
      - yIncrement1*len1;                                               \
    cint_t _i0,_i1;                                                     \
    TPTR e; TPTR f;                                                     \
    INDEXFN(e, CVT(x.A), x.compute_index("copy()", rdmin));             \
    INDEXFN(f, CVT(y.A), y.compute_index("copy()", rdmin));             \
    for(_i0=len0;_i0;_i0--) {                                           \
      for(_i1=len1;_i1;_i1--) {                                         \
        body;                                                           \
        INDEXFN(e,e,xIncrement1);INDEXFN(f,f,yIncrement1);              \
      }                                                                 \
      INDEXFN(e,e,xIncrement0);INDEXFN(f,f,yIncrement0);                \
    }                                                                   \
  } while(0)

#if UPCXXA_GLOBAL_ARRAY
# define UPCXXA_FORALL2ORDERED2(e,x,f,y,lo,hi,stride,rdmin,dim0,dim1,body,localbody) \
  do {                                                                  \
    if (UPCXXA_IS_DIRECTLY_ADDRESSABLE(x.A) &&                          \
        UPCXXA_IS_DIRECTLY_ADDRESSABLE(y.A))                            \
      UPCXXA__FORALL2ORDERED2(e,x,f,y,lo,hi,stride,rdmin,dim0,dim1,     \
                              localbody,T*,UPCXXA_INDEX_LOCAL,          \
                              UPCXXA_TO_LOCAL);                         \
    else                                                                \
      UPCXXA__FORALL2ORDERED2(e,x,f,y,lo,hi,stride,rdmin,dim0,dim1,     \
                              body,UPCXXA_PTR_TO_T,UPCXXA_INDEX,        \
                              UPCXXA__IDENTITY);                        \
  } while(0)
#else
# define UPCXXA_FORALL2ORDERED2(e,x,f,y,lo,hi,stride,rdmin,dim0,dim1,body,localbody) \
  UPCXXA__FORALL2ORDERED2(e,x,f,y,lo,hi,stride,rdmin,dim0,dim1,         \
                          localbody,T*,UPCXXA_INDEX_LOCAL,              \
                          UPCXXA__IDENTITY)
#endif


#define UPCXXA_TEMPLATE template<class T, int N>
#define UPCXXA_TEMPLATE_ARGS T, N
#define UPCXXA_TEMPLATE_ARGS_N_MINUS_1 T, UPCXXA_ARITY_N_MINUS_1
#define UPCXXA_ARRAY UPCXXA_ARRAY_T<UPCXXA_TEMPLATE_ARGS>
#define UPCXXA_ARRAYSIG(ret) UPCXXA_TEMPLATE ret UPCXXA_ARRAY::
#define UPCXXA_ARRAYSIG_NORET() UPCXXA_TEMPLATE UPCXXA_ARRAY::

namespace upcxx {

#include "array_addr.h" /* must come after defines above */

UPCXXA_ARRAYSIG_NORET() UPCXXA_ARRAY_T(const rectdomain<N> &R,
                                       void *(*allocator)(size_t),
                                       UPCXXA_PTR_TO_T ext_mem,
                                       point<N> padding) {
  int i;
  cint_t max[N];
  size_t size, bytes;
  point<N> p;
  size_t *mem;
  T *array;

  _domain = R;

#ifdef UPCXXA_ARRAYS_USERCHECK
  if (!(padding >= point<N>::all(0))) {
      std::cerr << "cannot create array with negative padding "
                << padding << std::endl;
      abort();
  }
#endif
  if (!R.is_empty() || /* only do calculation for non-empty array */
      padding != point<N>::all(0)) {
    p = R.min(); /* minimum point in R */
    for (i = 0; i < N; i++) {
      base[i] = p[i + 1];
    }

    p = R.raw_stride(); /* stride of R */
    for (i = 0; i < N; i++)
      stride[i] = p[i + 1];
  
    p = R.upb(); /* one greater than maximum point in R */
    for (i = 0; i < N; i++)
      max[i] = UPCXXA_PFAST_DIVIDE((p[i + 1] - 1 - base[i]),
                                   stride[i]);
    
    side_factors[N - 1] = 1;
    if (N > 1) {
      cint_t sf = 1;
      for (i = N - 2; i >= 0; i--)
        side_factors[i] = (sf = sf * (1 + max[i + 1]) + padding[i+2]);
    }

    size = 1 + max[0];
    if (N > 1)
      size *= side_factors[0];
    size += padding[1];
  } else {
    size = 0;
  }

#if UPCXXA_DEBUG_ARRAYS
  fprintf(stderr, "p%d: new array with domain = ", UPCXXA_MYPROC);
  UPCXXA_PRINT(R, err);
  fprintf(stderr, "\nArray<%d>:\tbase\tmax\tstride\tside_factors\n", N);
  for (i = 0; i < N; i++) {
    fprintf(stderr, " %d\t\t%d\t%d\t%d\t%d\n", i,
            base[i], max[i], stride[i], side_factors[i]);
  }
#endif

  if (UPCXXA_LOCAL_PART(ext_mem) == NULL) {
    bytes = size * sizeof(T);
    if (allocator == (void *(*)(size_t)) std::malloc) {
      array = new T[size]; // use new[] instead
      mem = (size_t *) array;
    } else {
      bytes += sizeof(size_t); // extra for bookkeeping
      mem = (size_t *) allocator(bytes);
    }
    if (!mem) {
      std::cerr << "failed to allocate " << bytes << " bytes in "
                << "array constructor" << std::endl;
      abort();
    }

    /* clearing data is necessary for POD types */
    memset(mem, 0, bytes);
    if (allocator != (void *(*)(size_t)) std::malloc) {
      /* keep track of number of elements, for deletion */
      *mem = size;
      /* placement new is necessary to initialize elements for non-POD
       * types */
      array = new ((void *) (mem + 1)) T[size];
    }

#if UPCXXA_GLOBAL_ARRAY
    UPCXXA_GLOBALIZE(A, array);
    UPCXXA_GLOBALIZE(ancestor, ((T *)(void *) mem));
#else
    A = array;
    ancestor = (T *)(void *) mem;
#endif
  } else {
    /* no initialization of external memory */
    A = ext_mem;
    /* user responsible for memory management */
    ancestor = (UPCXXA_PTR_TO_T) (T *) NULL;
  }

#if UPCXXA_EXPLICITLY_STORE_CREATOR
  _creator = UPCXXA_MYPROC;
#endif
}

#define UPCXXA_ARRAY_SLICE UPCXXA_ARRAY_T<T, UPCXXA_ARITY_N_MINUS_1>
UPCXXA_ARRAYSIG(UPCXXA_ARRAY_SLICE) slice(int k, cint_t j) const {
#ifdef UPCXXA_USE_CXX11
  static_assert(N > 1, "slice undefined for N <= 1");
#else
  if (N <= 1) {
    std::cerr << "slice undefined for N <= 1 (thread = "
              << UPCXXA_MYPROC << ")" << std::endl;
    abort();
  }
#endif
  return slice_internal(k, j);
}

UPCXXA_ARRAYSIG(UPCXXA_ARRAY_SLICE) slice_internal(int k, cint_t j) const {
  int i, l;
  UPCXXA_ARRAY_SLICE r;

  UPCXXA_ASSERT_EXISTS((*this), "(in slice method)");
  r._domain = _domain.slice_internal(k);

  for (i = l = 0; i < N - 1; i++, l++) {
    if (i == k - 1) ++l;
    r.stride[i] = stride[l];
    r.base[i] = base[l];
    r.side_factors[i] = side_factors[l];
  }    

#if UPCXXA_BOUNDS_CHECKING
  { point<N> p = _domain.min().set0(k - 1, j);
    compute_index("(in slice method)", p); /* just for bounds check */
  }
#endif
  UPCXXA_INDEX(r.A, A,
               UPCXXA_PFAST_DIVIDE((j - base[k - 1]),
                                   stride[k - 1]) *
               side_factors[k - 1]);

  r.ancestor = ancestor;
#if UPCXXA_EXPLICITLY_STORE_CREATOR 
  r._creator = _creator;
#endif

#if UPCXXA_DEBUG_ARRAYS
  fprintf(stderr, "slice(%d, %d) of [%x]\n  Array<%d>:\tbase\tstride\tside_factors\n", k, j, (int) UPCXXA_LOCAL_PART(A), N);
  for (i = 0; i < N; i++) {
    fprintf(stderr, "   %d\t\t%d\t%d\t%d\n", i,
            base[i], stride[i], side_factors[i]);
  }
  fprintf(stderr, "is [%x]\n  Array<%d>:\tbase\tstride\tside_factors\n",
          (int) UPCXXA_LOCAL_PART(r.A), N - 1);
  for (i = 0; i < N - 1; i++) {
    fprintf(stderr, "   %d\t\t%d\t%d\t%d\n", i,
            r.base[i], r.stride[i], r.side_factors[i]);
  }
#endif

  return r;
}
#undef UPCXXA_ARRAY_SLICE
/* ------------------------------------------------------------------------------------ */
/* return true if the elements of x and y might overlap within rectdomain R 
 * the test is conservative, so it may return true when they actually don't overlap
 */
UPCXXA_ARRAYSIG(int) may_overlap(const UPCXXA_ARRAY &y,
                                 const rectdomain<N> &R) const {
  #if UPCXXA_GLOBAL_ARRAY
    if (UPCXXA_TO_BOX(A) != UPCXXA_TO_BOX(y.A)) return 0; /* diff mem spaces */
  #endif
  #if 0 
    /* not currently a useful optimization - we never call this on empty rectdomains */
    if (R.is_empty()) return 0; /* empty domain */
  #endif

  {
    UPCXXA_PTR_TO_T xend;
    UPCXXA_PTR_TO_T yend;
    int i;

    /* determine address of max storage address */
    point<N> xptwithmaxaddr = R.max();
    point<N> yptwithmaxaddr = xptwithmaxaddr;

    for (i = 0; i < N; i++) {
      if (side_factors[i] < 0) 
        xptwithmaxaddr = xptwithmaxaddr.set0(i, R.min()[i + 1]);

      if (y.side_factors[i] < 0) 
        yptwithmaxaddr = yptwithmaxaddr.set0(i, R.min()[i + 1]);
    }
    UPCXXA_INDEX(xend, A,
                 compute_index("array copy maxpt", xptwithmaxaddr));
    UPCXXA_INDEX(yend, y.A,
                 y.compute_index("array copy maxpt", yptwithmaxaddr));
    
    /* see if the bounding memory regions are disjoint */ 
    /* start with a faster, more conservative check based on the following property,
       that is likely to catch most cases */
    assert(UPCXXA_LOCAL_PART(get_array_data_ptr_with_domain(R)) >=
           UPCXXA_LOCAL_PART(A));
    assert(UPCXXA_LOCAL_PART(y.get_array_data_ptr_with_domain(R)) >=
           UPCXXA_LOCAL_PART(y.A));
    if (UPCXXA_LOCAL_PART(xend) < UPCXXA_LOCAL_PART(y.A) || 
        UPCXXA_LOCAL_PART(A) > UPCXXA_LOCAL_PART(yend)) return 0;

    { /* more precise test based on actual starting offset */
      UPCXXA_PTR_TO_T const xbegin = get_array_data_ptr_with_domain(R);
      UPCXXA_PTR_TO_T const ybegin = y.get_array_data_ptr_with_domain(R);
      if (UPCXXA_LOCAL_PART(xend) < UPCXXA_LOCAL_PART(ybegin) || 
          UPCXXA_LOCAL_PART(xbegin) > UPCXXA_LOCAL_PART(yend)) return 0;
    }
    
    /* we could add more precise overlap detection tests here 
     * (to detect interleaved but non-overlapping arrays)
     */ 
  }
  return 1;
}
/* ------------------------------------------------------------------------------------ */
#if UPCXXA_ASSIGNABLE
/* x.exchange(w) is equivalent to:
   ordered_foreach (p in Ti.myTeam()._domain()) x[p] = broadcast w from p[1];

   The current implementation assumes myTeam()._domain() is [0 : PROCS - 1].
   */
UPCXXA_ARRAYSIG(void) exchange(T w) const {
  UPCXXA_ASSERT_EXISTS((*this), "(in exchange method)");
  if (N != 1) {
    fprintf(stderr, "fatal error: exchange() used on %dd array; "
            "it may only be used on 1d arrays.\n", N);
    abort();
  }
  UPCXXA_TRACE_PRINTF(("COLLECTIVE exchange: sz = %lu, ",
                       (unsigned long int) sizeof(T)));

  UPCXXA_THREAD_LOCAL_BUF(T *, localbuf);

  #if UPCXXA_BOUNDS_CHECKING
  {
    point<N> start = {{0}};
    point<N> end = {{UPCXXA_PROCS -1}};
    compute_index("(in exchange method)", start);
    compute_index("(in exchange method)", end);
  }
  #endif
  if (!localbuf) { /* Have to rely on C initialization semantics; ugh. */
    size_t size = (size_t)
      UPCXXA_THREAD_LOCAL_BUF_SIZE(UPCXXA_GLOBAL_PROCS, UPCXXA_PROCS);
    localbuf =
      (T *) UPCXXA_MALLOC(sizeof(T)*size);
    if (!localbuf) {
      std::cerr << "failed to allocate " 
                << (sizeof(T)*size) << " bytes in "
                << "array exchange" << std::endl;
      abort();
    }
  }
  
  /* Call backend exchange function on localbuf. */
  UPCXXA_EXCHANGE_BULK(localbuf, &w, sizeof(T));

  { /* now each thread fills in its target array from localbuf */
    T *result = localbuf;
    point<N> p;
    UPCXXA_PTR_TO_T target;
    if (is_contiguous()) {
      /* common case of contiguous arrays - use some shortcuts 
       * (note that thanks to single restrictions, if one array is contiguous then they all are)
       * we don't currently take advantage of that, but could in the future
       */
      p = p.set0(0, 0);
      UPCXXA_INDEX(target, A,
                   compute_index("(in exchange method)", p));
      if (UPCXXA_DIRECTLY_ADDRESSABLE(target)) {
        memcpy(UPCXXA_LOCAL_PART(target), result,
               sizeof(T)*UPCXXA_PROCS);
      } else {
        UPCXXA_GPTR_TO_T gp_array;
        #if UPCXXA_GLOBAL_ARRAY
          gp_array = target;
        #else
          UPCXXA_GLOBALIZE(gp_array, target);
        #endif
        UPCXXA_LOCAL_TO_GLOBAL_COPY(result, gp_array, UPCXXA_PROCS);
      }
    } else { /* non-contiguous target array */
      int i;
      for (i = 0; i < UPCXXA_PROCS; i++) {
        p = p.set0(0, i);
        UPCXXA_INDEX(target, A,
                     compute_index("(in exchange method)", p));
        UPCXXA_ASSIGN(target, result[i]);
      }
    }
  }
  UPCXXA_THREAD_LOCAL_BUF_FREE(localbuf, UPCXXA_FREE);
}
#endif /* UPCXXA_ASSIGNABLE */
/* ------------------------------------------------------------------------------------ */
#if UPCXXA_ASSIGNABLE
/* This is a "copy descriptor", which encapsulates the domain and array 
   information a remote node will need to pack array data into a buffer. */

UPCXXA_TEMPLATE struct UPCXXA_COPY_DESCRIPTOR {
  UPCXXA_ARRAY x;  /* Array involved in the copying. */
  rectdomain<N> R; /* The subarray that will actually be copied. */
};

/* Note that this name is actually a macro, to preserve
   uniqueness among different array types. */
#define UPCXXA_COPY_DESC_T                      \
  UPCXXA_COPY_DESCRIPTOR<UPCXXA_TEMPLATE_ARGS>

/* Takes the given copy descriptor and returns a buffer that contains the
   desired elements of the array. The array_data_size argument is an output
   parameter. 
   If "allocated" is a non-null address, the data will be copied into that provided buffer
    if it is null, a new buffer will be allocated and returned
 */
UPCXXA_TEMPLATE T *UPCXXA_PACK(void *copy_desc_ptr,
                               size_t *array_data_size,
                               void *allocated) {

  UPCXXA_ARRAY x;
  rectdomain<N> R;
  int i;
  cint_t lo[N], hi[N], stride_[N];
  size_t idx;
  size_t size;
  point<N> p, q, s;
  T *buf;
  UPCXXA_COPY_DESC_T copy_desc =
    *(UPCXXA_COPY_DESC_T *)copy_desc_ptr;
  
  R = copy_desc.R;
  x = copy_desc.x;

  if (R.is_empty()) {
    *array_data_size = 0;
    return (T *) allocated;
  }
  p = R.min();
  q = R.upb();
  s = R.raw_stride();
  for (i = 0; i < N; i++) {
    lo[i] = p[i + 1];
    hi[i] = q[i + 1] - 1;
    stride_[i] = s[i + 1];
    /* Check for unit width in this dimension */
    if (N > 1 && (lo[i] + stride_[i] > hi[i])) {
      UPCXXA_COPY_DESCRIPTOR<UPCXXA_TEMPLATE_ARGS_N_MINUS_1> ndesc;
      ndesc.R = R.slice_internal(i + 1);
      ndesc.x = x.slice_internal(i + 1, lo[i]);
      return
        UPCXXA_PACK<UPCXXA_TEMPLATE_ARGS_N_MINUS_1>(&ndesc,
                                                    array_data_size,
                                                    allocated);
    }
  }
  size = R.size() * sizeof(T);

  if (allocated == NULL || allocated == (void*)-1) {
    buf = (T *)UPCXXA_MALLOC(size);
    if (!buf) {
      std::cerr << "failed to allocate " << size << " bytes in "
                << "array copy" << std::endl;
      abort();
    }
  } else
    buf = (T *)allocated;

  /* 2D case */
 if (N == 2) {
  /*
     Two checks:
     1.  check if elements are contiguous along the last dimension
         if contiguous, sidefactor[N-1] should be 1
     2.  check if the stride of the domain of the array is the same
         as the stride of the intersection domain
  */
  if ((x.side_factors[UPCXXA_BIDX(1)] == 1) &&
      (x.stride[UPCXXA_BIDX(1)] == stride_[UPCXXA_BIDX(1)])){
    cint_t xIncrement;
    cint_t numOfRows;
    cint_t stripSize;
    cint_t stripSizeInBytes;
    T *src;
    T *bufTmp;
    cint_t i;

#if UPCXXA_GLOBAL_ARRAY
    src = UPCXXA_LOCAL_PART(x.A) + x.compute_index("pack()", p);
#else
    src = x.A + x.compute_index("pack()", p);
#endif

    xIncrement = x.side_factors[UPCXXA_BIDX(0)] *
      UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(0)],
                          x.stride[UPCXXA_BIDX(0)]);
      /* divide by stride_[0], because stride_[0] is a multiple of x.stride[0] */
    numOfRows = UPCXXA_PFAST_DIVIDE((hi[UPCXXA_BIDX(0)] -
                                     lo[UPCXXA_BIDX(0)]),
                                    stride_[UPCXXA_BIDX(0)]) + 1; 
    stripSize = UPCXXA_PFAST_DIVIDE((hi[UPCXXA_BIDX(1)] -
                                     lo[UPCXXA_BIDX(1)]),
                                    x.stride[UPCXXA_BIDX(1)]) + 1;
    stripSizeInBytes = stripSize * sizeof(T);
    
    bufTmp = buf;
    
    for (i = 0; i < numOfRows; i++){
      memcpy(bufTmp, src, stripSizeInBytes);
      bufTmp += stripSize;
      src += xIncrement;
    }
    *array_data_size = size;
    return buf;
  }
 }

 if (N == 3) {
  if ((x.side_factors[UPCXXA_BIDX(2)] == 1) &&
      (x.stride[UPCXXA_BIDX(2)] == stride_[UPCXXA_BIDX(2)])){
    cint_t xIncrement;
    cint_t yIncrement;
    cint_t numOfRows;
    cint_t numOfCols;
    cint_t stripSize;
    cint_t stripSizeInBytes;
    T *src;
    T *rowStartPtr;
    T *bufTmp;
    cint_t i, j;

#if UPCXXA_GLOBAL_ARRAY
    src = UPCXXA_LOCAL_PART(x.A) + x.compute_index("pack()", p);
#else
    src = x.A + x.compute_index("pack()", p);
#endif

    xIncrement = x.side_factors[UPCXXA_BIDX(0)] *
      UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(0)],
                          x.stride[UPCXXA_BIDX(0)]);
    yIncrement = x.side_factors[UPCXXA_BIDX(1)] *
      UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(1)],
                          x.stride[UPCXXA_BIDX(1)]);
    numOfRows = UPCXXA_PFAST_DIVIDE((hi[UPCXXA_BIDX(0)] -
                                     lo[UPCXXA_BIDX(0)]),
                                    stride_[UPCXXA_BIDX(0)]) + 1;
    numOfCols = UPCXXA_PFAST_DIVIDE((hi[UPCXXA_BIDX(1)] -
                                     lo[UPCXXA_BIDX(1)]),
                                    stride_[UPCXXA_BIDX(1)]) + 1;
    stripSize = UPCXXA_PFAST_DIVIDE((hi[UPCXXA_BIDX(2)] -
                                     lo[UPCXXA_BIDX(2)]),
                                    x.stride[UPCXXA_BIDX(2)]) + 1;
    stripSizeInBytes = stripSize * sizeof(T);
    
    bufTmp = buf;
    
    for (i = 0; i < numOfRows; i++){
      rowStartPtr = src;
      for (j = 0; j < numOfCols; j++){
        memcpy(bufTmp, src, stripSizeInBytes);
        bufTmp += stripSize;
        src += yIncrement;
      }
      src = rowStartPtr + xIncrement;
    }
    *array_data_size = size;
    return buf;
  }
 }

  idx = 0;
  UPCXXA_FORALL(e, x, lo, hi, stride_, 
                UPCXXA_DEREF(buf[idx++],e);,
                UPCXXA_DEREF_LOCAL(buf[idx++],e););
  *array_data_size = size;
  return buf;
}

/* Unpacks the given buffer into the array with the given copy
   descriptor. */
UPCXXA_TEMPLATE void UPCXXA_UNPACK(UPCXXA_COPY_DESC_T *copy_desc_ptr,
                                   T *buf) {
  int i;
  cint_t lo[N], hi[N], stride_[N];
  size_t size, idx;
  point<N> p, q, s;
  UPCXXA_ARRAY x = copy_desc_ptr->x;
  rectdomain<N> R = copy_desc_ptr->R;
  T tmp;

  UPCXXA_ASSERT_EXISTS(x, "(in unpack method)");
  
  if (R.is_empty()) return;
  p = R.min();
  q = R.upb();
  s = R.raw_stride();
  for (i = 0; i < N; i++) {
    lo[i] = p[i + 1];
    hi[i] = q[i + 1] - 1;
    stride_[i] = s[i + 1];
    /* Check for unit width in this dimension */
    if (N > 1 && (lo[i] + stride_[i] > hi[i])) {
      UPCXXA_COPY_DESCRIPTOR<UPCXXA_TEMPLATE_ARGS_N_MINUS_1> ndesc;
      ndesc.R = R.slice_internal(i + 1);
      ndesc.x = x.slice_internal(i + 1, lo[i]);
      UPCXXA_UNPACK<UPCXXA_TEMPLATE_ARGS_N_MINUS_1>(&ndesc, buf);
      return;
    }
  }

  /* 2D case */
 if (N == 2) {
  /*
     Two checks:
     1.  check if elements are contiguous along the last dimension
         if contiguous, sidefactor[N-1] should be 1
     2.  check if the stride of the domain of the array is the same
         as the stride of the intersection domain
  */
  if ((x.side_factors[UPCXXA_BIDX(1)] == 1) &&
      (x.stride[UPCXXA_BIDX(1)] == stride_[UPCXXA_BIDX(1)])) {
      cint_t xIncrement;
      cint_t numOfRows;
      cint_t stripSize;
      cint_t stripSizeInBytes;
      T *dest;
      cint_t i;

#if UPCXXA_GLOBAL_ARRAY
      dest = UPCXXA_LOCAL_PART(x.A) + x.compute_index("unpack()", p);
#else
      dest = x.A + x.compute_index("unpack()", p);
#endif

      xIncrement = x.side_factors[UPCXXA_BIDX(0)] *
        UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(0)],
                            x.stride[UPCXXA_BIDX(0)]);
      numOfRows = UPCXXA_PFAST_DIVIDE((hi[UPCXXA_BIDX(0)] -
                                       lo[UPCXXA_BIDX(0)]),
                                      stride_[UPCXXA_BIDX(0)]) + 1;
      stripSize = UPCXXA_PFAST_DIVIDE((hi[UPCXXA_BIDX(1)] -
                                       lo[UPCXXA_BIDX(1)]),
                                      x.stride[UPCXXA_BIDX(1)]) + 1;
      stripSizeInBytes = stripSize * sizeof(T);

      for (i = 0; i < numOfRows; i++){
          memcpy(dest, buf, stripSizeInBytes);
          buf += stripSize;
          dest += xIncrement;
      }
      return;
  }
 }

 if (N == 3) {
  if ((x.side_factors[UPCXXA_BIDX(2)] == 1) &&
      (x.stride[UPCXXA_BIDX(2)] == stride_[UPCXXA_BIDX(2)])) {
    cint_t xIncrement;
    cint_t yIncrement;
    cint_t numOfRows;
    cint_t numOfCols;
    cint_t stripSize;
    cint_t stripSizeInBytes;
    T *dest;
    T *rowStartPtr;
    cint_t i, j;

#if UPCXXA_GLOBAL_ARRAY
    dest = UPCXXA_LOCAL_PART(x.A) + x.compute_index("unpack()", p);
#else
    dest = x.A + x.compute_index("unpack()", p);
#endif

    xIncrement = x.side_factors[UPCXXA_BIDX(0)] *
      UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(0)],
                          x.stride[UPCXXA_BIDX(0)]);   
    yIncrement = x.side_factors[UPCXXA_BIDX(1)] *
      UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(1)],
                          x.stride[UPCXXA_BIDX(1)]);
    numOfRows = UPCXXA_PFAST_DIVIDE((hi[UPCXXA_BIDX(0)] -
                                     lo[UPCXXA_BIDX(0)]),
                                    stride_[UPCXXA_BIDX(0)]) + 1;
    numOfCols = UPCXXA_PFAST_DIVIDE((hi[UPCXXA_BIDX(1)] -
                                     lo[UPCXXA_BIDX(1)]),
                                    stride_[UPCXXA_BIDX(1)]) + 1;
    stripSize = UPCXXA_PFAST_DIVIDE((hi[UPCXXA_BIDX(2)] -
                                     lo[UPCXXA_BIDX(2)]),
                                    x.stride[UPCXXA_BIDX(2)]) + 1;
    stripSizeInBytes = stripSize * sizeof(T);
    
    for (i = 0; i < numOfRows; i++){
        rowStartPtr = dest;
        for (j = 0; j < numOfCols; j++){
            memcpy(dest, buf, stripSizeInBytes);
            buf += stripSize;
            dest += yIncrement;
        }
        dest = rowStartPtr + xIncrement;
    }
    return;
  }
 }

  idx = 0;
#if 0
  UPCXXA_FORALL(e, x, lo, hi, stride_,
                UPCXXA_WEAK_ASSIGN(e, buf[idx++]););
  UPCXXA_SYNC(); /* this is bogus - can't run UPCXXA_SYNC() within the context of an AM handler */
#else
  assert(UPCXXA_DIRECTLY_ADDRESSABLE(x.A)); /* unpack can _only_ be used to unpack data into a local array */
  UPCXXA_FORALL(e, x, lo, hi, stride_, 
         UPCXXA_ASSIGN(e, buf[idx++]);,
         UPCXXA_ASSIGN_LOCAL(e, buf[idx++]););
#endif
}
#undef return
#endif /* UPCXXA_ASSIGNABLE */
/* ------------------------------------------------------------------------------------ */
#if defined(UPCXXA_TRACE) || !defined(NDEBUG)
 #define UPCXXA_DEBUG_COPY_ENABLED 1
 static const char *UPCXXA_DEBUG_ARRAYCOPY;
 #define UPCXXA__DEBUG_COPY(args,sz) do {                        \
    size_t _ti_copy_sz = (sz);                                   \
    if (_ti_copy_sz == 0) { /* force eval lazily computed sz */  \
      _ti_copy_sz = R.size() * sizeof(T);                        \
    } else if (_ti_copy_sz == (size_t)-1) {                      \
      _ti_copy_sz = 0;                                           \
    }                                                            \
    UPCXXA_if_pf (UPCXXA_DEBUG_ARRAYCOPY == 0)                   \
      UPCXXA_DEBUG_ARRAYCOPY =                                   \
      UPCXXA_GETENV_MASTER("TI_DEBUG_ARRAY_COPY")+1;             \
    UPCXXA_if_pf (UPCXXA_DEBUG_ARRAYCOPY != (char*)1) {          \
      printf("%i: ", (int)UPCXXA_MYPROC);                        \
      printf args;                                               \
      printf("\n"); fflush(stdout);                              \
    }                                                            \
    UPCXXA_TRACE_PRINTF(args);                                   \
  } while(0)
#else
 #define UPCXXA_DEBUG_COPY_ENABLED 0
 #define UPCXXA__DEBUG_COPY(args,sz) ((void)0)
#endif
#define UPCXXA_DEBUG_COPY(str, sz)                              \
   UPCXXA__DEBUG_COPY(("TI_ARRAY_COPY: (%llu bytes) %s",        \
                       (unsigned long long)_ti_copy_sz, str),   \
                      sz)
#define UPCXXA_FAILED_NONBLOCKING_WARNING()                     \
   UPCXXA__DEBUG_COPY(("TI_ARRAY_COPY: Next TiArray.copy() was" \
                       " issued as non-blocking,"               \
                       " but was forced to be fully blocking"), \
                      -1)

/* a smart memcpy operation that correctly handles overlap */ 
#ifndef UPCXXA_SMART_MEMCPY_DEFINED
#define UPCXXA_SMART_MEMCPY_DEFINED
static void smart_memcpy(void *dest, void *src, size_t num_bytes) {
  assert(num_bytes >= 0);
  if (num_bytes == 0) return;
  else {
    if (((char *)src) + num_bytes <= ((char *)dest) || 
        ((char *)dest) + num_bytes <= ((char *)src))
      memcpy(dest, src, num_bytes);
    else 
      memmove(dest, src, num_bytes); /* handles overlap correctly */
  }
}
#endif
/* ------------------------------------------------------------------------------------ */
#if UPCXXA_ASSIGNABLE
UPCXXA_ARRAYSIG(void) copy_inner(const UPCXXA_ARRAY &y,
                                 UPCXXA_EVENT_T cevent) const {
  rectdomain<N> R;
  size_t R_numelements = 0;

  /* safety checks */
  UPCXXA_ASSERT_EXISTS((*this), "(in copy method)");
  UPCXXA_ASSERT_EXISTS(y, "(in copy method)");

  /* check for a simple case, where both arrays are the same array (possibly restricted)
   * this is always an identity transformation, regardless of the rectdomain intersection
   */
  if (UPCXXA_EQUAL(A, y.A)) {
    /* we need to check not only the array data ptr, but also the base, stride and sidefactors */
    assert(&(base[0]) == &(stride[0])+N && (cint_t*)&(side_factors[0]) == &(stride[0])+2*N);
    if (!memcmp(stride, y.stride, sizeof(stride)+sizeof(base)+sizeof(side_factors))) {
      UPCXXA_DEBUG_COPY("skipping identity copy of array to itself",
                        -1);
      return;
    }
  }

  R = y._domain * _domain;
  if (R.is_empty()) {
    UPCXXA_DEBUG_COPY("skipping empty copy (disjoint or non-overlapping domains)",
                      -1);
    return; /* empty copy */
  }

{ 
  #define UPCXXA_DONT_KNOW (-1)
  int iscontiguousx = UPCXXA_DONT_KNOW; /* 1, 0 or UPCXXA_DONT_KNOW indication of x,y contiguity over R */
  int iscontiguousy = UPCXXA_DONT_KNOW;
  UPCXXA_PTR_TO_T xdata; /* pointer to the start of the array data in R, valid iff iscontiguousx,y == 1 */
  UPCXXA_PTR_TO_T ydata;
  /* First we check for the fastest, simplest (and hopefully common) case -
    A) both arrays are contiguous in linear memory over the elements in the domain intersection, 
    and B) they store the corresponding elements of that region in the same order in linear memory
    which implies we can safely do a bulk copy of the data region
    Property B can be checked more efficiently, so start with that.
  */

  /* Property B) we need to check the elements are stored in the same order.
     Because contiguity is being checked separately, we can quickly check for
     correct element ordering by comparing side factors - if they are unequal
     then either we have a row length difference in some dimension, 
     or the dimensional ordering has been permuted (either due to a permutation or
     negative injection), which implies either a violation of property A or B (respectively)
     iff the intersection domain transcends more than a single row in the given dimension 
     (ie iff the region is non-singular in the dimension where the side_factors mismatch)
   */

  /* check for a sideFactor mismatch along a non-singular dimension */
  int i;
  int matchedorder = 1;
  for (i = 0; i < N; i++) {
    if (side_factors[i] != y.side_factors[i] &&
        R.upb()[i+1] > R.min()[i+1] + 1) {
      matchedorder = 0;
      break;
    }
  }
  if (matchedorder &&
      (iscontiguousx = internal_is_contiguous(&R, &R_numelements, &xdata)) && 
      (iscontiguousy = y.internal_is_contiguous(&R, &R_numelements, &ydata))) {
      /* safe to use a direct region copy from ydata to xdata */
      /* get size of contiguous region */
      size_t size = sizeof(T) * R_numelements;

      #if UPCXXA_GLOBAL_ARRAY
        #ifdef UPCXXA_MEMORY_DISTRIBUTED
          if (UPCXXA_IS_DIRECTLY_ADDRESSABLE(A) &&
              UPCXXA_IS_DIRECTLY_ADDRESSABLE(y.A)) {
            UPCXXA_DEBUG_COPY("bulk copying contiguous region (local <- local)",
                              size);
            smart_memcpy(UPCXXA_TO_LOCAL(xdata),
                         UPCXXA_TO_LOCAL(ydata), size);
          } else if (UPCXXA_IS_DIRECTLY_ADDRESSABLE(A)) {
            if (cevent == UPCXXA_EVENT_NONE) {
              UPCXXA_DEBUG_COPY("bulk copying contiguous region (get: local <- remote)",
                                size);
              UPCXXA_BULK_READ(UPCXXA_TO_LOCAL(xdata), ydata, size);
            } else {
              UPCXXA_DEBUG_COPY("async bulk copying contiguous region (get: local <- remote)",
                                size);
	      UPCXXA_GET_NB_BULK(cevent, UPCXXA_TO_LOCAL(xdata),
                                 ydata, size);
	    }
          } else if (UPCXXA_IS_DIRECTLY_ADDRESSABLE(y.A)) {
	    if (cevent == UPCXXA_EVENT_NONE) {
              UPCXXA_DEBUG_COPY("bulk copying contiguous region (put: remote <- local)",
                                size);
	      UPCXXA_BULK_WRITE(xdata, UPCXXA_TO_LOCAL(ydata), size);
	    } else {
	      UPCXXA_DEBUG_COPY("async bulk copying contiguous region (put: remote <- local)",
                                size);
	      UPCXXA_PUT_NB_BULK(cevent, xdata,
                                 UPCXXA_TO_LOCAL(ydata), size);
	    }
          } else {
            unsigned char *buf;
            if (cevent != UPCXXA_EVENT_NONE)
              UPCXXA_FAILED_NONBLOCKING_WARNING();
            UPCXXA_DEBUG_COPY("bulk copying contiguous region (3rd party: remote <- remote)",
                              size);
            buf = (unsigned char *) UPCXXA_MALLOC(size);
            if (!buf) {
              std::cerr << "failed to allocate " << size 
                        << " bytes in " << "array copy" << std::endl;
              abort();
            }
            UPCXXA_BULK_READ(buf, ydata, size);
            UPCXXA_BULK_WRITE(xdata, buf, size);
            UPCXXA_FREE(buf);
          }
        #else
          /* this is shared memory */
          assert(UPCXXA_IS_DIRECTLY_ADDRESSABLE(xdata) &&
                 UPCXXA_IS_DIRECTLY_ADDRESSABLE(ydata)); 
          UPCXXA_DEBUG_COPY("bulk copying contiguous region", size);
          smart_memcpy(UPCXXA_TO_LOCAL(xdata), UPCXXA_TO_LOCAL(ydata),
                       size);
        #endif
      #else
        /* array is local - all we need is memcpy */
        UPCXXA_DEBUG_COPY("bulk copying contiguous region", size);
        smart_memcpy(xdata, ydata, size);
      #endif

      return;
  }

#if UPCXXA_GLOBAL_ARRAY && defined(UPCXXA_COMM_AM2)
  if (!UPCXXA_IS_DIRECTLY_ADDRESSABLE(A) ||
      !UPCXXA_IS_DIRECTLY_ADDRESSABLE(y.A)) { 
    /* we have special AM functions for scatter-gather to optimize
     * non-contiguous copies across memory spaces pack is gather,
     * unpack is scatter
     */
    UPCXXA_COPY_DESC_T copy_desc;
    T *buf;
    int isrowmajorx, isrowmajory;

    copy_desc.R = R;

    /* ensure we have full information -
     * we already know that at least one of the predicates 
     * (iscontiguousx, iscontiguousy, matchedorder) is false, but 
     * we may still be able to take advantage of one array's contiguity
     */
    if (iscontiguousx == UPCXXA_DONT_KNOW)
      iscontiguousx = internal_is_contiguous(&R, &R_numelements,
                                             &xdata);
    if (iscontiguousy == UPCXXA_DONT_KNOW)
      iscontiguousy = y.internal_is_contiguous(&R, &R_numelements,
                                               &ydata);

    /* we actually need something stronger than contiguity and 
     * subtlely different than matchedorder - 
     * need to check the elements are stored in positive row-major order, because 
     * our packing algorithm currently always packs to row-major order according
     * to the intersection rectdomain.
     * We could make this more aggressive by parameterizing the packing 
     * algorithm to pack into an arbitrary dimensional ordering and direction,
     * which would allow us to always pack/unpack with the format matching the
     * contiguous array (even if that array has been permuted or negatively injected)
     */
    if (N == 1) {
      isrowmajorx = (side_factors[UPCXXA_BIDX(0)] > 0);
      isrowmajory = (y.side_factors[UPCXXA_BIDX(0)] > 0);
    } else if (N == 2) {
      isrowmajorx = (side_factors[UPCXXA_BIDX(0)] >
                     side_factors[UPCXXA_BIDX(1)]) && 
                    (side_factors[UPCXXA_BIDX(1)] > 0);
      isrowmajory = (y.side_factors[UPCXXA_BIDX(0)] >
                     y.side_factors[UPCXXA_BIDX(1)]) && 
                    (y.side_factors[UPCXXA_BIDX(1)] > 0);
    } else if (N == 3) {
      isrowmajorx = (side_factors[UPCXXA_BIDX(0)] >
                     side_factors[UPCXXA_BIDX(1)]) && 
                    (side_factors[UPCXXA_BIDX(1)] >
                     side_factors[UPCXXA_BIDX(2)]) && 
                    (side_factors[UPCXXA_BIDX(2)] > 0);
      isrowmajory = (y.side_factors[UPCXXA_BIDX(0)] >
                     y.side_factors[UPCXXA_BIDX(1)]) && 
                    (y.side_factors[UPCXXA_BIDX(1)] >
                     y.side_factors[UPCXXA_BIDX(2)]) && 
                    (y.side_factors[UPCXXA_BIDX(2)] > 0);
    } else {  
      int i;
      cint_t lastx = 0, lasty = 0;
      isrowmajorx = 1; isrowmajory = 1;
      for (i = N-1; i >= 0; i--) {
        if (!(side_factors[i] > lastx)) isrowmajorx = 0;
        if (!(y.side_factors[i] > lasty)) isrowmajory = 0;
        lastx = side_factors[i];
        lasty = y.side_factors[i];
      }
    }

   #if UPCXXA_DEBUG_COPY_ENABLED
    { char msg[255];
      if (cevent != UPCXXA_EVENT_NONE)
        UPCXXA_FAILED_NONBLOCKING_WARNING();
      sprintf(msg,"scatter-gather AM-based copy" 
              "(%s %scontiguous %srow-major <- %s %scontiguous %srow-major)",
              (UPCXXA_IS_DIRECTLY_ADDRESSABLE(A)?"local":"remote"),
              (iscontiguousx?"":"non-"), (isrowmajorx?"":"non-"),
              (UPCXXA_IS_DIRECTLY_ADDRESSABLE(y.A)?"local":"remote"),
              (iscontiguousy?"":"non-"), (isrowmajory?"":"non-"));
      UPCXXA_DEBUG_COPY(msg, R_numelements * sizeof(T));
    }
   #endif

    if (UPCXXA_IS_DIRECTLY_ADDRESSABLE(A)) { /* remote to local copy */
	size_t num_bytes = R_numelements * sizeof(T);

        if (iscontiguousx && isrowmajorx) {
          buf = UPCXXA_LOCAL_PART(xdata); /* fetch directly into x */
        } else {
          buf = (T *)UPCXXA_MALLOC(num_bytes);
          if (!buf) {
            std::cerr << "failed to allocate " << num_bytes << " bytes in "
                      << "array copy" << std::endl;
            abort();
          }
        }

        if (iscontiguousy && isrowmajory) {
          UPCXXA_BULK_READ(buf, ydata, num_bytes);
        } else {
          copy_desc.x = y;
          UPCXXA_GET_ARRAY((void *)UPCXXA_PACK<UPCXXA_TEMPLATE_ARGS>,
                           &copy_desc, sizeof(copy_desc),
                           UPCXXA_TO_BOX(y.A), (void *)buf);
          UPCXXA_SYNC();
        }

        if (!(iscontiguousx && isrowmajorx)) {
	  copy_desc.x = (*this);
	  UPCXXA_UNPACK<UPCXXA_TEMPLATE_ARGS>(&copy_desc, buf);
          UPCXXA_FREE(buf);
        }
	return;
      }
    else if (UPCXXA_IS_DIRECTLY_ADDRESSABLE(y.A)) { /* local to remote copy */
	size_t num_bytes;
        bool new_buf = false;

	if (iscontiguousy && isrowmajory) {
          buf = UPCXXA_LOCAL_PART(ydata); /* push directly from y */
          num_bytes = R_numelements * sizeof(T);
        } else {
          copy_desc.x = y;
          buf = UPCXXA_PACK<UPCXXA_TEMPLATE_ARGS>(&copy_desc,
                                                  &num_bytes, NULL);
          new_buf = true;
        }

	if (iscontiguousx && isrowmajorx) {
          if (cevent == UPCXXA_EVENT_NONE || new_buf) {
            /* for now, default to blocking if temporary buffer used */
            UPCXXA_BULK_WRITE(xdata, buf, num_bytes);
            if (new_buf) {
              UPCXXA_FREE(buf);
            }
          } else {
            UPCXXA_PUT_NB_BULK(cevent, xdata, buf, num_bytes);
          }
        } else {
          copy_desc.x = (*this);
          UPCXXA_PUT_ARRAY((void *)UPCXXA_UNPACK<UPCXXA_TEMPLATE_ARGS>,
                           &copy_desc, sizeof(copy_desc), buf,
                           num_bytes, UPCXXA_TO_BOX(A), cevent,
                           new_buf);
          UPCXXA_SYNC();
        }

	return;
      }
    else { /* remote to remote copy */
        size_t num_bytes = R_numelements * sizeof(T);

        buf = (T *)UPCXXA_MALLOC(num_bytes);
        if (!buf) {
          std::cerr << "failed to allocate " << num_bytes << " bytes in "
                    << "array copy" << std::endl;
          abort();
        }

        if (iscontiguousy && isrowmajory) {
          UPCXXA_BULK_READ(buf, ydata, num_bytes);
        } else {
          copy_desc.x = y;
          UPCXXA_GET_ARRAY((void *)UPCXXA_PACK<UPCXXA_TEMPLATE_ARGS>,
                           &copy_desc, sizeof(copy_desc),
                           UPCXXA_TO_BOX(y.A), (void *)buf);
          UPCXXA_SYNC();
        }

        if (iscontiguousx && isrowmajorx) {
          UPCXXA_BULK_WRITE(xdata, buf, num_bytes);
          UPCXXA_FREE(buf);
        } else {
          copy_desc.x = (*this);
          UPCXXA_PUT_ARRAY((void *)UPCXXA_UNPACK<UPCXXA_TEMPLATE_ARGS>,
                           &copy_desc, sizeof(copy_desc), buf,
                           num_bytes, UPCXXA_TO_BOX(A), cevent, true);
          UPCXXA_SYNC();
        }

        return;
      }
  }
#endif /* UPCXXA_GLOBAL_ARRAY && AM2 */

  { const int arraysMay_Overlap = may_overlap(y,R);
    int i;
    cint_t lo[N], hi[N], stride_[N];
    point<N> const p = R.min();
    point<N> const q = R.upb();
    point<N> const s = R.raw_stride();
    for (i = 0; i < N; i++) {
      lo[i] = p[i + 1];
      hi[i] = q[i + 1] - 1;
      stride_[i] = s[i + 1];
    }

    /* if both arrays are local and last dimension is contiguous on both 
       arrays, use memcpy along the last dimension for higher bandwidth 
       check if elements are contiguous along the last dimension
         if contiguous, sidefactor[N-1] should be 1
       check if the stride of the domain of the array is the same
         as the stride of the intersection domain
     */
    if (UPCXXA_DIRECTLY_ADDRESSABLE(A) &&
        UPCXXA_DIRECTLY_ADDRESSABLE(y.A) &&
        !arraysMay_Overlap &&
        (side_factors[N-1] == 1) && (y.side_factors[N-1] == 1) &&
        (stride[N-1] == stride_[N-1]) && (y.stride[N-1] == stride_[N-1])) {
        T *src;
        T *dst;

        UPCXXA_DEBUG_COPY("local non-contiguous copy with memcpy in last dimension",
                          R_numelements * sizeof(T));

        #if UPCXXA_GLOBAL_ARRAY
          src = UPCXXA_LOCAL_PART(y.A) + y.compute_index("copy()", p);
          dst = UPCXXA_LOCAL_PART(A) + compute_index("copy()", p);
        #else
          src = y.A + y.compute_index("copy()", p);
          dst = A + compute_index("copy()", p);
        #endif

        if (N == 1)
          abort(); /* N == 1 should never make it this far */
        else if (N == 2)
          { cint_t i;
            const cint_t xIncrement0 =
              side_factors[UPCXXA_BIDX(0)] *
              UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(0)],
                                  stride[UPCXXA_BIDX(0)]);
            const cint_t yIncrement0 =
              y.side_factors[UPCXXA_BIDX(0)] *
              UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(0)],
                                  y.stride[UPCXXA_BIDX(0)]);
              /* divide by stride_[0], because stride_[0] is a multiple of stride[0] */
            const cint_t numOfRows =
              UPCXXA_PFAST_DIVIDE((hi[UPCXXA_BIDX(0)] -
                                   lo[UPCXXA_BIDX(0)]),
                                  stride_[UPCXXA_BIDX(0)]) + 1;
            const cint_t stripSizeInBytes =
              (UPCXXA_PFAST_DIVIDE((hi[UPCXXA_BIDX(1)] -
                                    lo[UPCXXA_BIDX(1)]),
                                   stride_[UPCXXA_BIDX(1)]) + 1) *
              sizeof(T);

            for (i = numOfRows; i; i--){
              memcpy(dst, src, stripSizeInBytes);
              src += yIncrement0;
              dst += xIncrement0;
            }
            return;
          }
        else if (N == 3)
          { cint_t i, j;
            const cint_t xIncrement0 =
              side_factors[UPCXXA_BIDX(0)] *
              UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(0)],
                                  stride[UPCXXA_BIDX(0)]);
            const cint_t xIncrement1 =
              side_factors[UPCXXA_BIDX(1)] *
              UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(1)],
                                  stride[UPCXXA_BIDX(1)]);
            const cint_t yIncrement0 =
              y.side_factors[UPCXXA_BIDX(0)] *
              UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(0)],
                                  y.stride[UPCXXA_BIDX(0)]);
            const cint_t yIncrement1 =
              y.side_factors[UPCXXA_BIDX(1)] *
              UPCXXA_PFAST_DIVIDE(stride_[UPCXXA_BIDX(1)],
                                  y.stride[UPCXXA_BIDX(1)]);
            const cint_t numOfRows =
              UPCXXA_PFAST_DIVIDE((hi[UPCXXA_BIDX(0)] -
                                   lo[UPCXXA_BIDX(0)]),
                                  stride_[UPCXXA_BIDX(0)]) + 1;
            const cint_t numOfCols =
              UPCXXA_PFAST_DIVIDE((hi[UPCXXA_BIDX(1)] -
                                   lo[UPCXXA_BIDX(1)]),
                                  stride_[UPCXXA_BIDX(1)]) + 1;
            const cint_t stripSizeInBytes =
              (UPCXXA_PFAST_DIVIDE((hi[UPCXXA_BIDX(2)] -
                                    lo[UPCXXA_BIDX(2)]),
                                   stride_[UPCXXA_BIDX(2)]) + 1) *
              sizeof(T);

            for (i = numOfRows; i; i--){
              T * const srcrowStartPtr = src;
              T * const dstrowStartPtr = dst;
              for (j = numOfCols; j; j--){
                memcpy(dst, src, stripSizeInBytes);
                src += yIncrement1;
                dst += xIncrement1;
              }
              src = srcrowStartPtr + yIncrement0;
              dst = dstrowStartPtr + xIncrement0;
            }
            return;
          }
    }

    /* finally, the slow, general case that handles anything */
    /* Do an el-by-el copy. */
    if (arraysMay_Overlap) { /* areas might overlap - use an intermediate to be sure */
      size_t const num_bytes = R_numelements * sizeof(T);
      T *buf = (T *)UPCXXA_MALLOC(num_bytes);
      if (!buf) {
        std::cerr << "failed to allocate " << num_bytes << " bytes in "
                  << "array copy" << std::endl;
        abort();
      }
      size_t i=0;

      UPCXXA_DEBUG_COPY("element-wise generalized with-overlap naive copy",
                        num_bytes);
      UPCXXA_FORALL(f, y, lo, hi, stride_, 
                    UPCXXA_DEREF(buf[i++], f),
                    UPCXXA_DEREF_LOCAL(buf[i++], f));
      i=0;
      UPCXXA_FORALL(e, (*this), lo, hi, stride_, 
                    UPCXXA_WEAK_ASSIGN(e, buf[i++]),
                    UPCXXA_WEAK_ASSIGN_LOCAL(e, buf[i++]));
      UPCXXA_SYNC();
      UPCXXA_FREE(buf);
    }
    else { /* areas do not overlap */

      /* We're going to do an element-by-element copy - any ordering will work,
       * but we'll get better cache performance if we follow the memory contiguity
       * of one or both arrays. Additionally, we give preference to the contiguity
       * direction of the array being written, since this gets better performance
       * on write-back caches. As a secondary heuristic (for 3D), if both arrays have a
       * contiguous direction (and they differ), then those two directions should
       * be the inner two directions in the loop nest.
       *
       * TODO: we'd get better performance on non-contiguous arrays with a heuristic 
       * that gives preference to the direction with the lowest sideFactor (assuming 
       * it's small enough to fit two elements in a cache line) - the current heuristic
       * uses an arbitrarily chosen order when no side_factors are 1. However, favoring the
       * min sidefactor entails greater heuristic overhead to calculate the right traversal.
       */

      #define UPCXXA_DEBUG_COPY_ORDERED(x,y,dir,dirall) do {            \
        char msg[255];                                                  \
        sprintf(msg,"element-wise generalized no-overlap copy, "        \
                "%i arrs matching cache direction %x",                  \
                (side_factors[dir] == 1) + (y.side_factors[dir] == 1),  \
                dirall);                                                \
        UPCXXA_DEBUG_COPY(msg, R_numelements * sizeof(T));              \
      } while (0)

      if (N == 2)
      { int dir = 0;
        T tmp;
        const int m0 = UPCXXA_BIDX(0);
        const int m1 = UPCXXA_BIDX(1);
        if (side_factors[UPCXXA_BIDX(1)] == 1) dir = 1;
        else if (side_factors[UPCXXA_BIDX(0)] == 1) dir = 0;
        else if (y.side_factors[UPCXXA_BIDX(1)] == 1) dir = 1;
        else if (y.side_factors[UPCXXA_BIDX(0)] == 1) dir = 0;
        else dir = 1;

        if (dir == 1) {
            UPCXXA_DEBUG_COPY_ORDERED((*this),y,m1,0x01);
            UPCXXA_FORALL2ORDERED2(e, (*this), f, y, lo, hi, stride_,
                                   p, m0, m1, UPCXXA_DEREF(tmp, f);
                                   UPCXXA_WEAK_ASSIGN(e, tmp),
                                   *e = *f);
            UPCXXA_SYNC();
        } else {
            UPCXXA_DEBUG_COPY_ORDERED((*this),y,m0,0x10);
            UPCXXA_FORALL2ORDERED2(e, (*this), f, y, lo, hi, stride_,
                                   p, m1, m0, UPCXXA_DEREF(tmp, f);
                                   UPCXXA_WEAK_ASSIGN(e, tmp),
                                   *e = *f);
            UPCXXA_SYNC();
        }
      }
      else if (N == 3)
      { int dir = 0;
        T tmp;
        const int m0 = UPCXXA_BIDX(0);
        const int m1 = UPCXXA_BIDX(1);
        const int m2 = UPCXXA_BIDX(2);
        if (side_factors[UPCXXA_BIDX(2)] == 1) {
          if (y.side_factors[UPCXXA_BIDX(0)] == 1) dir = 0x102;
          else dir = 0x012;
        } else if (side_factors[UPCXXA_BIDX(1)] == 1) {
          if (y.side_factors[UPCXXA_BIDX(0)] == 1) dir = 0x201;
          else dir = 0x021;
        } else if (side_factors[UPCXXA_BIDX(0)] == 1) {
          if (y.side_factors[UPCXXA_BIDX(1)] == 1) dir = 0x210;
          else dir = 0x120;
        } else if (y.side_factors[UPCXXA_BIDX(2)] == 1) dir = 0x012;
        else if (y.side_factors[UPCXXA_BIDX(1)] == 1) dir = 0x021;
        else if (y.side_factors[UPCXXA_BIDX(0)] == 1) dir = 0x120;
        else dir = 0x012;

        UPCXXA_DEBUG_COPY_ORDERED((*this),y,(dir&0xF),dir);
        #define UPCXXA_CASE(dir0,dir1,dir2)                                \
          case 0x##dir0##dir1##dir2:                                       \
            UPCXXA_FORALL2ORDERED3(e, (*this), f, y, lo, hi, stride_, p,   \
                    m ## dir0, m ## dir1, m ## dir2,                       \
                    UPCXXA_DEREF(tmp, f); UPCXXA_WEAK_ASSIGN(e, tmp),      \
                    *e = *f);                                              \
            break
        switch (dir) {
          UPCXXA_CASE(0,1,2);
          UPCXXA_CASE(1,0,2);
          UPCXXA_CASE(0,2,1);
          UPCXXA_CASE(2,0,1);
          UPCXXA_CASE(1,2,0);
          UPCXXA_CASE(2,1,0);
          default: abort();
        } 
        #undef UPCXXA_CASE
        UPCXXA_SYNC();
      }
      else
      { T tmp;
        UPCXXA_DEBUG_COPY("element-wise generalized no-overlap naive copy",
                          R_numelements * sizeof(T));
        UPCXXA_FORALL2(e, (*this), f, y, lo, hi, stride_, 
                       UPCXXA_DEREF(tmp, f); UPCXXA_WEAK_ASSIGN(e, tmp),
                       *e = *f);
        UPCXXA_SYNC();
      }

      #undef UPCXXA_DEBUG_COPY_ORDERED
    }
    return;
  }
}
}
#undef return
#endif /* UPCXXA_ASSIGNABLE */
#undef UPCXXA_DEBUG_COPY
#undef UPCXXA__DEBUG_COPY
/* ------------------------------------------------------------------------------------ */
#if UPCXXA_ASSIGNABLE
UPCXXA_ARRAYSIG(void) gather(const UPCXXA_ARRAY_T<T, 1> &packedArray, 
                             const UPCXXA_ARRAY_T<point<N>, 1> &ptArray) const {
  size_t numpts = 0; 
  UPCXXA_BOX_T src_box = UPCXXA_BOX_PART(A);
  UPCXXA_ARRAY_T<T, 1> lc_packedArray; 
  UPCXXA_ARRAY_T<point<N>, 1> lc_ptArray;
  point<N> *ptList = NULL;
  T **addrList = NULL;
  T *destList = NULL;

  /* safety checks */
  UPCXXA_ASSERT_EXISTS((*this), "(in gather method)");
  UPCXXA_ASSERT_EXISTS(packedArray, "(in gather method)");
  UPCXXA_ASSERT_EXISTS(ptArray, "(in gather method)");

  numpts = ptArray._domain.size();
  if (numpts == 0) return; /* nothing to do */

  addrList = (T**)UPCXXA_MALLOC(numpts * sizeof(T*));
  if (!addrList) {
    std::cerr << "failed to allocate " << (numpts * sizeof(T*))
              << " bytes in " << "array gather" << std::endl;
    abort();
  }

  /* build local & contiguous versions of params(with same domains), if necessary */
  lc_ptArray = ptArray.make_local_and_contiguous();
  lc_packedArray = packedArray.make_local_and_contiguous();

  assert(UPCXXA_DIRECTLY_ADDRESSABLE(lc_ptArray.A));
  assert(UPCXXA_DIRECTLY_ADDRESSABLE(lc_packedArray.A));
  ptList =
    (point<N>*)UPCXXA_LOCAL_PART(lc_ptArray.get_array_data_ptr());
  destList = UPCXXA_LOCAL_PART(lc_packedArray.get_array_data_ptr());  

  { /* build list of addresses and do array-bounds check */
    T *baseAddr = UPCXXA_LOCAL_PART(A);
    if (UPCXXA_BOUNDS_CHECKING == 0) {
      T *alteredBaseAddr = NULL;
      if (N == 1) {
        if ((stride[UPCXXA_BIDX(0)] == 1) &&
            (side_factors[UPCXXA_BIDX(0)] == 1))
          alteredBaseAddr = baseAddr - base[UPCXXA_BIDX(0)];
      } else if (N == 2) {
        if ((stride[UPCXXA_BIDX(0)] == 1) &&
            (stride[UPCXXA_BIDX(1)] == 1) &&
            (side_factors[UPCXXA_BIDX(1)] == 1))
          alteredBaseAddr = baseAddr -
            (base[UPCXXA_BIDX(0)]*side_factors[UPCXXA_BIDX(0)] +
             base[UPCXXA_BIDX(1)]);
      } else if (N == 3) {
        if ((stride[UPCXXA_BIDX(0)] == 1) &&
            (stride[UPCXXA_BIDX(1)] == 1) &&
            (stride[UPCXXA_BIDX(2)] == 1) &&
            (side_factors[UPCXXA_BIDX(2)] == 1))
          alteredBaseAddr = baseAddr -
            (base[UPCXXA_BIDX(0)]*side_factors[UPCXXA_BIDX(0)] +
             base[UPCXXA_BIDX(1)]*side_factors[UPCXXA_BIDX(1)] +
             base[UPCXXA_BIDX(2)]);
      } else { /* N > 3 */
        if ((stride[N-1] == 1) && (side_factors[N-1] == 1)) {
          alteredBaseAddr = baseAddr - base[N-1];
          for (int i = 0; i < N-1; i++) {
            if (stride[i] == 1) {
              alteredBaseAddr -= (base[i]*side_factors[i]);
            } else {
              alteredBaseAddr = NULL;
              break;
            }
          }
        }
      }
      if (alteredBaseAddr) {
        for (size_t i=0; i < numpts; i++) {
          UPCXXA_ARRAY_FASTBESTCONV(N, addrList[i], alteredBaseAddr,
                                    (*this), ptList[i]);
        }
      } else {
        for (size_t i=0; i < numpts; i++) {
          UPCXXA_ARRAY_FASTCONV(N, addrList[i], baseAddr, (*this),
                                ptList[i]);
        }
      }
    } else { /* UPCXXA_BOUNDS_CHECKING */
      for (size_t i=0; i < numpts; i++) {
      #if UPCXXA_BOUNDS_CHECKING
        addrList[i] = baseAddr + compute_index("gather()", ptList[i]);
      #else
        UPCXXA_ARRAY_FASTCONV(N, addrList[i], baseAddr, (*this),
                              ptList[i]);
      #endif
      }
    }
  }

  /* is the src simply local? (or are we missing AM?) - use a simple copy */
  #ifdef UPCXXA_COMM_AM2
    if (UPCXXA_DIRECTLY_ADDRESSABLE(A))  
  #endif
  {
    size_t i;
    for (i=0; i < numpts; i++) {
      #if UPCXXA_GLOBAL_ARRAY     
        UPCXXA_PTR_TO_T p;
        UPCXXA_TO_GLOBALB(p, src_box, addrList[i]);
        UPCXXA_DEREF(destList[i], p);
      #else
        UPCXXA_DEREF(destList[i], addrList[i]);
      #endif
    }
  } 
  #ifdef UPCXXA_COMM_AM2
    else {  /* use AM to do a fast remote gather */
      UPCXXA_SPARSE_GATHER(destList, (void **)addrList, src_box,
                           numpts, sizeof(T));
    }
  #endif

  /* copy back & free temps */
  if (!UPCXXA_EQUAL(lc_packedArray.A, packedArray.A)) { /* need to copy back result */
    packedArray.copy_inner(lc_packedArray, UPCXXA_EVENT_NONE);

    assert(UPCXXA_DIRECTLY_ADDRESSABLE(lc_packedArray.A));
    lc_packedArray.destroy();
  }
  if (!UPCXXA_EQUAL(lc_ptArray.A, ptArray.A)) {
    assert(UPCXXA_DIRECTLY_ADDRESSABLE(lc_ptArray.A));
    lc_ptArray.destroy();
  }

  UPCXXA_FREE(addrList);
}
#endif /* UPCXXA_ASSIGNABLE */
/* ------------------------------------------------------------------------------------ */
#if UPCXXA_ASSIGNABLE
UPCXXA_ARRAYSIG(void) scatter(const UPCXXA_ARRAY_T<T, 1> &packedArray, 
                              const UPCXXA_ARRAY_T<point<N>, 1> &ptArray) const {
  size_t numpts = 0; 
  UPCXXA_BOX_T dest_box = UPCXXA_BOX_PART(A);
  UPCXXA_ARRAY_T<T, 1> lc_packedArray; 
  UPCXXA_ARRAY_T<point<N>, 1> lc_ptArray;
  point<N> *ptList = NULL;
  T **addrList = NULL;
  T *srcList = NULL;

  /* safety checks */
  UPCXXA_ASSERT_EXISTS((*this), "(in scatter method)");
  UPCXXA_ASSERT_EXISTS(packedArray, "(in scatter method)");
  UPCXXA_ASSERT_EXISTS(ptArray, "(in scatter method)");

  numpts = ptArray._domain.size();
  if (numpts == 0) return; /* nothing to do */

  addrList = (T**)UPCXXA_MALLOC(numpts * sizeof(T*));
  if (!addrList) {
    std::cerr << "failed to allocate " << (numpts * sizeof(T*)) 
              << " bytes in " << "array scatter" << std::endl;
    abort();
  }

  /* build local & contiguous versions of params(with same domains), if necessary */
  lc_ptArray = ptArray.make_local_and_contiguous();
  lc_packedArray = packedArray.make_local_and_contiguous();

  assert(UPCXXA_DIRECTLY_ADDRESSABLE(lc_ptArray.A));
  assert(UPCXXA_DIRECTLY_ADDRESSABLE(lc_packedArray.A));
  ptList =
    (point<N> *)UPCXXA_LOCAL_PART(lc_ptArray.get_array_data_ptr());  
  srcList = UPCXXA_LOCAL_PART(lc_packedArray.get_array_data_ptr());  

  { /* build list of addresses and do array-bounds check */
    T *baseAddr = UPCXXA_LOCAL_PART(A);
    if (UPCXXA_BOUNDS_CHECKING == 0) {
      T *alteredBaseAddr = NULL;
      if (N == 1) {
        if ((stride[UPCXXA_BIDX(0)] == 1) &&
            (side_factors[UPCXXA_BIDX(0)] == 1))
          alteredBaseAddr = baseAddr - base[UPCXXA_BIDX(0)];
      } else if (N == 2) {
        if ((stride[UPCXXA_BIDX(0)] == 1) &&
            (stride[UPCXXA_BIDX(1)] == 1) &&
            (side_factors[UPCXXA_BIDX(1)] == 1))
          alteredBaseAddr = baseAddr -
            (base[UPCXXA_BIDX(0)]*side_factors[UPCXXA_BIDX(0)] +
             base[UPCXXA_BIDX(1)]);
      } else if (N == 3) {
        if ((stride[UPCXXA_BIDX(0)] == 1) &&
            (stride[UPCXXA_BIDX(1)] == 1) &&
            (stride[UPCXXA_BIDX(2)] == 1) &&
            (side_factors[UPCXXA_BIDX(2)] == 1))
          alteredBaseAddr = baseAddr -
            (base[UPCXXA_BIDX(0)]*side_factors[UPCXXA_BIDX(0)] +
             base[UPCXXA_BIDX(1)]*side_factors[UPCXXA_BIDX(1)] +
             base[UPCXXA_BIDX(2)]);
      } else { /* N > 3 */
        if ((stride[N-1] == 1) && (side_factors[N-1] == 1)) {
          alteredBaseAddr = baseAddr - base[N-1];
          for (int i = 0; i < N-1; i++) {
            if (stride[i] == 1) {
              alteredBaseAddr -= (base[i]*side_factors[i]);
            } else {
              alteredBaseAddr = NULL;
              break;
            }
          }
        }
      }
      if (alteredBaseAddr) {
        for (size_t i=0; i < numpts; i++) {
          UPCXXA_ARRAY_FASTBESTCONV(N, addrList[i], alteredBaseAddr,
                                    (*this), ptList[i]);
        }
      } else {
        for (size_t i=0; i < numpts; i++) {
          UPCXXA_ARRAY_FASTCONV(N, addrList[i], baseAddr, (*this),
                                ptList[i]);
        }
      }
    } else { /* UPCXXA_BOUNDS_CHECKING */
      for (size_t i=0; i < numpts; i++) {
      #if UPCXXA_BOUNDS_CHECKING
        addrList[i] = baseAddr + compute_index("gather()", ptList[i]);
      #else
        UPCXXA_ARRAY_FASTCONV(N, addrList[i], baseAddr, (*this),
                              ptList[i]);
      #endif
      }
    }
  }
   
  /* is the dest simply local? (or are we missing AM?) - use a simple copy */
  #ifdef UPCXXA_COMM_AM2
    if (UPCXXA_DIRECTLY_ADDRESSABLE(A))  
  #endif
  {
    size_t i;
    for (i=0; i < numpts; i++) {
      #if UPCXXA_GLOBAL_ARRAY     
        UPCXXA_PTR_TO_T p;
        UPCXXA_TO_GLOBALB(p, dest_box, addrList[i]);
        UPCXXA_WEAK_ASSIGN(p, srcList[i]);
      #else
        UPCXXA_WEAK_ASSIGN(addrList[i], srcList[i]);
      #endif
    }
    UPCXXA_SYNC();
  } 
  #ifdef UPCXXA_COMM_AM2
    else {  /* use AM to do a fast remote scatter */
      UPCXXA_SPARSE_SCATTER((void **)addrList, srcList, dest_box,
                            numpts, sizeof(T));
    }
  #endif

  /* free temps */
  if (!UPCXXA_EQUAL(lc_packedArray.A,packedArray.A)) { 
    assert(UPCXXA_DIRECTLY_ADDRESSABLE(lc_packedArray.A));
    lc_packedArray.destroy();
  }
  if (!UPCXXA_EQUAL(lc_ptArray.A,ptArray.A)) {
    assert(UPCXXA_DIRECTLY_ADDRESSABLE(lc_ptArray.A));
    lc_ptArray.destroy();
  }

  UPCXXA_FREE(addrList);

}
#endif /* UPCXXA_ASSIGNABLE */
/* ------------------------------------------------------------------------------------ */
#if UPCXXA_ASSIGNABLE
UPCXXA_ARRAYSIG(inline void) copy(const UPCXXA_ARRAY &y,
                                  const rectdomain<N> &R) const {
  UPCXXA_ARRAY n = y;
  UPCXXA_ASSERT_EXISTS(y, "(in copy_withrectdomain method)");
  n._domain = y._domain * R;
  copy(n);
}
#endif /* UPCXXA_ASSIGNABLE */
/* ------------------------------------------------------------------------------------ */
#if UPCXXA_ASSIGNABLE
UPCXXA_ARRAYSIG(void) copy(const UPCXXA_ARRAY &y,
                           const UPCXXA_ARRAY_T<point<N>, 1> &ptArray) const {
  size_t numpts = 0; 
  point<N> *ptList = NULL;
  UPCXXA_ARRAY_T<point<N>, 1> lc_ptArray;
  rectdomain<N> R;

  /* safety checks */
  UPCXXA_ASSERT_EXISTS((*this), "(in copy_withptarray method)");
  UPCXXA_ASSERT_EXISTS(y, "(in copy_withptarray method)");
  UPCXXA_ASSERT_EXISTS(ptArray, "(in copy_withptarray method)");

  numpts = ptArray._domain.size();
  if (numpts == 0) return; /* nothing to do */

  /* build local & contiguous versions of params(with same domains), if necessary */
  lc_ptArray = ptArray.make_local_and_contiguous();
  ptList =
    (point<N>*)UPCXXA_LOCAL_PART(lc_ptArray.get_array_data_ptr());  

  if (UPCXXA_DIRECTLY_ADDRESSABLE(A) &&
      UPCXXA_DIRECTLY_ADDRESSABLE(y.A) && 
      (R=(y._domain * _domain), !may_overlap(y,R))) { 
    /* everything is local and non-overlapping - use a direct copy */
    T *baseAddrX = UPCXXA_LOCAL_PART(A);
    T *baseAddrY = UPCXXA_LOCAL_PART(y.A);
    size_t i;
    for (i=0; i < numpts; i++) {
      T *pXelem = baseAddrX +
        compute_index("(in copy_withptarray method)", ptList[i]);
      T *pYelem = baseAddrY +
        y.compute_index("(in copy_withptarray method)", ptList[i]);
      *pXelem = *pYelem;
    }
  } else { /* use scatter gather through a temp */
    UPCXXA_ARRAY_T<T, 1> packedArray(ptArray._domain);

    y.gather(packedArray, lc_ptArray);
    scatter(packedArray, lc_ptArray);

    assert(UPCXXA_DIRECTLY_ADDRESSABLE(packedArray.A));
    packedArray.destroy();
  }

  if (!UPCXXA_EQUAL(lc_ptArray.A, ptArray.A)) {
    assert(UPCXXA_DIRECTLY_ADDRESSABLE(lc_ptArray.A));
    lc_ptArray.destroy();
  }  
}
#endif /* UPCXXA_ASSIGNABLE */
/* ------------------------------------------------------------------------------------ */
#if UPCXXA_ASSIGNABLE
UPCXXA_ARRAYSIG(void) copy(const UPCXXA_ARRAY &y,
                           const upcxx::domain<N> &dom) const {
  
  /* safety checks */
  UPCXXA_ASSERT_EXISTS((*this), "(in copy_withdomain method)");
  UPCXXA_ASSERT_EXISTS(y, "(in copy_withdomain method)");

  #if 0
  UPCXXA_ARRAY_T<rectdomain<N>, 1> rdArray = dom.rectdomain_list();
  #endif

  /* for now, simply punt to the ptArray method
   * future optimizations: detect strongly clustered domains and do a 
   * rectangle-by-rectangle copy using the copy_withrectdomain method
   */
  { 
    size_t size = (size_t) dom.size();
    point<N> start = {0};
    point<N> end = {(cint_t) size}; // warning: possible overflow
    UPCXXA_ARRAY_T<point<N>, 1> ptArray(rectdomain<N>(start, end));
    point<N> *points = dom.point_list();
    memcpy(UPCXXA_TO_LOCAL(ptArray.A), points, size*sizeof(point<N>));
    copy(y, ptArray);
  }
}
#endif /* UPCXXA_ASSIGNABLE */
/* ------------------------------------------------------------------------------------ */
#if UPCXXA_ASSIGNABLE
/*
 * x.set(w) is equivalent to:
 * foreach (p in x._domain()) x[p] = w;
 */
UPCXXA_ARRAYSIG(void) set(T w) const {
  rectdomain<N> R = _domain;
  if (!R.is_empty()) {
    size_t i;
    UPCXXA_PTR_TO_T pdata;
    size_t R_numelements = 0;
    char *p = (char *)&w;
    UPCXXA_ASSERT_EXISTS((*this), "(in set method)");

    for (i=1; i < sizeof(T); i++) { /* see if all bytes are the same */
      if (*p != p[i]) break;
    }
    if (i == sizeof(T) && UPCXXA_DIRECTLY_ADDRESSABLE(A) && 
        internal_is_contiguous(&_domain, &R_numelements, &pdata)) {
      /* use memset when possible */
      memset(UPCXXA_LOCAL_PART(pdata), *p, sizeof(T) * R_numelements);
    }
    else {
      cint_t lo[N], hi[N], stride_[N];
      int i;
      point<N> const p = R.min();
      point<N> const q = R.upb();
      point<N> const s = R.raw_stride();
      for (i = 0; i < N; i++) {
	lo[i] = p[i + 1];
	hi[i] = q[i + 1] - 1;
	stride_[i] = s[i + 1];
      }
  
      UPCXXA_FORALL(e, (*this), lo, hi, stride_, 
                    UPCXXA_ASSIGN(e, w),
                    UPCXXA_ASSIGN_LOCAL(e, w));
    }
  }
}
#endif /* UPCXXA_ASSIGNABLE */
/* ------------------------------------------------------------------------------------ */
UPCXXA_ARRAYSIG(inline UPCXXA_ARRAY) translate(const point<N> &p) const {
  UPCXXA_ARRAY n = (*this);
  int i;
  UPCXXA_ASSERT_EXISTS((*this), "(in translate method)");
  n._domain = _domain.translate(p);
  /*p = n._domain.min();*/
  for (i = 0; i < N; i++)
    n.base[i] += p[i + 1];

#if UPCXXA_DEBUG_ARRAYS
  fprintf(stderr, "p%d: translated array from domain = ",
          UPCXXA_MYPROC);
  UPCXXA_PRINT(_domain, err);  fprintf(stderr, "\n");
  fprintf(stderr, "p%d: translated array to domain = ",
          UPCXXA_MYPROC);
  UPCXXA_PRINT(n._domain, err);  fprintf(stderr, "\n");
  fprintf(stderr, "UPCXXA_ARRAY_T<%d>:\tbase\tstride\tside_factors\n",
          N);
  for (i = 0; i < N; i++) {
    fprintf(stderr, " %d\t\t%d\t%d\t%d\n", i,
	    n.base[i], n.stride[i], n.side_factors[i]);
  }
#endif

  return n;
}
/* ------------------------------------------------------------------------------------ */
#define UPCXXA_PERMUTE_C_ARRAY(src, dest, p) do {        \
  int i_;				          \
  for (i_ = 0; i_ < N; i_++)                      \
     dest[i_] = src[p[i_ + 1] - 1];  \
  } while (0)

UPCXXA_ARRAYSIG(inline UPCXXA_ARRAY) permute(const point<N> &p) const {
  UPCXXA_ARRAY n = (*this);
  UPCXXA_ASSERT_EXISTS((*this), "(in permute method)");
  n._domain = n._domain.permute(p);
  UPCXXA_PERMUTE_C_ARRAY(base, n.base, p);
  UPCXXA_PERMUTE_C_ARRAY(stride, n.stride, p);
  UPCXXA_PERMUTE_C_ARRAY(side_factors, n.side_factors, p);
  return n;
}

#undef UPCXXA_PERMUTE_C_ARRAY
/* ------------------------------------------------------------------------------------ */
UPCXXA_ARRAYSIG(inline UPCXXA_ARRAY) inject(const point<N> &p) const {
  UPCXXA_ARRAY n = (*this);
  int i;
  UPCXXA_ASSERT_EXISTS((*this), "(in inject method)");
  n._domain = _domain * p;
  for (i = 0; i < N; i++) {
    cint_t px = p[i + 1];
    n.base[i] *= px;
    if ((n.stride[i] *= px) < 0) {
      n.stride[i] *= -1;
      n.side_factors[i] *= -1;
    }
  }

#if UPCXXA_DEBUG_ARRAYS
  fprintf(stderr, "p%d: injected array from domain = ",
          UPCXXA_MYPROC);
  UPCXXA_PRINT(_domain, err); fprintf(stderr, "\n");
  fprintf(stderr, "p%d: injected array to domain = ", UPCXXA_MYPROC);
  UPCXXA_PRINT(n._domain, err); fprintf(stderr, "\n");
  fprintf(stderr, "\nArray<%d>:\tbase\tstride\tside_factors\n", N);
  for (i = 0; i < N; i++) {
    fprintf(stderr, " %d\t\t%d\t%d\t%d\n", i,
	    n.base[i], n.stride[i], n.side_factors[i]);
  }
#endif

  return n;
}
/* ------------------------------------------------------------------------------------ */
UPCXXA_ARRAYSIG(inline UPCXXA_ARRAY) project(const point<N> &p) const {
  UPCXXA_ARRAY n = (*this);
  int i;
  UPCXXA_ASSERT_EXISTS((*this), "(in project method)");
  n._domain = _domain / p;
  for (i = 0; i < N; i++) {
    cint_t px = p[i + 1];
    assert(("project() beyond limits", n.base[i] % px == 0));
    n.base[i] /= px;
    assert(("project() beyond limits", n.stride[i] % px == 0));
    if ((n.stride[i] /= px) < 0) {
      n.stride[i] *= -1;
      n.side_factors[i] *= -1;
    }
  }

#if UPCXXA_DEBUG_ARRAYS
  fprintf(stderr, "p%d: projected array from domain = ",
          UPCXXA_MYPROC);
  UPCXXA_PRINT(_domain, err); fprintf(stderr, "\n");
  fprintf(stderr, "p%d: projected array to domain = ", UPCXXA_MYPROC);
  UPCXXA_PRINT(n._domain, err); fprintf(stderr, "\n");
  fprintf(stderr, "\nArray<%d>:\tbase\tstride\tside_factors\n", N);
  for (i = 0; i < N; i++) {
    fprintf(stderr, " %d\t\t%d\t%d\t%d\n", i,
	    n.base[i], n.stride[i], n.side_factors[i]);
  }
#endif

  return n;
}
/* ------------------------------------------------------------------------------------ */
UPCXXA_ARRAYSIG(inline void) bounds_check(const char *where,
                                          const point<N> &p) const {
/* DOB: method must be defined even when not bounds checking, in order
 * to link libraries which use TiArrays and are compiled with bounds
 * checking enabled
 */
#if UPCXXA_BOUNDS_CHECKING
  UPCXXA_ASSERT_EXISTS((*this), where);
  for (int i = N; i >= 1; i--)
    {
      cint_t const q = p[i];
      cint_t const qm = q - base[i-1];
      cint_t const lo = _domain.min()[i];
      cint_t const hi = _domain.upb()[i] - 1;
      cint_t const s = _domain.stride()[i];
      if (s > 1 && ((q - lo) % s) != 0) {
	fprintf(stderr, "UPCXXA_ARRAY_T bounds violation on rank %d\n"
                        "bounds checking error at %s:\n  failed condition: ",
		UPCXXA_MYPROC, where);
	UPCXXA_PRINT(_domain, err);
	fprintf(stderr, ".contains(");
	UPCXXA_PRINT(p, err);
	fprintf(stderr, ")\n");
        abort();
	exit(-2);
      }
      else if (q < lo || q > hi) {
	fprintf(stderr, "bounds checking error at %s:\n  failed condition: ",
		where);
	UPCXXA_PRINT(_domain.min(), err);
	fprintf(stderr, " <= ");
	UPCXXA_PRINT(p, err);
	fprintf(stderr, " <= ");
	UPCXXA_PRINT(_domain.max(), err);
	fprintf(stderr, ")\n");
        abort();
	exit(-2);
      }
      assert(("internal error: bad array descriptor", 
            stride[i-1] == 1 || (qm % stride[i-1]) == 0));
    }
#endif
}
/* ------------------------------------------------------------------------------------ */
#define UPCXXA_DEBUG_ISCONTIGUOUS 0
/* REQUIRES: given domain (*pR) is a subset of *px._domain()
 *    *pnumelements is the number of elements in *pR, or zero
 *
 * EFFECTS: returns true iff all elements in the given domain 
 *          are stored contiguously in memory in array *px
 *    if *pnumelements was zero, it is filled in with the number of elements in *pR
 *    if pRdataptr is non-NULL and the call returns true, then *pRdataptr recieves 
 *       the value of px->get_array_data_ptr_with_domain(pR) (computed more efficiently)
 */
UPCXXA_ARRAYSIG(bool) internal_is_contiguous(const rectdomain<N> *pR,
                                             size_t *pnumelements,
                                             UPCXXA_PTR_TO_T *pRdataptr) const {
  int i;
  size_t minoffset, maxoffset;
  UPCXXA_ARRAY tmp;
  UPCXXA_ARRAY *px = (UPCXXA_ARRAY *)this;
  UPCXXA_ARRAY *_px = (UPCXXA_ARRAY *)this;
  size_t numelements = *pnumelements;
  if (numelements == 0) {
    numelements = pR->size();
    *pnumelements = numelements;
    if (numelements == 0) return 1;
  }

  for (i = 0; i < N; i++) {
    if (px->side_factors[i] < 0) {
      if (_px == px) { tmp = *px; _px = &tmp; }
      _px->side_factors[i] = -_px->side_factors[i];
    }
  }
#ifdef NDEBUG
  maxoffset = _px->compute_index("is_contiguous()", pR->max());
  if (maxoffset + 1 == numelements) { /* minoffset must be zero, no need to compute it */
    if (pRdataptr != NULL) *pRdataptr = px->A;
    return 1;
  }
  minoffset = _px->compute_index("is_contiguous()", pR->min());
  if (maxoffset - minoffset + 1 == numelements) {
    if (pRdataptr != NULL) UPCXXA_INDEX(*pRdataptr, px->A, minoffset);
    return 1;
  } else return 0;
#else
 maxoffset = _px->compute_index("is_contiguous()", pR->max());
 minoffset = _px->compute_index("is_contiguous()", pR->min());
 assert(minoffset >= 0 && maxoffset >= minoffset);
 assert(numelements <= maxoffset - minoffset + 1);
 { int result = (maxoffset - minoffset + 1 == numelements);
  if (result && pRdataptr != NULL)
    UPCXXA_INDEX(*pRdataptr, px->A, minoffset);
  if (pR->is_empty()) {
      assert(result == 1);
      return 1;
  }
  {
  #if 0
    /* this test fails in the presence of negative side_factors */
    size_t numelements = pR->size();
    size_t minoffset = compute_index("is_contiguous()", pR->min());
    size_t maxoffset = compute_index("is_contiguous()", pR->max());
    size_t ref_result = (numelements == maxoffset - minoffset + 1);
    assert(ref_result == result);
    return result;
  #else 
    /* this test handles negative side_factors */
    int i;
    point<N> pointwithminaddr, pointwithmaxaddr;
    point<N> const dommin = pR->min();
    point<N> const domupb = pR->upb();

    #if UPCXXA_DEBUG_ISCONTIGUOUS
      fprintf(stderr, "\n");
      fprintf(stderr, "dommin=");
      UPCXXA_PRINT(dommin, err);
      fprintf(stderr, "\ndomupb=");
      UPCXXA_PRINT(domupb, err);
      fprintf(stderr, "\ndomstride=");
      UPCXXA_PRINT(pR->stride(), err);
      fflush(stderr);
    #endif

    for (i = 0; i < N; i++) {
      cint_t const lo = dommin[i + 1];
      cint_t const hi = domupb[i + 1] - 1;
      #if UPCXXA_DEBUG_ISCONTIGUOUS
        fprintf(stderr, "\nlo=%i",lo);
        fprintf(stderr, "\nhi=%i",hi);
      #endif
      if (px->side_factors[i] >= 0) {
        pointwithminaddr = pointwithminaddr.set0(i, lo);
        pointwithmaxaddr = pointwithmaxaddr.set0(i, hi);
        }
      else {
        pointwithminaddr = pointwithminaddr.set0(i, hi);
        pointwithmaxaddr = pointwithmaxaddr.set0(i, lo);
        }
      }

    #if UPCXXA_DEBUG_ISCONTIGUOUS
      fprintf(stderr, "\npointwithminaddr=");
      UPCXXA_PRINT(pointwithminaddr, err);
      fprintf(stderr, "\npointwithmaxaddr=");
      UPCXXA_PRINT(pointwithmaxaddr, err);
      fprintf(stderr, "\n");
      fflush(stderr);
    #endif

    {
      size_t numelements = pR->size();
      size_t minoffset =
        px->compute_index("is_contiguous()", pointwithminaddr);
      size_t maxoffset =
        px->compute_index("is_contiguous()", pointwithmaxaddr);
      size_t ref_result = (numelements == maxoffset - minoffset + 1);
      assert(ref_result == result);
      return result;
      }
  #endif
  }
 }
#endif
}
#undef UPCXXA_DEBUG_ISCONTIGUOUS

/* EFFECTS: returns true if all elements in the domain of x are stored contiguously in memory */
UPCXXA_ARRAYSIG(inline bool) is_contiguous() const {
  size_t numelements = 0;
  UPCXXA_ASSERT_EXISTS((*this), "(in is_contiguous method)");
  return internal_is_contiguous(&_domain, &numelements, NULL);
}
/* REQUIRES: given domain (R) is a subset of _domain()
 * EFFECTS: returns true if all elements in the given domain 
 * are stored contiguously in memory in array
 */
UPCXXA_ARRAYSIG(inline bool) is_contiguous_over(const rectdomain<N> &R) const {
  size_t numelements = 0;
  UPCXXA_ASSERT_EXISTS((*this), "(in is_contiguous_over method)");
  return internal_is_contiguous(&R, &numelements, NULL);
}
/* ------------------------------------------------------------------------------------ */
/* EFFECTS: returns a ptr to the element with the lowest address in the current array 
 * (should be used to get a handle on the array data when performing bulk operations on contiguous arrays) 
 */
UPCXXA_ARRAYSIG(inline UPCXXA_PTR_TO_T) get_array_data_ptr() const {
  UPCXXA_ASSERT_EXISTS((*this), "(in get_array_data_ptr)");
  return get_array_data_ptr_with_domain(_domain);
}
/* REQUIRES: given domain (R) is a subset of _domain
 * EFFECTS: returns a ptr to the element with the lowest address in the current array 
 * (should be used to get a handle on the array data when performing bulk operations on contiguous arrays) 
 */
UPCXXA_ARRAYSIG(inline UPCXXA_PTR_TO_T) get_array_data_ptr_with_domain(const rectdomain<N> &R) const {
  point<N> pointwithminaddr;
  UPCXXA_PTR_TO_T tiarray_firstelem;
  int i;
  UPCXXA_ASSERT_EXISTS((*this),
                       "(in get_array_data_ptr_with_domain)");
  #if 0
    /* no negative side_factors */
    pointwithminaddr = R.min();
  #elif 0
    point<N> const dommin = R.min();
    point<N> const domupb = R.upb();
    for (i = 0; i < N; i++) {
      if (side_factors[i] >= 0) 
        pointwithminaddr = pointwithminaddr.set0(i, dommin[i + 1]);        
      else 
        pointwithminaddr = pointwithminaddr.set0(i, domupb[i + 1] - 1);
      }
  #else
    pointwithminaddr = R.min();
    for (i = 0; i < N; i++) {
      if (side_factors[i] < 0) 
        pointwithminaddr = pointwithminaddr.set0(i, R.upb()[i + 1] - 1);
    }
  #endif
  UPCXXA_INDEX(tiarray_firstelem, A,
               compute_index("get_array_data_ptr", pointwithminaddr));
  return tiarray_firstelem;
}
/* ------------------------------------------------------------------------------------ */
#if UPCXXA_ASSIGNABLE
UPCXXA_ARRAYSIG(inline UPCXXA_ARRAY) make_local_and_contiguous() const {
  /* returns a locally situated & contiguous version of this array,
   * with the same domain as x
   * possibly copying the elements (if necessary)
   */
  UPCXXA_ASSERT_EXISTS((*this), "(in make_local_and_contiguous)");

  if (UPCXXA_DIRECTLY_ADDRESSABLE(A) && is_contiguous()) 
    return (*this);
  else {
    UPCXXA_ARRAY retval;

    retval = UPCXXA_ARRAY(_domain);
    retval.copy((*this));
    return retval;
  }
}
#endif /* UPCXXA_ASSIGNABLE */
/* ------------------------------------------------------------------------------------ */
#if UPCXXA_ASSIGNABLE
/* x.vbroadcast(s) is equivalent to:
   ordered_foreach (p in x._domain()) x[p] = broadcast x[p] from s;

   The current implementation assumes myTeam()._domain() is [0 : UPCXXA_PROCS - 1].
   */
UPCXXA_ARRAYSIG(void) vbroadcast(int s) const {
  size_t num;
  UPCXXA_ASSERT_EXISTS((*this), "(in vbroadcast method)");
  if (N != 1) {
    fprintf(stderr, "fatal error: vbroadcast() used on %dd array; "
            "it may only be used on 1d arrays.\n", N);
    abort();
  }

  num = _domain.size();
  UPCXXA_TRACE_PRINTF(("COLLECTIVE vbroadcast: sz = %lu, ",
                       (unsigned long int) (sizeof(T) * num)));
  if (s < 0 || s >= UPCXXA_PROCS) {
    fprintf(stderr, "fatal error:"
	    " cannot vbroadcast from rank %d, there are only %d ranks!\n",
	    s, UPCXXA_PROCS);
    fflush(stderr);
    abort();
  }

  UPCXXA_ARRAY y = make_local_and_contiguous();

  /* Call backend broadcast function on y. */
  UPCXXA_BROADCAST_RAW(UPCXXA_LOCAL_PART(y.A), s,
                       UPCXXA_LOCAL_PART(y.A),
                       (size_t) (sizeof(T) * num));

  if (!UPCXXA_EQUAL(A, y.A)) {
    assert(UPCXXA_DIRECTLY_ADDRESSABLE(y.A));
    copy(y);
    y.destroy();
  }
}
#endif /* UPCXXA_ASSIGNABLE */

/* ------------------------------------------------------------------------------------ */

} // namespace upcxx

#undef UPCXXA_ASSIGNABLE
#undef UPCXXA_FORALL2ORDERED2
#undef UPCXXA__FORALL2ORDERED2
#undef UPCXXA_FORALL2ORDERED3
#undef UPCXXA__FORALL2ORDERED3

#undef UPCXXA_PRINT
#undef UPCXXA_GLOBAL_ARRAY
#undef UPCXXA_DEBUG_COPY_ENABLED
#undef UPCXXA_DONT_KNOW
#undef UPCXXA_FAILED_NONBLOCKING_WARNING
#undef UPCXXA_COPY_DESC_T

#undef UPCXXA_ARRAYSIG
#undef UPCXXA_ARRAYSIG_NORET
#undef UPCXXA_TEMPLATE
#undef UPCXXA_TEMPLATE_ARGS
#undef UPCXXA_ARRAY

// array_addr.h undefs
#undef UPCXXA_BIDX
#undef UPCXXA_COMPUTE_INDEX
#undef UPCXXA_PIDX
#undef UPCXXA_ABV
#undef UPCXXA_ARRAY_FASTBESTCONV
#undef UPCXXA_ARRAY_FASTCONV
#undef UPCXXA__ABV_MSG1
#undef UPCXXA__ABV_MSG2
#undef UPCXXA__ARRAY_FASTADDR1
#undef UPCXXA__ARRAY_FASTADDR2
#undef UPCXXA__ARRAY_FASTADDR3
#undef UPCXXA__ARRAY_FASTADDRN
#undef UPCXXA__ARRAY_FASTBEST1
#undef UPCXXA__ARRAY_FASTBEST2
#undef UPCXXA__ARRAY_FASTBEST3
#undef UPCXXA__ARRAY_FASTBESTN
#undef UPCXXA__FORALL2_1
#undef UPCXXA__FORALL2_2
#undef UPCXXA__FORALL2_3
#undef UPCXXA__FORALL2_N
#undef UPCXXA__FORALL_1
#undef UPCXXA__FORALL_2
#undef UPCXXA__FORALL_3
#undef UPCXXA__FORALL_N
#undef UPCXXA_FORALL
#undef UPCXXA_FORALL2
#undef UPCXXA_FORALL2_1
#undef UPCXXA_FORALL2_2
#undef UPCXXA_FORALL2_3
#undef UPCXXA_FORALL2_N
#undef UPCXXA_FORALL_1
#undef UPCXXA_FORALL_2
#undef UPCXXA_FORALL_3
#undef UPCXXA_FORALL_N
