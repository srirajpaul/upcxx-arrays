//
// ========================
// Multi Rect Domain Type A
// ========================
//
// This represents a domain that is a list of rectdomains.  It
// is optimized for a fast "foreach" at the expense of set comparisons
// and operations.
//

#include <algorithm>
#include <cstring>
#include <cassert>
#include "foreach.h"
#include "utils.h"

#define UPCXXA_ASSERTREPOKON(mrad)              \
  assert(((mrad)->rep_ok()==NULL))
#define UPCXXA_ASSERTREPOK()       UPCXXA_ASSERTREPOKON(this)

#define UPCXXA_INITCACHES(x) do {                  \
    (x)->size_cache=0;                             \
    (x)->bbox_cache=rectdomain<N>();               \
  } while(false)


#if 0 // mutability of domains makes it dangerous to use the same object
# define UPCXXA_EMPTY_MRAD() empty_domain()
#else
# define UPCXXA_EMPTY_MRAD() domain<N>()
#endif

#define UPCXXA_MRAD_SIZE(mrad, arrayLength)     \
  mrad.rects.reserve(arrayLength)

#define UPCXXA_RD_TO_MRAD(D, RD) *to_domain(D, RD)

#define UPCXXA_ARRAY_COPY(src,srcidx,dst,dstidx,num,type)       \
  memcpy(dst+dstidx, src+srcidx, num*sizeof(type))

#define UPCXXA_ARRAY_TO_VEC_COPY(src,srcidx,dst,num)    \
  dst.insert(dst.end(), src+srcidx, src+srcidx+num)

#define UPCXXA_VEC_TO_VEC_COPY(src,dst)         \
  dst.insert(dst.end(), src.begin(), src.end())

/* Append an rd onto an rd vector
 * Also, autoincrement the index variable given
 */
#define UPCXXA_APPEND_RD(vect,rd,idx) do {             \
    vect->push_back(rd);                               \
    idx++;                                             \
  } while(false)

namespace upcxx {

/* --------------------------------------------------------------------- */

template<int N> inline void domain<N>::clear() {
  rects.clear();
  UPCXXA_INITCACHES(this);
}

template<int N> inline domain<N> *domain<N>::to_domain(domain<N> *dom,
                                                       const rectdomain<N> &rd) {
  // reset dom to just contain rd
  dom->clear();
  if (rd.is_empty()) return dom;
  else {
    dom->rects.push_back(rd);
    UPCXXA_ASSERTREPOKON(dom);
    return dom;
  }
}

template<int N> inline domain<N> domain<N>::to_domain(const rectdomain<N> *rectList,
                                                      size_t num) {
  domain<N> result;
  for (size_t i = 0; i < num; i++)
    result += rectList[i];
  return result;
}
  
template<int N> domain<N> domain<N>::to_domain(const point<N> *paList,
                                               size_t num) {
#if UPCXXA_DOMAINS_USERCHECK
  point<N> *paCopy = new point<N>[num];
  UPCXXA_ARRAY_COPY(paList, 0, paCopy, 0, num, point<N>);
  // sort the points
  std::sort(paCopy, paCopy + num, point<N>::compare);
  // check for duplicates
  bool duplicate = false;
  for (size_t i = 1; i < num; i++)
    UPCXXA_CHECKF((paCopy[i-1] != paCopy[i]), 
                  "duplicate point", duplicate = true;);
  delete[] paCopy;
  if (duplicate) return UPCXXA_EMPTY_MRAD();
#endif
  return domain<N>(paList, num);
}
                                                 
/* --------------------------------------------------------------------- */

// Constructors
template<int N> inline domain<N>::domain() {
  UPCXXA_INITCACHES(this);
  //UPCXXA_CHECK((rep_ok()==NULL),rep_ok()+" at line "+__LINE__);
  UPCXXA_ASSERTREPOK();
}

template<int N> inline domain<N>::domain(const rectdomain<N> *rectList, size_t num) {
  if (rectList != NULL && num>0) { // nonempty MRAD
    UPCXXA_ARRAY_TO_VEC_COPY(rectList,0,rects,num);
  }
  UPCXXA_INITCACHES(this);
  //UPCXXA_CHECK((rep_ok()==NULL),rep_ok()+" at line "+__LINE__);
  UPCXXA_ASSERTREPOK();
}

template<int N> inline void domain<N>::copy(const domain<N> *d) {
  if (d == this) return; // nothing to do here
  UPCXXA_VEC_TO_VEC_COPY(d->rects, rects);
  // Copy over the caches
  size_cache=d->size_cache; bbox_cache=d->bbox_cache;
}

template<int N> inline domain<N>::domain(const domain<N> &d) {
  // copy constructor
  copy(&d);
  // Do we even need to do a copy if we're duplicating a domain?
  
  //UPCXXA_CHECK((rep_ok()==NULL),rep_ok()+" at line "+__LINE__);
  UPCXXA_ASSERTREPOK();
}

template<int N> inline domain<N>::domain(const rectdomain<N> &rd) {
  if (!rd.is_empty()) {
    //System.out.println("Called tiMRAD("+rd+")");
    rects.push_back(rd);
    //System.out.println("\tResulting in"+this);
  }
  UPCXXA_INITCACHES(this);
  //UPCXXA_CHECK((rep_ok()==NULL),rep_ok()+" at line "+__LINE__);
  UPCXXA_ASSERTREPOK();
}

template<int N> inline domain<N>::domain(const point<N> *pa, size_t num) {
  // unchecked: assert that point's in pa are disjoint
  if (num!=0) {
    for (size_t idx=0; idx<num; idx++) {
      rects.push_back(rectdomain<N>(pa[idx],pa[idx]+
                                    UPCXXA_ONES_POINT,true));
    }
  }
  UPCXXA_INITCACHES(this);
  //UPCXXA_CHECK((rep_ok()==NULL),rep_ok()+" at line "+__LINE__);
  UPCXXA_ASSERTREPOK();
}

template<int N> inline domain<N> &domain<N>::operator=(const domain &d) {
  rects.clear();
  copy(&d);
  return *this;
}

#ifdef UPCXXA_USE_CXX11
// move constructor and assignment
template<int N> inline domain<N>::domain(domain<N> &&d) {
  rects = std::move(d.rects);
  size_cache = d.size_cache;
  bbox_cache = d.bbox_cache;
  UPCXXA_INITCACHES(&d);
}

template<int N> inline domain<N> &domain<N>::operator=(domain<N> &&d) {
  if (this != &d) {
    rects = std::move(d.rects);
    size_cache = d.size_cache;
    bbox_cache = d.bbox_cache;
    UPCXXA_INITCACHES(&d);
  }
  return *this;
}
#endif

// Internal set relations routines
// rectsToAddD == dlD || rectsToAddD == NULL
// If testEmpty==false, calculates (this-dlD+rectsToAddD)
//                      target is modified to be the result
// If testEmpty==true, sets testEmpty to false iff this-dlD is nonempty
template<int N> void domain<N>::new_subtract(const domain<N> &dlD,
                                             bool &testEmpty,
                                             const domain<N> *rectsToAddD,
                                             domain<N> *target) const {
  assert(rectsToAddD == &dlD || rectsToAddD==NULL);//,"Failed parameter invariant in tiMRAD.new_subtract");

  //System.out.println("\tEntered newSub subtracting "+new domain<N>(dl)+" and adding "+new domain<N>(rectsToAdd));
  // If this is empty, then this-anything is empty
  // This code will get screwed if a rectdomain can be stored in its bb
  if (testEmpty && (is_empty() || dlD.is_empty())) return;
  if (is_empty()) {
    if (rectsToAddD != NULL)
      target->copy(rectsToAddD);
    else
      target->clear();
    return;
  }
  // Subtracting nothing is rather pointless...
  if (dlD.is_empty()) {
    target->copy(this);
    return;
  }

  //TODO: could change append to if checkempty, return nonnull, else append

  // Initialize our inbox to the list of rectangles, using temp array 1
  // Also acquire our outbox, with the same length as the inbox
  // And get a count of valid elements in each array
  UPCXXA_THREAD_LOCAL(vector, tmp1);
  UPCXXA_THREAD_LOCAL(vector, tmp2);
  vector *result = &tmp1, *saved_result = &tmp2;
  vsize_t resultlength=rects.size();
  UPCXXA_VEC_TO_VEC_COPY(rects,(*result));

  for (vsize_t dlidx=0; dlidx <dlD.rects.size(); dlidx++) {
    //Swap the inbox and outbox and replace validity lengths
    vector *rdswap=saved_result;
    saved_result=result;
    result=rdswap;
    result->clear();
    resultlength=0;
      
    rectdomain<N> curdl=dlD.rects[dlidx];

    for (vsize_t sridx=0; sridx < saved_result->size(); sridx++) {
      rectdomain<N> sr_sridx=(*saved_result)[sridx];
      rectdomain<N> id = sr_sridx * curdl;

      if (id.is_empty()) {
        UPCXXA_APPEND_RD(result,sr_sridx,resultlength);
      } else if (id == sr_sridx) {
        // do nothing
      } else {
        rectdomain<N> rd = sr_sridx;
        point<N> p0 = rd.min();
        point<N> p1 = rd.upb();
        point<N> stride = rd.get_stride();

        // slice
        for (int i = 0; i < N; i++) {
          cint_t rd_p0 = p0.get0(i);
          cint_t rd_p1 = p1.get0(i);
          cint_t id_p0 = id.min().get0(i);
          cint_t id_p1 = id.upb().get0(i);

          if (rd_p0 < id_p0) {
            UPCXXA_APPEND_RD(result,
                             rectdomain<N>(p0,p1.set0(i,id_p0),stride),
                             resultlength);
            p0 = p0.set0(i, id_p0);
          }
          if (rd_p1 > id_p1) {
            UPCXXA_APPEND_RD(result,
                             rectdomain<N>(p0.set0(i,(id_p1-1+
                                                      stride.get0(i))),
                                           p1,stride),resultlength);
            p1 = p1.set0(i, id_p1);
          }
        }

        // handle interior of overlap
        point<N> id_stride=id.get_stride();
        for (int i = 0; i < N; i++) {
          if (stride.get0(i) < id_stride.get0(i)) {
            for (cint_t set_p0 = p0.get0(i);
                 set_p0 < (p1.get0(i) -
                           id_stride.get0(i));
                 set_p0 += id_stride.get0(i)) {
              UPCXXA_APPEND_RD(result,
                               rectdomain<N>(p0.set0(i,(set_p0 +
                                                        stride.get0(i))),
                                     p1.set0(i,(set_p0 +
                                                id_stride.get0(i))),
                                     stride),resultlength);
            }
            stride = stride.set0(i,
                                 id_stride.get0(i));
          }
        }
      }
    }
  }

  if (testEmpty) {
    //System.out.println("\tReturning out of testEmpty check");
    if (resultlength != 0) testEmpty = false;
  } else {
    vsize_t totallen = resultlength;
    if (rectsToAddD != NULL) totallen +=dlD.rects.size(); // use guarantee that rectsToAddD == dlD
    target->clear();
    if (totallen != 0) {
      if (resultlength > 0)
        UPCXXA_VEC_TO_VEC_COPY((*result),target->rects);
      if (totallen > resultlength)
        UPCXXA_VEC_TO_VEC_COPY(dlD.rects,target->rects);
    }
  }
  tmp1.clear(); // clear temps for use in next call
  tmp2.clear();
}

// Set Relations
template<int N> inline domain<N> domain<N>::operator+(const domain<N> &d) const {
  //System.out.println("Calling newSub on "+this+" subtracting "+d);
  bool testEmpty = false;
  domain<N> result;
  new_subtract(d,testEmpty,&d,&result);
  return result;
}

template<int N> inline domain<N> &domain<N>::operator+=(const domain<N> &d) {
  bool testEmpty = false;
  new_subtract(d,testEmpty,&d,this);
  return *this;
}

template<int N> inline domain<N> domain<N>::operator-(const domain<N> &d) const {
  bool testEmpty = false;
  domain<N> result;
  new_subtract(d,testEmpty,NULL,&result);
  return result;
}

template<int N> inline domain<N> &domain<N>::operator-=(const domain<N> &d) {
  bool testEmpty = false;
  new_subtract(d,testEmpty,NULL,this);
  return *this;
}

template<int N> void domain<N>::intersect(const domain<N> &d,
                                          domain<N> &target) const {
  // this * d
  UPCXXA_THREAD_LOCAL(vector, tmp);
  vector *result = &tmp;
  vsize_t resultsize=0;

  if ((rects.size()==0) || (d.rects.size()==0)) { //Isct anything with empty is empty
    target.clear();
    return;
  }

  if ((rects.size()==1) && (d.rects.size()==1)) { //Intersecting rectangles!
    rectdomain<N> res=rects[0] * d.rects[0];
    target.clear();
    if (!res.is_empty())
      target.rects.push_back(res);
    return;
  }

  for (vsize_t myidx=0; myidx < rects.size(); myidx++) {
    rectdomain<N> myRD = rects[myidx];
    for (vsize_t d_idx=0; d_idx < d.rects.size(); d_idx++) {
      rectdomain<N> partial = myRD * d.rects[d_idx];

      if (!partial.is_empty()) {
        UPCXXA_APPEND_RD(result,partial,resultsize);
      }
    }
  }

  target.clear();
  if (resultsize != 0)
    UPCXXA_VEC_TO_VEC_COPY((*result),target.rects);
  tmp.clear();
}

template<int N> inline domain<N> domain<N>::operator*(const domain<N> &d) const {
  domain<N> result;
  intersect(d, result);
  return result;
}

template<int N> inline domain<N> &domain<N>::operator*=(const domain<N> &d) {
  intersect(d, *this);
  return *this;
}

template<int N> inline bool domain<N>::is_empty() const {
  // this is the empty set
  return rects.size() == 0;
}

template<int N> inline bool domain<N>::is_not_empty() const {
  return !is_empty();
}

template<int N> inline bool domain<N>::is_adr() const {
  if (rects.size()==0)
    return true;
  for (vsize_t idx=0; idx < rects.size(); idx++) {
    if (!rects[idx].is_adr())
      return false;
  }
  return true;
}
  
template<int N> inline bool domain<N>::equals(const domain<N> &d) const {
  // this == d
  if (size()!=d.size()) return false;
  if (bounding_box()!=d.bounding_box()) return false;
  return operator-(d).is_empty();
}

template<int N> inline bool domain<N>::operator==(const domain<N> &d) const {
  return equals(d);
}

template<int N> inline bool domain<N>::operator<=(const domain<N> &d) const { // isSubset
  // this <= d
  bool testEmpty = true;
  new_subtract(d,testEmpty,NULL,NULL);
  return testEmpty;
}

template<int N> inline bool domain<N>::operator<(const domain<N> &d) const { // isStrictSubset
  // this < d
  return operator<=(d) && !(d <= *this);
}

template<int N> inline bool domain<N>::operator>=(const domain<N> &d) const { // isSuperset
  return d <= *this;
}

template<int N> inline bool domain<N>::operator>(const domain<N> &d) const { // isStrictSuperset
  return d < *this;
}

// rectdomain Relations
template<int N> inline domain<N> domain<N>::operator+(const rectdomain<N> &rd) const {
  UPCXXA_THREAD_LOCAL(domain<N>, tmp);
  tmp.clear();
  domain<N> *dom = &tmp;
  return operator+(UPCXXA_RD_TO_MRAD(dom, rd));
}

template<int N> inline domain<N> &domain<N>::operator+=(const rectdomain<N> &rd) {
  UPCXXA_THREAD_LOCAL(domain<N>, tmp);
  tmp.clear();
  domain<N> *dom = &tmp;
  return operator+=(UPCXXA_RD_TO_MRAD(dom, rd));
}

template<int N> inline domain<N> domain<N>::operator-(const rectdomain<N> &rd) const {
  UPCXXA_THREAD_LOCAL(domain<N>, tmp);
  tmp.clear();
  domain<N> *dom = &tmp;
  return operator-(UPCXXA_RD_TO_MRAD(dom, rd));
}

template<int N> inline domain<N> &domain<N>::operator-=(const rectdomain<N> &rd) {
  UPCXXA_THREAD_LOCAL(domain<N>, tmp);
  tmp.clear();
  domain<N> *dom = &tmp;
  return operator-=(UPCXXA_RD_TO_MRAD(dom, rd));
}

template<int N> inline domain<N> domain<N>::operator*(const rectdomain<N> &rd) const {
  UPCXXA_THREAD_LOCAL(domain<N>, tmp);
  tmp.clear();
  domain<N> *dom = &tmp;
  return operator*(UPCXXA_RD_TO_MRAD(dom, rd));
}

template<int N> inline domain<N> &domain<N>::operator*=(const rectdomain<N> &rd) {
  UPCXXA_THREAD_LOCAL(domain<N>, tmp);
  tmp.clear();
  domain<N> *dom = &tmp;
  return operator*=(UPCXXA_RD_TO_MRAD(dom, rd));
}

// TODO: these could be optimized further by first checking rd against bounding_box
template<int N> inline bool domain<N>::equals(const rectdomain<N> &rd) const {
  UPCXXA_THREAD_LOCAL(domain<N>, tmp);
  tmp.clear();
  domain<N> *dom = &tmp;
  return equals(UPCXXA_RD_TO_MRAD(dom, rd));
}

template<int N> inline bool domain<N>::operator==(const rectdomain<N> &rd) const {
  return equals(rd);
}

template<int N> inline bool domain<N>::operator<=(const rectdomain<N> &rd) const { // isSubset
  UPCXXA_THREAD_LOCAL(domain<N>, tmp);
  tmp.clear();
  domain<N> *dom = &tmp;
  return operator<=(UPCXXA_RD_TO_MRAD(dom, rd));
}

template<int N> inline bool domain<N>::operator<(const rectdomain<N> &rd) const { // isStrictSubset
  UPCXXA_THREAD_LOCAL(domain<N>, tmp);
  tmp.clear();
  domain<N> *dom = &tmp;
  return operator<(UPCXXA_RD_TO_MRAD(dom, rd));
}

template<int N> inline bool domain<N>::operator>=(const rectdomain<N> &rd) const { // isSuperset
  UPCXXA_THREAD_LOCAL(domain<N>, tmp);
  tmp.clear();
  domain<N> *dom = &tmp;
  return operator>=(UPCXXA_RD_TO_MRAD(dom, rd));
}

template<int N> inline bool domain<N>::operator>(const rectdomain<N> &rd) const { // isStrictSuperset
  UPCXXA_THREAD_LOCAL(domain<N>, tmp);
  tmp.clear();
  domain<N> *dom = &tmp;
  return operator>(UPCXXA_RD_TO_MRAD(dom, rd));
}

// Point Relations
template<int N> domain<N> domain<N>::operator+(const point<N> &p) const {
  // this + p
  if (rects.size()==0)
    return UPCXXA_EMPTY_MRAD();
   
  domain<N> retval;
  UPCXXA_MRAD_SIZE(retval, rects.size());
  vector *result = &retval.rects;
 
  for (vsize_t ctr=0; ctr < rects.size();) {
    UPCXXA_APPEND_RD(result,rects[ctr]+p,ctr);
  }

  UPCXXA_ASSERTREPOKON(&retval);
  return retval;
}

template<int N> domain<N> &domain<N>::operator+=(const point<N> &p) {
  // this += p
  for (vsize_t ctr=0; ctr < rects.size();) {
    rects[ctr] += p;
  }

  UPCXXA_ASSERTREPOK();
  return *this;
}

template<int N> domain<N> domain<N>::operator-(const point<N> &p) const {
  // this - p
  if (rects.size()==0)
    return UPCXXA_EMPTY_MRAD();

  domain<N> retval;
  UPCXXA_MRAD_SIZE(retval, rects.size());
  vector *result = &retval.rects;

  for (vsize_t ctr=0; ctr < rects.size();)
    UPCXXA_APPEND_RD(result,rects[ctr]-p,ctr);
  
  UPCXXA_ASSERTREPOKON(&retval);
  return retval;
}

template<int N> domain<N> &domain<N>::operator-=(const point<N> &p) {
  // this -= p
  for (vsize_t ctr=0; ctr < rects.size();) {
    rects[ctr] -= p;
  }

  UPCXXA_ASSERTREPOK();
  return *this;
}

template<int N> domain<N> domain<N>::operator/(const point<N> &p) const {
  // this / p
  domain<N> result;

  if (rects.size()==0)
    return result;

  for (vsize_t ctr=0; ctr < rects.size(); ctr++) {
    // Call the add function to remove any overlaps generated by rounding
    // effects during the divide.
    result += (rects[ctr] / p);
  }

  return result;
}

template<int N> domain<N> &domain<N>::operator/=(const point<N> &p) {
  // this /= p
  // call operator/ to avoid remove any overlaps
  domain<N> tmp = operator/(p);

  // copy results back
  clear(); // clear any caches as well
#ifdef UPCXXA_USE_CXX11
  rects = std::move(tmp.rects); // use move assignment instead of copy
#else
  UPCXXA_VEC_TO_VEC_COPY(tmp.rects, rects);
#endif

  UPCXXA_ASSERTREPOK();
  return *this;
}

template<int N> domain<N> domain<N>::operator*(const point<N> &p) const {
  // this * p
  if (rects.size()==0)
    return UPCXXA_EMPTY_MRAD();

  // a zero in p can result in overlap
  bool zero_in_p = false;
  for (int i = 1; i <= N; i++)
    if (p[i] == 0) {
      zero_in_p = true;
      break;
    }

  if (zero_in_p) {
    domain<N> result;
    for (vsize_t ctr=0; ctr < rects.size(); ctr++) {
      // Call the add function to remove any overlaps.
      result += (rects[ctr] * p);
    }
    return result;
  } else {
    domain<N> retval;
    UPCXXA_MRAD_SIZE(retval, rects.size());
    vector *result = &retval.rects;

    for (vsize_t ctr=0; ctr < rects.size();)
      UPCXXA_APPEND_RD(result,rects[ctr]*p,ctr);
    
    UPCXXA_ASSERTREPOKON(&retval);
    return retval;
  }
}

template<int N> domain<N> &domain<N>::operator*=(const point<N> &p) {
  // this *= p
  // call operator* to avoid remove any overlaps
  domain<N> tmp = operator*(p);

  // copy results back
  clear(); // clear any caches as well
#ifdef UPCXXA_USE_CXX11
  rects = std::move(tmp.rects); // use move assignment instead of copy
#else
  UPCXXA_VEC_TO_VEC_COPY(tmp.rects, rects);
#endif

  UPCXXA_ASSERTREPOK();
  return *this;
}

template<int N> domain<N> domain<N>::permute(const point<N> &p) const {
  UPCXXA_CHECK_PERMUTE(p, domain<N>(*this));
  if (rects.size()==0)
    return UPCXXA_EMPTY_MRAD();
    
  domain<N> retval;
  UPCXXA_MRAD_SIZE(retval, rects.size());
  vector *result = &retval.rects;

  for (vsize_t ctr=0; ctr < rects.size();)
    UPCXXA_APPEND_RD(result,rects[ctr].permute(p),ctr);
  
  UPCXXA_ASSERTREPOKON(&retval);
  return retval;
}
  
// Shape Information
template<int N> inline bool domain<N>::contains(const point<N> &p) const {
  // this contains p
  if (rects.size()==0) return false;
  if (!bounding_box().contains(p)) return false;
  for (vsize_t ctr=0; ctr < rects.size(); ctr++) {
    if (rects[ctr].contains(p)) {
      return true;
    }
  }
  return false;
}

// If this->is_rectangular(), modifies this->rects so that it contains
// only one item
template<int N> rectdomain<N> *domain<N>::is_rectangular_ex() {
  // if we're empty or a single rd, then clearly we're rectangular
  // if ((rects.getNumItems()==0)||(rects.getNumItems()==1))
  if (rects.size()==0)
    return NULL;
  if (rects.size()==1)
    return &rects[0];

  cint_t size=0;
  rectdomain<N> bb=bounding_box();
  point<N> min=bb.min();
  point<N> base_upb=min;
  point<N> upb=bb.upb();
  cint_t base_size[N];
  point<N> theStride=UPCXXA_ONES_POINT;

  // Initialize base_size array to zero
  memset(base_size, 0, N*sizeof(cint_t));

  // Calculate the base-row lengths, base-row maxes, and this->size()
  // For details on the algorithm, see PR677
  for (vsize_t ctr=0; ctr < rects.size(); ctr++) {
    rectdomain<N> rd=rects[ctr];
    size+=rd.size();

    for (int varyingdim=0;varyingdim<N;varyingdim++) {
      bool rowpresent=true;
	
      for (int ar=0;ar < N && rowpresent; ar++) {
        if (ar==varyingdim) continue; // We're varying this coord
        if (rd.min().get0(ar) > min.get0(ar))
          // If the rd is based above the min we won't have a row to examine
          rowpresent=false;
      }
      if (rowpresent) {
        // We know that every other coordinate is fixed, so we just
        // want the variation possible in varyingdim. This will be
        // bounded by the max and min, divided by the stride in that
        // dimension
        cint_t len=((rd.upb().get0(varyingdim) - 1 -
                     rd.min().get0(varyingdim))/
                    rd.raw_stride().get0(varyingdim))+1;
        base_upb=base_upb.set0(varyingdim,
                               domain_utils::max(base_upb.get0(varyingdim),
                                                 rd.upb().get0(varyingdim)));
        // System.out.println("Found row of length "+len+" varying dimension "+varyingdim+" in "+rd);
        base_size[varyingdim]+=len;
      }
    }
  }
    
  // Make sure the maxes are compatible
  if (base_upb != upb) return NULL;
  // Calculate the base stride
  for (int dim=0;dim < N; dim++) {
    cint_t bbsize=upb.get0(dim) - min.get0(dim);
    if (bbsize != 1) { //if bbsize==1, s[i]=1, which it's initialized to anyway
      if ((bbsize - 1) % (base_size[dim] - 1) != 0) return NULL;
      theStride=theStride.set0(dim,(bbsize - 1)/(base_size[dim] - 1));
    }
  }
    
  rectdomain<N> rectResult=rectdomain<N>(min,upb,theStride);
  //Check that we have the right number of points
  if (rectResult.size() != size) return NULL;

  //Verify that each rectangle is compatible with the putative rectResult
  for (vsize_t ctr=0; ctr < rects.size(); ctr++)
    if (!(rects[ctr]<=rectResult)) return NULL;
    
  rects.clear();
  rects.push_back(rectResult);
  UPCXXA_ASSERTREPOK();

  return &rects[0];
}

// If this->is_rectangular_ex!=NULL, modifies this as specified in
// interface to is_rectangular_ex
template<int N> inline bool domain<N>::is_rectangular() {
  //if we're empty or a single rd, then clearly we're rectangular
  //is_rectangular_ex now returns NULL on empty, too
  return (is_empty() || (is_rectangular_ex()!=NULL));
}

// Does not modify this
template<int N> inline bool domain<N>::is_rectangular() const {
  //if we're empty or a single rd, then clearly we're rectangular
  if (is_empty() || rects.size() == 1)
    return true;
  else {
    domain tmp(*this);
    return tmp.is_rectangular_ex()!=NULL;
  }
}

template<int N> inline size_t domain<N>::size() const {
  if (size_cache == 0) { //Check for a cached value
    vsize_t result = 0;
    for (vsize_t ctr=0; ctr < rects.size(); ctr++) {
      result += rects[ctr].size();
    }
    size_cache = result;
    return result;
  }
  return size_cache;
}

template<int N> inline point<N> domain<N>::min() const {
  return bounding_box().min();
}

template<int N> inline point<N> domain<N>::lwb() const {
  return min();
}

template<int N> inline point<N> domain<N>::upb() const {
  return bounding_box().upb();
}

template<int N> inline point<N> domain<N>::max() const {
  return upb() - (UPCXXA_ONES_POINT);
}

template<int N> rectdomain<N> domain<N>::bounding_box() const {
  if (bbox_cache.is_empty() && (!is_empty())) { //BB is not cached
    point<N> min,upb;
    //Expand min() and upb() inline so that we don't loop over rectangles twice
    upb=rects[0].upb();
    min=rects[0].min();
    for (vsize_t ctr=1; ctr < rects.size(); ctr++) {
      upb=upb.upper_bound(rects[ctr].upb());
      min=min.lower_bound(rects[ctr].min());
    }
    bbox_cache=rectdomain<N>(min,upb,true);
  }
  return bbox_cache;
}    

template<int N> void domain<N>::copy_rectdomains_to(rectdomain<N> *dst) const {
  vsize_t i = 0;
  for (typename vector::const_iterator it = rects.begin();
       it != rects.end(); it++, i++)
    dst[i] = *it;
}

// These are 0-indexed, unlike the Titanium versions.
template<int N> rectdomain<N> *domain<N>::rectdomain_list() const {
  vsize_t numRectangles = rects.size();
  rectdomain<N> *result = NULL;
  if (rects.size()) {    
    result = new rectdomain<N>[numRectangles];
    copy_rectdomains_to(result);
  }
  return result;
}

template<int N> inline size_t domain<N>::num_rectdomains() const {
  return (size_t) rects.size();
}

template<int N> inline rectdomain<N> domain<N>::get_rectdomain(size_t i) const {
  return rects[i];
}

template<int N> point<N> *domain<N>::point_list() const {
  size_t numPoints = size();
  size_t numPointsAdded = 0;
  point<N> *result = NULL;

  if (rects.size()) {
    result = new point<N>[numPoints];
    for (vsize_t ctr=0; ctr < rects.size(); ctr++) {
      //rep invar is such that we'll never call this on an empty rd...
      const rectdomain<N> &rd=rects[ctr];
      upcxx_foreach (p, rd) {
	result[numPointsAdded] = p;
	numPointsAdded++;
      };
    }
  }
  // assert (numPointsAdded == numPoints);
  return result;
}

// Internal dynamic optimization routines
template<int N> void domain<N>::join_rectangles() {
  if (rects.size()<=1) return;

  bool changes_made;
  UPCXXA_THREAD_LOCAL(vector, tmp1);
  UPCXXA_THREAD_LOCAL(vector, tmp2);
  vector *currList = &tmp1, *newList = &tmp2;
  UPCXXA_VEC_TO_VEC_COPY(rects,(*currList));
  bool ignore[rects.size()];
  vsize_t currListLen=rects.size(), newListLen=0;
  vsize_t listSize=rects.size();
    
  do {
    changes_made = false;
    newList->clear();
    newListLen=0;
    for (vsize_t i=0; i < currListLen; i++) {
      ignore[i] = false;
    }

    vsize_t searchIndexInit = 0;
    for (vsize_t curidx=0; curidx < currListLen; curidx++) {
      if (!ignore[searchIndexInit]) {
        vsize_t searchIndex = searchIndexInit + 1;
        bool matchFound = false;
        for (vsize_t compidx=1; ((compidx < currListLen) && (!matchFound)); compidx++) {
          if (!ignore[searchIndex]) {
            domain<N> result = (*currList)[curidx] + (*currList)[compidx];
            vector &rdl = result.rects;
            vsize_t rdl_size = result.rects.size();
            if (rdl_size==1) { //rdl should never be NULL here - never get empty RDs
              UPCXXA_APPEND_RD(newList,rdl[0],newListLen);
              changes_made = true;
              matchFound = true;
              ignore[searchIndex] = true;
            }
          }
          searchIndex++;
        }
        if (!matchFound) {
          UPCXXA_APPEND_RD(newList,(*currList)[curidx],newListLen);
        }
      }
      searchIndexInit++;
    }
    vector *rdswap=currList;
    currList = newList;
    newList=rdswap;
    //No need to swap lengths - newListLen gets clobbered at the top of the loop anyway
    currListLen=newListLen;
  } while (changes_made);
  if (currListLen < rects.size()) {
    rects.clear();
    UPCXXA_VEC_TO_VEC_COPY((*currList),rects);
  }

  // these may be inverted here, but that's ok
  tmp1.clear();
  tmp2.clear();
 
  UPCXXA_ASSERTREPOK();
}

// Dynamic optimizations
// is_rectangular_ex already does a join if the domain is rectangular
template<int N> void domain<N>::optimize() {
  if (!is_empty() && (is_rectangular_ex()==NULL))
    join_rectangles();
}

//returns NULL on invariant ok, otherwise an error string
template<int N> const char *domain<N>::rep_ok() const {
  if (rects.size()==0) {
    if (!(size_cache==-1 || size_cache==0))
      return "size cached incorrectly for an empty domain";
    if (!bbox_cache.is_empty())
      return "bounding box cached incorrectly for an empty domain";
    return NULL;
  } else {
    for (vsize_t idx=0;idx<rects.size();idx++) {
      if (rects[idx].is_empty())
        return "found empty RD in the rects list";
    }
    return NULL;
  }
}

template<int N> inline size_t domain<N>::index(const point<N> &p) const {
  size_t sum = 0;
  for (vsize_t i = 0; i < num_rectdomains(); i++) {
    if (rects[i].contains(p))
      return sum + rects[i].index(p);
    sum += rects[i].size();
  }
  return 0; // it is an error to reach this
}

} // namespace upcxx
