#!/bin/bash 

defs=($(grep '#' $* | grep 'define ' | cut -d '#' -f 2 | cut -d 'd' -f 2- | cut -d ' ' -f 2 | cut -d '(' -f 1 | cut -f 1 | sort -u))

for i in ${defs[@]}; do
    echo "#undef ${i}"
done

