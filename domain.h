#pragma once

#include <iostream>
#include <vector>
#include <utility>
#include "rdomain.h"

namespace upcxx {

template<int N> class domain {
 public:
  typedef typename std::vector< rectdomain<N> >::size_type vsize_t;

  domain();
  domain(const domain &d);
  domain(const rectdomain<N> &rd);

  domain &operator=(const domain &d);

#ifdef UPCXXA_USE_CXX11
  // move constructor and assignment
  domain(domain &&d);
  domain &operator=(domain &&d);
#endif

  // domain set relationships
  domain operator+(const domain &d) const;
  domain &operator+=(const domain &d);
  domain operator-(const domain &d) const;
  domain &operator-=(const domain &d);
  domain operator*(const domain &d) const;
  domain &operator*=(const domain &d);

  // domain bool relationships
  bool operator<(const domain &d) const;
  bool operator<=(const domain &d) const;
  bool operator>(const domain &d) const;
  bool operator>=(const domain &d) const;
  bool operator==(const domain<N> &d) const;
  bool equals(const domain &d) const;

  // rectdomain set relationships
  domain operator+(const rectdomain<N> &d) const;
  domain &operator+=(const rectdomain<N> &d);
  domain operator-(const rectdomain<N> &d) const;
  domain &operator-=(const rectdomain<N> &d);
  domain operator*(const rectdomain<N> &d) const;
  domain &operator*=(const rectdomain<N> &d);

  // rectdomain bool relationships
  bool operator<(const rectdomain<N> &d) const;
  bool operator<=(const rectdomain<N> &d) const;
  bool operator>(const rectdomain<N> &d) const;
  bool operator>=(const rectdomain<N> &d) const;
  bool operator==(const rectdomain<N> &d) const;
  bool equals(const rectdomain<N> &d) const;

  // point set relationships
  domain operator+(const point<N> &p) const;
  domain &operator+=(const point<N> &p);
  domain operator-(const point<N> &p) const;
  domain &operator-=(const point<N> &p);
  domain operator*(const point<N> &p) const;
  domain &operator*=(const point<N> &p);
  domain operator/(const point<N> &p) const;
  domain &operator/=(const point<N> &p);

  // Shape
  domain permute(const point<N> &p) const;

  // Shape information
  enum { arity = N };

  point<N> lwb() const;
  point<N> upb() const;
  point<N> min() const;
  point<N> max() const;
  size_t size() const;
  bool contains(const point<N> &p) const;
  rectdomain<N> bounding_box() const;
  bool is_empty() const;
  bool is_not_empty() const;
  bool is_adr() const;
  bool is_rectangular() const;
  bool is_rectangular();
  void copy_rectdomains_to(rectdomain<N> *dst) const;
  rectdomain<N> *rectdomain_list() const;
  size_t num_rectdomains() const;
  rectdomain<N> get_rectdomain(size_t i) const;
  point<N> *point_list() const;

  static domain to_domain(const rectdomain<N> *rd, size_t num);
  static domain to_domain(const point<N> *pts, size_t num);

  // dynamic optimization (undocumented)
  void optimize();

  size_t index(const point<N> &p) const;
  point_iter<N> iter() const { return point_iter<N>(*this); }

  // Print a string representation to the given stream.
  friend std::ostream& operator<<(std::ostream& os, const domain& d) {
    bool is_first = true;
    for (vsize_t ctr=0; ctr < d.rects.size(); ctr++) {
      rectdomain<N> dom=d.rects[ctr];
      if (!dom.is_empty()) {
        if (!is_first) {
          os << " + ";
        }
        os << dom;
        is_first = false;
      }
    }
    if (is_first) // if this is an empty domain
      os << rectdomain<N>();
    return os;
  }

  friend class rectdomain<N>;
  friend class point_iter<N>;

 private:
  typedef std::vector< rectdomain<N> > vector;

  domain(const rectdomain<N> *rds, size_t num);
  domain(const point<N> *pts, size_t num);
  void copy(const domain<N> *d);
  void clear();
  rectdomain<N> *is_rectangular_ex();
  void join_rectangles();
  void new_subtract(const domain &dlD, bool &testEmpty,
                    const domain *rectsToAddD,
                    domain *target) const;
  void intersect(const domain &d, domain &target) const;
  const char *rep_ok() const;

  static domain *to_domain(domain *dom, const rectdomain<N> &rd);

  // array of nonempty disjoint RD's which cover the domain
  // always has the sameity as "this" and therefore stored as ptr
  // if the MRAD is empty, then (list == NULL),
  //  (ie NEVER an empty array or array with all empty entries)
  // every RD in the list is part of the domain
  vector rects;
  // cached .size() for the domain, or 0 if never been computed
  mutable size_t size_cache;
  // cached unit-stride bounding box of the domain, or empty RD if
  // never computed
  mutable rectdomain<N> bbox_cache;
};

template<> class domain<0> {};

} // namespace upcxx

#include "domain.tpp"
#include "domain_undefs.h"
