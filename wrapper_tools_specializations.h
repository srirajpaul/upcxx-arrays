template<> struct rebased_indexer<1, strided> {
  inline static size_t compute(const cint_t base[1],
                               const cint_t stride[1],
                               const cint_t side_factors[1],
                               const char *where,
                               const cint_t px[1]) {
    return
      ((size_t)
       UPCXXA_PFAST_DIVIDE(((px[0] - base[0])*side_factors[0]), stride[0]));
  }
};

template<> struct rebased_indexer<1, unstrided> {
  inline static size_t compute(const cint_t base[1],
                               const cint_t stride[1],
                               const cint_t side_factors[1],
                               const char *where,
                               const cint_t px[1]) {
    return
      ((size_t)
       ((px[0] UPCXXA_ARRAY_BASE(0))*side_factors[0]));
  }
};

template<> struct rebased_indexer<1, row> {
  inline static size_t compute(const cint_t base[1],
                               const cint_t stride[1],
                               const cint_t side_factors[1],
                               const char *where,
                               const cint_t px[1]) {
    return
      ((size_t)
       (px[0] UPCXXA_ARRAY_BASE(0)));
  }
};

template<> struct rebased_indexer<1, column> {
  inline static size_t compute(const cint_t base[1],
                               const cint_t stride[1],
                               const cint_t side_factors[1],
                               const char *where,
                               const cint_t px[1]) {
    return
      ((size_t)
       (px[0] UPCXXA_ARRAY_BASE(0)));
  }
};

template<> struct rebased_indexer<2, strided> {
  inline static size_t compute(const cint_t base[2],
                               const cint_t stride[2],
                               const cint_t side_factors[2],
                               const char *where,
                               const cint_t px[2]) {
    return
      ((size_t)
       UPCXXA_PFAST_DIVIDE(((px[0] - base[0])*side_factors[0]), stride[0])
       + UPCXXA_PFAST_DIVIDE(((px[1] - base[1])*side_factors[1]), stride[1]));
  }
};

template<> struct rebased_indexer<2, unstrided> {
  inline static size_t compute(const cint_t base[2],
                               const cint_t stride[2],
                               const cint_t side_factors[2],
                               const char *where,
                               const cint_t px[2]) {
    return
      ((size_t)
       ((px[0] UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1]));
  }
};

template<> struct rebased_indexer<2, row> {
  inline static size_t compute(const cint_t base[2],
                               const cint_t stride[2],
                               const cint_t side_factors[2],
                               const char *where,
                               const cint_t px[2]) {
    return
      ((size_t)
       ((px[0] UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + (px[1] UPCXXA_ARRAY_BASE(1)));
  }
};

template<> struct rebased_indexer<2, column> {
  inline static size_t compute(const cint_t base[2],
                               const cint_t stride[2],
                               const cint_t side_factors[2],
                               const char *where,
                               const cint_t px[2]) {
    return
      ((size_t)
       (px[0] UPCXXA_ARRAY_BASE(0))
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1]));
  }
};

template<> struct rebased_indexer<3, strided> {
  inline static size_t compute(const cint_t base[3],
                               const cint_t stride[3],
                               const cint_t side_factors[3],
                               const char *where,
                               const cint_t px[3]) {
    return
      ((size_t)
       UPCXXA_PFAST_DIVIDE(((px[0] - base[0])*side_factors[0]), stride[0])
       + UPCXXA_PFAST_DIVIDE(((px[1] - base[1])*side_factors[1]), stride[1])
       + UPCXXA_PFAST_DIVIDE(((px[2] - base[2])*side_factors[2]), stride[2]));
  }
};

template<> struct rebased_indexer<3, unstrided> {
  inline static size_t compute(const cint_t base[3],
                               const cint_t stride[3],
                               const cint_t side_factors[3],
                               const char *where,
                               const cint_t px[3]) {
    return
      ((size_t)
       ((px[0] UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2]));
  }
};

template<> struct rebased_indexer<3, row> {
  inline static size_t compute(const cint_t base[3],
                               const cint_t stride[3],
                               const cint_t side_factors[3],
                               const char *where,
                               const cint_t px[3]) {
    return
      ((size_t)
       ((px[0] UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + (px[2] UPCXXA_ARRAY_BASE(2)));
  }
};

template<> struct rebased_indexer<3, column> {
  inline static size_t compute(const cint_t base[3],
                               const cint_t stride[3],
                               const cint_t side_factors[3],
                               const char *where,
                               const cint_t px[3]) {
    return
      ((size_t)
       (px[0] UPCXXA_ARRAY_BASE(0))
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2]));
  }
};

template<> struct rebased_indexer<4, strided> {
  inline static size_t compute(const cint_t base[4],
                               const cint_t stride[4],
                               const cint_t side_factors[4],
                               const char *where,
                               const cint_t px[4]) {
    return
      ((size_t)
       UPCXXA_PFAST_DIVIDE(((px[0] - base[0])*side_factors[0]), stride[0])
       + UPCXXA_PFAST_DIVIDE(((px[1] - base[1])*side_factors[1]), stride[1])
       + UPCXXA_PFAST_DIVIDE(((px[2] - base[2])*side_factors[2]), stride[2])
       + UPCXXA_PFAST_DIVIDE(((px[3] - base[3])*side_factors[3]), stride[3]));
  }
};

template<> struct rebased_indexer<4, unstrided> {
  inline static size_t compute(const cint_t base[4],
                               const cint_t stride[4],
                               const cint_t side_factors[4],
                               const char *where,
                               const cint_t px[4]) {
    return
      ((size_t)
       ((px[0] UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((px[3] UPCXXA_ARRAY_BASE(3))*side_factors[3]));
  }
};

template<> struct rebased_indexer<4, row> {
  inline static size_t compute(const cint_t base[4],
                               const cint_t stride[4],
                               const cint_t side_factors[4],
                               const char *where,
                               const cint_t px[4]) {
    return
      ((size_t)
       ((px[0] UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + (px[3] UPCXXA_ARRAY_BASE(3)));
  }
};

template<> struct rebased_indexer<4, column> {
  inline static size_t compute(const cint_t base[4],
                               const cint_t stride[4],
                               const cint_t side_factors[4],
                               const char *where,
                               const cint_t px[4]) {
    return
      ((size_t)
       (px[0] UPCXXA_ARRAY_BASE(0))
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((px[3] UPCXXA_ARRAY_BASE(3))*side_factors[3]));
  }
};

template<> struct rebased_indexer<5, strided> {
  inline static size_t compute(const cint_t base[5],
                               const cint_t stride[5],
                               const cint_t side_factors[5],
                               const char *where,
                               const cint_t px[5]) {
    return
      ((size_t)
       UPCXXA_PFAST_DIVIDE(((px[0] - base[0])*side_factors[0]), stride[0])
       + UPCXXA_PFAST_DIVIDE(((px[1] - base[1])*side_factors[1]), stride[1])
       + UPCXXA_PFAST_DIVIDE(((px[2] - base[2])*side_factors[2]), stride[2])
       + UPCXXA_PFAST_DIVIDE(((px[3] - base[3])*side_factors[3]), stride[3])
       + UPCXXA_PFAST_DIVIDE(((px[4] - base[4])*side_factors[4]), stride[4]));
  }
};

template<> struct rebased_indexer<5, unstrided> {
  inline static size_t compute(const cint_t base[5],
                               const cint_t stride[5],
                               const cint_t side_factors[5],
                               const char *where,
                               const cint_t px[5]) {
    return
      ((size_t)
       ((px[0] UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((px[3] UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((px[4] UPCXXA_ARRAY_BASE(4))*side_factors[4]));
  }
};

template<> struct rebased_indexer<5, row> {
  inline static size_t compute(const cint_t base[5],
                               const cint_t stride[5],
                               const cint_t side_factors[5],
                               const char *where,
                               const cint_t px[5]) {
    return
      ((size_t)
       ((px[0] UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((px[3] UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + (px[4] UPCXXA_ARRAY_BASE(4)));
  }
};

template<> struct rebased_indexer<5, column> {
  inline static size_t compute(const cint_t base[5],
                               const cint_t stride[5],
                               const cint_t side_factors[5],
                               const char *where,
                               const cint_t px[5]) {
    return
      ((size_t)
       (px[0] UPCXXA_ARRAY_BASE(0))
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((px[3] UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((px[4] UPCXXA_ARRAY_BASE(4))*side_factors[4]));
  }
};

template<> struct rebased_indexer<6, strided> {
  inline static size_t compute(const cint_t base[6],
                               const cint_t stride[6],
                               const cint_t side_factors[6],
                               const char *where,
                               const cint_t px[6]) {
    return
      ((size_t)
       UPCXXA_PFAST_DIVIDE(((px[0] - base[0])*side_factors[0]), stride[0])
       + UPCXXA_PFAST_DIVIDE(((px[1] - base[1])*side_factors[1]), stride[1])
       + UPCXXA_PFAST_DIVIDE(((px[2] - base[2])*side_factors[2]), stride[2])
       + UPCXXA_PFAST_DIVIDE(((px[3] - base[3])*side_factors[3]), stride[3])
       + UPCXXA_PFAST_DIVIDE(((px[4] - base[4])*side_factors[4]), stride[4])
       + UPCXXA_PFAST_DIVIDE(((px[5] - base[5])*side_factors[5]), stride[5]));
  }
};

template<> struct rebased_indexer<6, unstrided> {
  inline static size_t compute(const cint_t base[6],
                               const cint_t stride[6],
                               const cint_t side_factors[6],
                               const char *where,
                               const cint_t px[6]) {
    return
      ((size_t)
       ((px[0] UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((px[3] UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((px[4] UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((px[5] UPCXXA_ARRAY_BASE(5))*side_factors[5]));
  }
};

template<> struct rebased_indexer<6, row> {
  inline static size_t compute(const cint_t base[6],
                               const cint_t stride[6],
                               const cint_t side_factors[6],
                               const char *where,
                               const cint_t px[6]) {
    return
      ((size_t)
       ((px[0] UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((px[3] UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((px[4] UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + (px[5] UPCXXA_ARRAY_BASE(5)));
  }
};

template<> struct rebased_indexer<6, column> {
  inline static size_t compute(const cint_t base[6],
                               const cint_t stride[6],
                               const cint_t side_factors[6],
                               const char *where,
                               const cint_t px[6]) {
    return
      ((size_t)
       (px[0] UPCXXA_ARRAY_BASE(0))
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((px[3] UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((px[4] UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((px[5] UPCXXA_ARRAY_BASE(5))*side_factors[5]));
  }
};

template<> struct rebased_indexer<7, strided> {
  inline static size_t compute(const cint_t base[7],
                               const cint_t stride[7],
                               const cint_t side_factors[7],
                               const char *where,
                               const cint_t px[7]) {
    return
      ((size_t)
       UPCXXA_PFAST_DIVIDE(((px[0] - base[0])*side_factors[0]), stride[0])
       + UPCXXA_PFAST_DIVIDE(((px[1] - base[1])*side_factors[1]), stride[1])
       + UPCXXA_PFAST_DIVIDE(((px[2] - base[2])*side_factors[2]), stride[2])
       + UPCXXA_PFAST_DIVIDE(((px[3] - base[3])*side_factors[3]), stride[3])
       + UPCXXA_PFAST_DIVIDE(((px[4] - base[4])*side_factors[4]), stride[4])
       + UPCXXA_PFAST_DIVIDE(((px[5] - base[5])*side_factors[5]), stride[5])
       + UPCXXA_PFAST_DIVIDE(((px[6] - base[6])*side_factors[6]), stride[6]));
  }
};

template<> struct rebased_indexer<7, unstrided> {
  inline static size_t compute(const cint_t base[7],
                               const cint_t stride[7],
                               const cint_t side_factors[7],
                               const char *where,
                               const cint_t px[7]) {
    return
      ((size_t)
       ((px[0] UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((px[3] UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((px[4] UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((px[5] UPCXXA_ARRAY_BASE(5))*side_factors[5])
       + ((px[6] UPCXXA_ARRAY_BASE(6))*side_factors[6]));
  }
};

template<> struct rebased_indexer<7, row> {
  inline static size_t compute(const cint_t base[7],
                               const cint_t stride[7],
                               const cint_t side_factors[7],
                               const char *where,
                               const cint_t px[7]) {
    return
      ((size_t)
       ((px[0] UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((px[3] UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((px[4] UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((px[5] UPCXXA_ARRAY_BASE(5))*side_factors[5])
       + (px[6] UPCXXA_ARRAY_BASE(6)));
  }
};

template<> struct rebased_indexer<7, column> {
  inline static size_t compute(const cint_t base[7],
                               const cint_t stride[7],
                               const cint_t side_factors[7],
                               const char *where,
                               const cint_t px[7]) {
    return
      ((size_t)
       (px[0] UPCXXA_ARRAY_BASE(0))
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((px[3] UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((px[4] UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((px[5] UPCXXA_ARRAY_BASE(5))*side_factors[5])
       + ((px[6] UPCXXA_ARRAY_BASE(6))*side_factors[6]));
  }
};

template<> struct rebased_indexer<8, strided> {
  inline static size_t compute(const cint_t base[8],
                               const cint_t stride[8],
                               const cint_t side_factors[8],
                               const char *where,
                               const cint_t px[8]) {
    return
      ((size_t)
       UPCXXA_PFAST_DIVIDE(((px[0] - base[0])*side_factors[0]), stride[0])
       + UPCXXA_PFAST_DIVIDE(((px[1] - base[1])*side_factors[1]), stride[1])
       + UPCXXA_PFAST_DIVIDE(((px[2] - base[2])*side_factors[2]), stride[2])
       + UPCXXA_PFAST_DIVIDE(((px[3] - base[3])*side_factors[3]), stride[3])
       + UPCXXA_PFAST_DIVIDE(((px[4] - base[4])*side_factors[4]), stride[4])
       + UPCXXA_PFAST_DIVIDE(((px[5] - base[5])*side_factors[5]), stride[5])
       + UPCXXA_PFAST_DIVIDE(((px[6] - base[6])*side_factors[6]), stride[6])
       + UPCXXA_PFAST_DIVIDE(((px[7] - base[7])*side_factors[7]), stride[7]));
  }
};

template<> struct rebased_indexer<8, unstrided> {
  inline static size_t compute(const cint_t base[8],
                               const cint_t stride[8],
                               const cint_t side_factors[8],
                               const char *where,
                               const cint_t px[8]) {
    return
      ((size_t)
       ((px[0] UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((px[3] UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((px[4] UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((px[5] UPCXXA_ARRAY_BASE(5))*side_factors[5])
       + ((px[6] UPCXXA_ARRAY_BASE(6))*side_factors[6])
       + ((px[7] UPCXXA_ARRAY_BASE(7))*side_factors[7]));
  }
};

template<> struct rebased_indexer<8, row> {
  inline static size_t compute(const cint_t base[8],
                               const cint_t stride[8],
                               const cint_t side_factors[8],
                               const char *where,
                               const cint_t px[8]) {
    return
      ((size_t)
       ((px[0] UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((px[3] UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((px[4] UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((px[5] UPCXXA_ARRAY_BASE(5))*side_factors[5])
       + ((px[6] UPCXXA_ARRAY_BASE(6))*side_factors[6])
       + (px[7] UPCXXA_ARRAY_BASE(7)));
  }
};

template<> struct rebased_indexer<8, column> {
  inline static size_t compute(const cint_t base[8],
                               const cint_t stride[8],
                               const cint_t side_factors[8],
                               const char *where,
                               const cint_t px[8]) {
    return
      ((size_t)
       (px[0] UPCXXA_ARRAY_BASE(0))
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((px[3] UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((px[4] UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((px[5] UPCXXA_ARRAY_BASE(5))*side_factors[5])
       + ((px[6] UPCXXA_ARRAY_BASE(6))*side_factors[6])
       + ((px[7] UPCXXA_ARRAY_BASE(7))*side_factors[7]));
  }
};

template<> struct rebased_indexer<9, strided> {
  inline static size_t compute(const cint_t base[9],
                               const cint_t stride[9],
                               const cint_t side_factors[9],
                               const char *where,
                               const cint_t px[9]) {
    return
      ((size_t)
       UPCXXA_PFAST_DIVIDE(((px[0] - base[0])*side_factors[0]), stride[0])
       + UPCXXA_PFAST_DIVIDE(((px[1] - base[1])*side_factors[1]), stride[1])
       + UPCXXA_PFAST_DIVIDE(((px[2] - base[2])*side_factors[2]), stride[2])
       + UPCXXA_PFAST_DIVIDE(((px[3] - base[3])*side_factors[3]), stride[3])
       + UPCXXA_PFAST_DIVIDE(((px[4] - base[4])*side_factors[4]), stride[4])
       + UPCXXA_PFAST_DIVIDE(((px[5] - base[5])*side_factors[5]), stride[5])
       + UPCXXA_PFAST_DIVIDE(((px[6] - base[6])*side_factors[6]), stride[6])
       + UPCXXA_PFAST_DIVIDE(((px[7] - base[7])*side_factors[7]), stride[7])
       + UPCXXA_PFAST_DIVIDE(((px[8] - base[8])*side_factors[8]), stride[8]));
  }
};

template<> struct rebased_indexer<9, unstrided> {
  inline static size_t compute(const cint_t base[9],
                               const cint_t stride[9],
                               const cint_t side_factors[9],
                               const char *where,
                               const cint_t px[9]) {
    return
      ((size_t)
       ((px[0] UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((px[3] UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((px[4] UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((px[5] UPCXXA_ARRAY_BASE(5))*side_factors[5])
       + ((px[6] UPCXXA_ARRAY_BASE(6))*side_factors[6])
       + ((px[7] UPCXXA_ARRAY_BASE(7))*side_factors[7])
       + ((px[8] UPCXXA_ARRAY_BASE(8))*side_factors[8]));
  }
};

template<> struct rebased_indexer<9, row> {
  inline static size_t compute(const cint_t base[9],
                               const cint_t stride[9],
                               const cint_t side_factors[9],
                               const char *where,
                               const cint_t px[9]) {
    return
      ((size_t)
       ((px[0] UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((px[3] UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((px[4] UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((px[5] UPCXXA_ARRAY_BASE(5))*side_factors[5])
       + ((px[6] UPCXXA_ARRAY_BASE(6))*side_factors[6])
       + ((px[7] UPCXXA_ARRAY_BASE(7))*side_factors[7])
       + (px[8] UPCXXA_ARRAY_BASE(8)));
  }
};

template<> struct rebased_indexer<9, column> {
  inline static size_t compute(const cint_t base[9],
                               const cint_t stride[9],
                               const cint_t side_factors[9],
                               const char *where,
                               const cint_t px[9]) {
    return
      ((size_t)
       (px[0] UPCXXA_ARRAY_BASE(0))
       + ((px[1] UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((px[2] UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((px[3] UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((px[4] UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((px[5] UPCXXA_ARRAY_BASE(5))*side_factors[5])
       + ((px[6] UPCXXA_ARRAY_BASE(6))*side_factors[6])
       + ((px[7] UPCXXA_ARRAY_BASE(7))*side_factors[7])
       + ((px[8] UPCXXA_ARRAY_BASE(8))*side_factors[8]));
  }
};

template<> struct var_indexer_spec<1, strided> {
  inline static size_t index(const cint_t base[1],
                             const cint_t stride[1],
                             const cint_t side_factors[1],
                             const char *where,
                             const cint_t i0) {
    return
      ((size_t)
       UPCXXA_PFAST_DIVIDE(((i0 - base[0])*side_factors[0]), stride[0]));
  }
};

template<> struct var_indexer_spec<1, unstrided> {
  inline static size_t index(const cint_t base[1],
                             const cint_t stride[1],
                             const cint_t side_factors[1],
                             const char *where,
                             const cint_t i0) {
    return
      ((size_t)
       ((i0 UPCXXA_ARRAY_BASE(0))*side_factors[0]));
  }
};

template<> struct var_indexer_spec<1, row> {
  inline static size_t index(const cint_t base[1],
                             const cint_t stride[1],
                             const cint_t side_factors[1],
                             const char *where,
                             const cint_t i0) {
    return
      ((size_t)
       (i0 UPCXXA_ARRAY_BASE(0)));
  }
};

template<> struct var_indexer_spec<1, column> {
  inline static size_t index(const cint_t base[1],
                             const cint_t stride[1],
                             const cint_t side_factors[1],
                             const char *where,
                             const cint_t i0) {
    return
      ((size_t)
       (i0 UPCXXA_ARRAY_BASE(0)));
  }
};

template<> struct var_indexer_spec<2, strided> {
  inline static size_t index(const cint_t base[2],
                             const cint_t stride[2],
                             const cint_t side_factors[2],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1) {
    return
      ((size_t)
       UPCXXA_PFAST_DIVIDE(((i0 - base[0])*side_factors[0]), stride[0])
       + UPCXXA_PFAST_DIVIDE(((i1 - base[1])*side_factors[1]), stride[1]));
  }
};

template<> struct var_indexer_spec<2, unstrided> {
  inline static size_t index(const cint_t base[2],
                             const cint_t stride[2],
                             const cint_t side_factors[2],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1) {
    return
      ((size_t)
       ((i0 UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1]));
  }
};

template<> struct var_indexer_spec<2, row> {
  inline static size_t index(const cint_t base[2],
                             const cint_t stride[2],
                             const cint_t side_factors[2],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1) {
    return
      ((size_t)
       ((i0 UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + (i1 UPCXXA_ARRAY_BASE(1)));
  }
};

template<> struct var_indexer_spec<2, column> {
  inline static size_t index(const cint_t base[2],
                             const cint_t stride[2],
                             const cint_t side_factors[2],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1) {
    return
      ((size_t)
       (i0 UPCXXA_ARRAY_BASE(0))
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1]));
  }
};

template<> struct var_indexer_spec<3, strided> {
  inline static size_t index(const cint_t base[3],
                             const cint_t stride[3],
                             const cint_t side_factors[3],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2) {
    return
      ((size_t)
       UPCXXA_PFAST_DIVIDE(((i0 - base[0])*side_factors[0]), stride[0])
       + UPCXXA_PFAST_DIVIDE(((i1 - base[1])*side_factors[1]), stride[1])
       + UPCXXA_PFAST_DIVIDE(((i2 - base[2])*side_factors[2]), stride[2]));
  }
};

template<> struct var_indexer_spec<3, unstrided> {
  inline static size_t index(const cint_t base[3],
                             const cint_t stride[3],
                             const cint_t side_factors[3],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2) {
    return
      ((size_t)
       ((i0 UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2]));
  }
};

template<> struct var_indexer_spec<3, row> {
  inline static size_t index(const cint_t base[3],
                             const cint_t stride[3],
                             const cint_t side_factors[3],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2) {
    return
      ((size_t)
       ((i0 UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + (i2 UPCXXA_ARRAY_BASE(2)));
  }
};

template<> struct var_indexer_spec<3, column> {
  inline static size_t index(const cint_t base[3],
                             const cint_t stride[3],
                             const cint_t side_factors[3],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2) {
    return
      ((size_t)
       (i0 UPCXXA_ARRAY_BASE(0))
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2]));
  }
};

template<> struct var_indexer_spec<4, strided> {
  inline static size_t index(const cint_t base[4],
                             const cint_t stride[4],
                             const cint_t side_factors[4],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3) {
    return
      ((size_t)
       UPCXXA_PFAST_DIVIDE(((i0 - base[0])*side_factors[0]), stride[0])
       + UPCXXA_PFAST_DIVIDE(((i1 - base[1])*side_factors[1]), stride[1])
       + UPCXXA_PFAST_DIVIDE(((i2 - base[2])*side_factors[2]), stride[2])
       + UPCXXA_PFAST_DIVIDE(((i3 - base[3])*side_factors[3]), stride[3]));
  }
};

template<> struct var_indexer_spec<4, unstrided> {
  inline static size_t index(const cint_t base[4],
                             const cint_t stride[4],
                             const cint_t side_factors[4],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3) {
    return
      ((size_t)
       ((i0 UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((i3 UPCXXA_ARRAY_BASE(3))*side_factors[3]));
  }
};

template<> struct var_indexer_spec<4, row> {
  inline static size_t index(const cint_t base[4],
                             const cint_t stride[4],
                             const cint_t side_factors[4],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3) {
    return
      ((size_t)
       ((i0 UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + (i3 UPCXXA_ARRAY_BASE(3)));
  }
};

template<> struct var_indexer_spec<4, column> {
  inline static size_t index(const cint_t base[4],
                             const cint_t stride[4],
                             const cint_t side_factors[4],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3) {
    return
      ((size_t)
       (i0 UPCXXA_ARRAY_BASE(0))
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((i3 UPCXXA_ARRAY_BASE(3))*side_factors[3]));
  }
};

template<> struct var_indexer_spec<5, strided> {
  inline static size_t index(const cint_t base[5],
                             const cint_t stride[5],
                             const cint_t side_factors[5],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4) {
    return
      ((size_t)
       UPCXXA_PFAST_DIVIDE(((i0 - base[0])*side_factors[0]), stride[0])
       + UPCXXA_PFAST_DIVIDE(((i1 - base[1])*side_factors[1]), stride[1])
       + UPCXXA_PFAST_DIVIDE(((i2 - base[2])*side_factors[2]), stride[2])
       + UPCXXA_PFAST_DIVIDE(((i3 - base[3])*side_factors[3]), stride[3])
       + UPCXXA_PFAST_DIVIDE(((i4 - base[4])*side_factors[4]), stride[4]));
  }
};

template<> struct var_indexer_spec<5, unstrided> {
  inline static size_t index(const cint_t base[5],
                             const cint_t stride[5],
                             const cint_t side_factors[5],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4) {
    return
      ((size_t)
       ((i0 UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((i3 UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((i4 UPCXXA_ARRAY_BASE(4))*side_factors[4]));
  }
};

template<> struct var_indexer_spec<5, row> {
  inline static size_t index(const cint_t base[5],
                             const cint_t stride[5],
                             const cint_t side_factors[5],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4) {
    return
      ((size_t)
       ((i0 UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((i3 UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + (i4 UPCXXA_ARRAY_BASE(4)));
  }
};

template<> struct var_indexer_spec<5, column> {
  inline static size_t index(const cint_t base[5],
                             const cint_t stride[5],
                             const cint_t side_factors[5],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4) {
    return
      ((size_t)
       (i0 UPCXXA_ARRAY_BASE(0))
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((i3 UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((i4 UPCXXA_ARRAY_BASE(4))*side_factors[4]));
  }
};

template<> struct var_indexer_spec<6, strided> {
  inline static size_t index(const cint_t base[6],
                             const cint_t stride[6],
                             const cint_t side_factors[6],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4,
                             const cint_t i5) {
    return
      ((size_t)
       UPCXXA_PFAST_DIVIDE(((i0 - base[0])*side_factors[0]), stride[0])
       + UPCXXA_PFAST_DIVIDE(((i1 - base[1])*side_factors[1]), stride[1])
       + UPCXXA_PFAST_DIVIDE(((i2 - base[2])*side_factors[2]), stride[2])
       + UPCXXA_PFAST_DIVIDE(((i3 - base[3])*side_factors[3]), stride[3])
       + UPCXXA_PFAST_DIVIDE(((i4 - base[4])*side_factors[4]), stride[4])
       + UPCXXA_PFAST_DIVIDE(((i5 - base[5])*side_factors[5]), stride[5]));
  }
};

template<> struct var_indexer_spec<6, unstrided> {
  inline static size_t index(const cint_t base[6],
                             const cint_t stride[6],
                             const cint_t side_factors[6],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4,
                             const cint_t i5) {
    return
      ((size_t)
       ((i0 UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((i3 UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((i4 UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((i5 UPCXXA_ARRAY_BASE(5))*side_factors[5]));
  }
};

template<> struct var_indexer_spec<6, row> {
  inline static size_t index(const cint_t base[6],
                             const cint_t stride[6],
                             const cint_t side_factors[6],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4,
                             const cint_t i5) {
    return
      ((size_t)
       ((i0 UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((i3 UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((i4 UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + (i5 UPCXXA_ARRAY_BASE(5)));
  }
};

template<> struct var_indexer_spec<6, column> {
  inline static size_t index(const cint_t base[6],
                             const cint_t stride[6],
                             const cint_t side_factors[6],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4,
                             const cint_t i5) {
    return
      ((size_t)
       (i0 UPCXXA_ARRAY_BASE(0))
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((i3 UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((i4 UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((i5 UPCXXA_ARRAY_BASE(5))*side_factors[5]));
  }
};

template<> struct var_indexer_spec<7, strided> {
  inline static size_t index(const cint_t base[7],
                             const cint_t stride[7],
                             const cint_t side_factors[7],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4,
                             const cint_t i5,
                             const cint_t i6) {
    return
      ((size_t)
       UPCXXA_PFAST_DIVIDE(((i0 - base[0])*side_factors[0]), stride[0])
       + UPCXXA_PFAST_DIVIDE(((i1 - base[1])*side_factors[1]), stride[1])
       + UPCXXA_PFAST_DIVIDE(((i2 - base[2])*side_factors[2]), stride[2])
       + UPCXXA_PFAST_DIVIDE(((i3 - base[3])*side_factors[3]), stride[3])
       + UPCXXA_PFAST_DIVIDE(((i4 - base[4])*side_factors[4]), stride[4])
       + UPCXXA_PFAST_DIVIDE(((i5 - base[5])*side_factors[5]), stride[5])
       + UPCXXA_PFAST_DIVIDE(((i6 - base[6])*side_factors[6]), stride[6]));
  }
};

template<> struct var_indexer_spec<7, unstrided> {
  inline static size_t index(const cint_t base[7],
                             const cint_t stride[7],
                             const cint_t side_factors[7],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4,
                             const cint_t i5,
                             const cint_t i6) {
    return
      ((size_t)
       ((i0 UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((i3 UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((i4 UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((i5 UPCXXA_ARRAY_BASE(5))*side_factors[5])
       + ((i6 UPCXXA_ARRAY_BASE(6))*side_factors[6]));
  }
};

template<> struct var_indexer_spec<7, row> {
  inline static size_t index(const cint_t base[7],
                             const cint_t stride[7],
                             const cint_t side_factors[7],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4,
                             const cint_t i5,
                             const cint_t i6) {
    return
      ((size_t)
       ((i0 UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((i3 UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((i4 UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((i5 UPCXXA_ARRAY_BASE(5))*side_factors[5])
       + (i6 UPCXXA_ARRAY_BASE(6)));
  }
};

template<> struct var_indexer_spec<7, column> {
  inline static size_t index(const cint_t base[7],
                             const cint_t stride[7],
                             const cint_t side_factors[7],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4,
                             const cint_t i5,
                             const cint_t i6) {
    return
      ((size_t)
       (i0 UPCXXA_ARRAY_BASE(0))
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((i3 UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((i4 UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((i5 UPCXXA_ARRAY_BASE(5))*side_factors[5])
       + ((i6 UPCXXA_ARRAY_BASE(6))*side_factors[6]));
  }
};

template<> struct var_indexer_spec<8, strided> {
  inline static size_t index(const cint_t base[8],
                             const cint_t stride[8],
                             const cint_t side_factors[8],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4,
                             const cint_t i5,
                             const cint_t i6,
                             const cint_t i7) {
    return
      ((size_t)
       UPCXXA_PFAST_DIVIDE(((i0 - base[0])*side_factors[0]), stride[0])
       + UPCXXA_PFAST_DIVIDE(((i1 - base[1])*side_factors[1]), stride[1])
       + UPCXXA_PFAST_DIVIDE(((i2 - base[2])*side_factors[2]), stride[2])
       + UPCXXA_PFAST_DIVIDE(((i3 - base[3])*side_factors[3]), stride[3])
       + UPCXXA_PFAST_DIVIDE(((i4 - base[4])*side_factors[4]), stride[4])
       + UPCXXA_PFAST_DIVIDE(((i5 - base[5])*side_factors[5]), stride[5])
       + UPCXXA_PFAST_DIVIDE(((i6 - base[6])*side_factors[6]), stride[6])
       + UPCXXA_PFAST_DIVIDE(((i7 - base[7])*side_factors[7]), stride[7]));
  }
};

template<> struct var_indexer_spec<8, unstrided> {
  inline static size_t index(const cint_t base[8],
                             const cint_t stride[8],
                             const cint_t side_factors[8],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4,
                             const cint_t i5,
                             const cint_t i6,
                             const cint_t i7) {
    return
      ((size_t)
       ((i0 UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((i3 UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((i4 UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((i5 UPCXXA_ARRAY_BASE(5))*side_factors[5])
       + ((i6 UPCXXA_ARRAY_BASE(6))*side_factors[6])
       + ((i7 UPCXXA_ARRAY_BASE(7))*side_factors[7]));
  }
};

template<> struct var_indexer_spec<8, row> {
  inline static size_t index(const cint_t base[8],
                             const cint_t stride[8],
                             const cint_t side_factors[8],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4,
                             const cint_t i5,
                             const cint_t i6,
                             const cint_t i7) {
    return
      ((size_t)
       ((i0 UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((i3 UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((i4 UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((i5 UPCXXA_ARRAY_BASE(5))*side_factors[5])
       + ((i6 UPCXXA_ARRAY_BASE(6))*side_factors[6])
       + (i7 UPCXXA_ARRAY_BASE(7)));
  }
};

template<> struct var_indexer_spec<8, column> {
  inline static size_t index(const cint_t base[8],
                             const cint_t stride[8],
                             const cint_t side_factors[8],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4,
                             const cint_t i5,
                             const cint_t i6,
                             const cint_t i7) {
    return
      ((size_t)
       (i0 UPCXXA_ARRAY_BASE(0))
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((i3 UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((i4 UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((i5 UPCXXA_ARRAY_BASE(5))*side_factors[5])
       + ((i6 UPCXXA_ARRAY_BASE(6))*side_factors[6])
       + ((i7 UPCXXA_ARRAY_BASE(7))*side_factors[7]));
  }
};

template<> struct var_indexer_spec<9, strided> {
  inline static size_t index(const cint_t base[9],
                             const cint_t stride[9],
                             const cint_t side_factors[9],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4,
                             const cint_t i5,
                             const cint_t i6,
                             const cint_t i7,
                             const cint_t i8) {
    return
      ((size_t)
       UPCXXA_PFAST_DIVIDE(((i0 - base[0])*side_factors[0]), stride[0])
       + UPCXXA_PFAST_DIVIDE(((i1 - base[1])*side_factors[1]), stride[1])
       + UPCXXA_PFAST_DIVIDE(((i2 - base[2])*side_factors[2]), stride[2])
       + UPCXXA_PFAST_DIVIDE(((i3 - base[3])*side_factors[3]), stride[3])
       + UPCXXA_PFAST_DIVIDE(((i4 - base[4])*side_factors[4]), stride[4])
       + UPCXXA_PFAST_DIVIDE(((i5 - base[5])*side_factors[5]), stride[5])
       + UPCXXA_PFAST_DIVIDE(((i6 - base[6])*side_factors[6]), stride[6])
       + UPCXXA_PFAST_DIVIDE(((i7 - base[7])*side_factors[7]), stride[7])
       + UPCXXA_PFAST_DIVIDE(((i8 - base[8])*side_factors[8]), stride[8]));
  }
};

template<> struct var_indexer_spec<9, unstrided> {
  inline static size_t index(const cint_t base[9],
                             const cint_t stride[9],
                             const cint_t side_factors[9],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4,
                             const cint_t i5,
                             const cint_t i6,
                             const cint_t i7,
                             const cint_t i8) {
    return
      ((size_t)
       ((i0 UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((i3 UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((i4 UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((i5 UPCXXA_ARRAY_BASE(5))*side_factors[5])
       + ((i6 UPCXXA_ARRAY_BASE(6))*side_factors[6])
       + ((i7 UPCXXA_ARRAY_BASE(7))*side_factors[7])
       + ((i8 UPCXXA_ARRAY_BASE(8))*side_factors[8]));
  }
};

template<> struct var_indexer_spec<9, row> {
  inline static size_t index(const cint_t base[9],
                             const cint_t stride[9],
                             const cint_t side_factors[9],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4,
                             const cint_t i5,
                             const cint_t i6,
                             const cint_t i7,
                             const cint_t i8) {
    return
      ((size_t)
       ((i0 UPCXXA_ARRAY_BASE(0))*side_factors[0])
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((i3 UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((i4 UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((i5 UPCXXA_ARRAY_BASE(5))*side_factors[5])
       + ((i6 UPCXXA_ARRAY_BASE(6))*side_factors[6])
       + ((i7 UPCXXA_ARRAY_BASE(7))*side_factors[7])
       + (i8 UPCXXA_ARRAY_BASE(8)));
  }
};

template<> struct var_indexer_spec<9, column> {
  inline static size_t index(const cint_t base[9],
                             const cint_t stride[9],
                             const cint_t side_factors[9],
                             const char *where,
                             const cint_t i0,
                             const cint_t i1,
                             const cint_t i2,
                             const cint_t i3,
                             const cint_t i4,
                             const cint_t i5,
                             const cint_t i6,
                             const cint_t i7,
                             const cint_t i8) {
    return
      ((size_t)
       (i0 UPCXXA_ARRAY_BASE(0))
       + ((i1 UPCXXA_ARRAY_BASE(1))*side_factors[1])
       + ((i2 UPCXXA_ARRAY_BASE(2))*side_factors[2])
       + ((i3 UPCXXA_ARRAY_BASE(3))*side_factors[3])
       + ((i4 UPCXXA_ARRAY_BASE(4))*side_factors[4])
       + ((i5 UPCXXA_ARRAY_BASE(5))*side_factors[5])
       + ((i6 UPCXXA_ARRAY_BASE(6))*side_factors[6])
       + ((i7 UPCXXA_ARRAY_BASE(7))*side_factors[7])
       + ((i8 UPCXXA_ARRAY_BASE(8))*side_factors[8]));
  }
};

