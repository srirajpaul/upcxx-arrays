    ref_type operator()(cint_t i0) {
#ifdef UPCXXA_USE_CXX11
      static_assert(N == 1,
                    "array of arity N must be indexed with N arguments");
#endif
#if defined(UPCXXA_USE_CXX11) || !UPCXXA_BOUNDS_CHECKING
      return
        rebased[var_indexer_spec<N, S>::index(_array.base,
                                              _array.stride,
                                              _array.side_factors,
                                              UPCXXA_BC_PARAM(_array._domain)
                                              "operator()",
                                              i0)];
#else
      // punt to operator[] to perform bounds checking
      return operator[](PT(i0));
#endif
    }

    const ref_type operator()(cint_t i0) const {
#ifdef UPCXXA_USE_CXX11
      static_assert(N == 1,
                    "array of arity N must be indexed with N arguments");
#endif
#if defined(UPCXXA_USE_CXX11) || !UPCXXA_BOUNDS_CHECKING
      return
        rebased[var_indexer_spec<N, S>::index(_array.base,
                                              _array.stride,
                                              _array.side_factors,
                                              UPCXXA_BC_PARAM(_array._domain)
                                              "operator()",
                                              i0)];
#else
      // punt to operator[] to perform bounds checking
      return operator[](PT(i0));
#endif
    }

    ref_type operator()(cint_t i0, cint_t i1) {
#ifdef UPCXXA_USE_CXX11
      static_assert(N == 2,
                    "array of arity N must be indexed with N arguments");
#endif
#if defined(UPCXXA_USE_CXX11) || !UPCXXA_BOUNDS_CHECKING
      return
        rebased[var_indexer_spec<N, S>::index(_array.base,
                                              _array.stride,
                                              _array.side_factors,
                                              UPCXXA_BC_PARAM(_array._domain)
                                              "operator()",
                                              i0, i1)];
#else
      // punt to operator[] to perform bounds checking
      return operator[](PT(i0, i1));
#endif
    }

    const ref_type operator()(cint_t i0, cint_t i1) const {
#ifdef UPCXXA_USE_CXX11
      static_assert(N == 2,
                    "array of arity N must be indexed with N arguments");
#endif
#if defined(UPCXXA_USE_CXX11) || !UPCXXA_BOUNDS_CHECKING
      return
        rebased[var_indexer_spec<N, S>::index(_array.base,
                                              _array.stride,
                                              _array.side_factors,
                                              UPCXXA_BC_PARAM(_array._domain)
                                              "operator()",
                                              i0, i1)];
#else
      // punt to operator[] to perform bounds checking
      return operator[](PT(i0, i1));
#endif
    }

    ref_type operator()(cint_t i0, cint_t i1, cint_t i2) {
#ifdef UPCXXA_USE_CXX11
      static_assert(N == 3,
                    "array of arity N must be indexed with N arguments");
#endif
#if defined(UPCXXA_USE_CXX11) || !UPCXXA_BOUNDS_CHECKING
      return
        rebased[var_indexer_spec<N, S>::index(_array.base,
                                              _array.stride,
                                              _array.side_factors,
                                              UPCXXA_BC_PARAM(_array._domain)
                                              "operator()",
                                              i0, i1, i2)];
#else
      // punt to operator[] to perform bounds checking
      return operator[](PT(i0, i1, i2));
#endif
    }

    const ref_type operator()(cint_t i0, cint_t i1, cint_t i2) const {
#ifdef UPCXXA_USE_CXX11
      static_assert(N == 3,
                    "array of arity N must be indexed with N arguments");
#endif
#if defined(UPCXXA_USE_CXX11) || !UPCXXA_BOUNDS_CHECKING
      return
        rebased[var_indexer_spec<N, S>::index(_array.base,
                                              _array.stride,
                                              _array.side_factors,
                                              UPCXXA_BC_PARAM(_array._domain)
                                              "operator()",
                                              i0, i1, i2)];
#else
      // punt to operator[] to perform bounds checking
      return operator[](PT(i0, i1, i2));
#endif
    }

    ref_type operator()(cint_t i0, cint_t i1, cint_t i2, cint_t i3) {
#ifdef UPCXXA_USE_CXX11
      static_assert(N == 4,
                    "array of arity N must be indexed with N arguments");
#endif
#if defined(UPCXXA_USE_CXX11) || !UPCXXA_BOUNDS_CHECKING
      return
        rebased[var_indexer_spec<N, S>::index(_array.base,
                                              _array.stride,
                                              _array.side_factors,
                                              UPCXXA_BC_PARAM(_array._domain)
                                              "operator()",
                                              i0, i1, i2, i3)];
#else
      // punt to operator[] to perform bounds checking
      return operator[](PT(i0, i1, i2, i3));
#endif
    }

    const ref_type operator()(cint_t i0, cint_t i1, cint_t i2,
                              cint_t i3) const {
#ifdef UPCXXA_USE_CXX11
      static_assert(N == 4,
                    "array of arity N must be indexed with N arguments");
#endif
#if defined(UPCXXA_USE_CXX11) || !UPCXXA_BOUNDS_CHECKING
      return
        rebased[var_indexer_spec<N, S>::index(_array.base,
                                              _array.stride,
                                              _array.side_factors,
                                              UPCXXA_BC_PARAM(_array._domain)
                                              "operator()",
                                              i0, i1, i2, i3)];
#else
      // punt to operator[] to perform bounds checking
      return operator[](PT(i0, i1, i2, i3));
#endif
    }

    ref_type operator()(cint_t i0, cint_t i1, cint_t i2, cint_t i3,
                        cint_t i4) {
#ifdef UPCXXA_USE_CXX11
      static_assert(N == 5,
                    "array of arity N must be indexed with N arguments");
#endif
#if defined(UPCXXA_USE_CXX11) || !UPCXXA_BOUNDS_CHECKING
      return
        rebased[var_indexer_spec<N, S>::index(_array.base,
                                              _array.stride,
                                              _array.side_factors,
                                              UPCXXA_BC_PARAM(_array._domain)
                                              "operator()",
                                              i0, i1, i2, i3, i4)];
#else
      // punt to operator[] to perform bounds checking
      return operator[](PT(i0, i1, i2, i3, i4));
#endif
    }

    const ref_type operator()(cint_t i0, cint_t i1, cint_t i2,
                              cint_t i3, cint_t i4) const {
#ifdef UPCXXA_USE_CXX11
      static_assert(N == 5,
                    "array of arity N must be indexed with N arguments");
#endif
#if defined(UPCXXA_USE_CXX11) || !UPCXXA_BOUNDS_CHECKING
      return
        rebased[var_indexer_spec<N, S>::index(_array.base,
                                              _array.stride,
                                              _array.side_factors,
                                              UPCXXA_BC_PARAM(_array._domain)
                                              "operator()",
                                              i0, i1, i2, i3, i4)];
#else
      // punt to operator[] to perform bounds checking
      return operator[](PT(i0, i1, i2, i3, i4));
#endif
    }

    ref_type operator()(cint_t i0, cint_t i1, cint_t i2, cint_t i3,
                        cint_t i4, cint_t i5) {
#ifdef UPCXXA_USE_CXX11
      static_assert(N == 6,
                    "array of arity N must be indexed with N arguments");
#endif
#if defined(UPCXXA_USE_CXX11) || !UPCXXA_BOUNDS_CHECKING
      return
        rebased[var_indexer_spec<N, S>::index(_array.base,
                                              _array.stride,
                                              _array.side_factors,
                                              UPCXXA_BC_PARAM(_array._domain)
                                              "operator()",
                                              i0, i1, i2, i3, i4, i5)];
#else
      // punt to operator[] to perform bounds checking
      return operator[](PT(i0, i1, i2, i3, i4, i5));
#endif
    }

    const ref_type operator()(cint_t i0, cint_t i1, cint_t i2,
                              cint_t i3, cint_t i4, cint_t i5) const {
#ifdef UPCXXA_USE_CXX11
      static_assert(N == 6,
                    "array of arity N must be indexed with N arguments");
#endif
#if defined(UPCXXA_USE_CXX11) || !UPCXXA_BOUNDS_CHECKING
      return
        rebased[var_indexer_spec<N, S>::index(_array.base,
                                              _array.stride,
                                              _array.side_factors,
                                              UPCXXA_BC_PARAM(_array._domain)
                                              "operator()",
                                              i0, i1, i2, i3, i4, i5)];
#else
      // punt to operator[] to perform bounds checking
      return operator[](PT(i0, i1, i2, i3, i4, i5));
#endif
    }

    ref_type operator()(cint_t i0, cint_t i1, cint_t i2, cint_t i3,
                        cint_t i4, cint_t i5, cint_t i6) {
#ifdef UPCXXA_USE_CXX11
      static_assert(N == 7,
                    "array of arity N must be indexed with N arguments");
#endif
#if defined(UPCXXA_USE_CXX11) || !UPCXXA_BOUNDS_CHECKING
      return
        rebased[var_indexer_spec<N, S>::index(_array.base,
                                              _array.stride,
                                              _array.side_factors,
                                              UPCXXA_BC_PARAM(_array._domain)
                                              "operator()",
                                              i0, i1, i2, i3, i4, i5,
                                              i6)];
#else
      // punt to operator[] to perform bounds checking
      return operator[](PT(i0, i1, i2, i3, i4, i5, i6));
#endif
    }

    const ref_type operator()(cint_t i0, cint_t i1, cint_t i2,
                              cint_t i3, cint_t i4, cint_t i5,
                              cint_t i6) const {
#ifdef UPCXXA_USE_CXX11
      static_assert(N == 7,
                    "array of arity N must be indexed with N arguments");
#endif
#if defined(UPCXXA_USE_CXX11) || !UPCXXA_BOUNDS_CHECKING
      return
        rebased[var_indexer_spec<N, S>::index(_array.base,
                                              _array.stride,
                                              _array.side_factors,
                                              UPCXXA_BC_PARAM(_array._domain)
                                              "operator()",
                                              i0, i1, i2, i3, i4, i5,
                                              i6)];
#else
      // punt to operator[] to perform bounds checking
      return operator[](PT(i0, i1, i2, i3, i4, i5, i6));
#endif
    }

    ref_type operator()(cint_t i0, cint_t i1, cint_t i2, cint_t i3,
                        cint_t i4, cint_t i5, cint_t i6, cint_t i7) {
#ifdef UPCXXA_USE_CXX11
      static_assert(N == 8,
                    "array of arity N must be indexed with N arguments");
#endif
#if defined(UPCXXA_USE_CXX11) || !UPCXXA_BOUNDS_CHECKING
      return
        rebased[var_indexer_spec<N, S>::index(_array.base,
                                              _array.stride,
                                              _array.side_factors,
                                              UPCXXA_BC_PARAM(_array._domain)
                                              "operator()",
                                              i0, i1, i2, i3, i4, i5,
                                              i6, i7)];
#else
      // punt to operator[] to perform bounds checking
      return operator[](PT(i0, i1, i2, i3, i4, i5, i6, i7));
#endif
    }

    const ref_type operator()(cint_t i0, cint_t i1, cint_t i2,
                              cint_t i3, cint_t i4, cint_t i5,
                              cint_t i6, cint_t i7) const {
#ifdef UPCXXA_USE_CXX11
      static_assert(N == 8,
                    "array of arity N must be indexed with N arguments");
#endif
#if defined(UPCXXA_USE_CXX11) || !UPCXXA_BOUNDS_CHECKING
      return
        rebased[var_indexer_spec<N, S>::index(_array.base,
                                              _array.stride,
                                              _array.side_factors,
                                              UPCXXA_BC_PARAM(_array._domain)
                                              "operator()",
                                              i0, i1, i2, i3, i4, i5,
                                              i6, i7)];
#else
      // punt to operator[] to perform bounds checking
      return operator[](PT(i0, i1, i2, i3, i4, i5, i6, i7));
#endif
    }

    ref_type operator()(cint_t i0, cint_t i1, cint_t i2, cint_t i3,
                        cint_t i4, cint_t i5, cint_t i6, cint_t i7,
                        cint_t i8) {
#ifdef UPCXXA_USE_CXX11
      static_assert(N == 9,
                    "array of arity N must be indexed with N arguments");
#endif
#if defined(UPCXXA_USE_CXX11) || !UPCXXA_BOUNDS_CHECKING
      return
        rebased[var_indexer_spec<N, S>::index(_array.base,
                                              _array.stride,
                                              _array.side_factors,
                                              UPCXXA_BC_PARAM(_array._domain)
                                              "operator()",
                                              i0, i1, i2, i3, i4, i5,
                                              i6, i7, i8)];
#else
      // punt to operator[] to perform bounds checking
      return operator[](PT(i0, i1, i2, i3, i4, i5, i6, i7, i8));
#endif
    }

    const ref_type operator()(cint_t i0, cint_t i1, cint_t i2,
                              cint_t i3, cint_t i4, cint_t i5,
                              cint_t i6, cint_t i7, cint_t i8) const {
#ifdef UPCXXA_USE_CXX11
      static_assert(N == 9,
                    "array of arity N must be indexed with N arguments");
#endif
#if defined(UPCXXA_USE_CXX11) || !UPCXXA_BOUNDS_CHECKING
      return
        rebased[var_indexer_spec<N, S>::index(_array.base,
                                              _array.stride,
                                              _array.side_factors,
                                              UPCXXA_BC_PARAM(_array._domain)
                                              "operator()",
                                              i0, i1, i2, i3, i4, i5,
                                              i6, i7, i8)];
#else
      // punt to operator[] to perform bounds checking
      return operator[](PT(i0, i1, i2, i3, i4, i5, i6, i7, i8));
#endif
    }

