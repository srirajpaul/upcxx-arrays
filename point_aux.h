inline static point<1> PT(cint_t i0) {
  point<1> p = {{ i0 }};
  return p;
}

inline static point<2> PT(cint_t i0, cint_t i1) {
  point<2> p = {{ i0, i1 }};
  return p;
}

inline static point<3> PT(cint_t i0, cint_t i1, cint_t i2) {
  point<3> p = {{ i0, i1, i2 }};
  return p;
}

inline static point<4> PT(cint_t i0, cint_t i1, cint_t i2, cint_t i3) {
  point<4> p = {{ i0, i1, i2, i3 }};
  return p;
}

inline static point<5> PT(cint_t i0, cint_t i1, cint_t i2, cint_t i3,
                          cint_t i4) {
  point<5> p = {{ i0, i1, i2, i3, i4 }};
  return p;
}

inline static point<6> PT(cint_t i0, cint_t i1, cint_t i2, cint_t i3,
                          cint_t i4, cint_t i5) {
  point<6> p = {{ i0, i1, i2, i3, i4, i5 }};
  return p;
}

inline static point<7> PT(cint_t i0, cint_t i1, cint_t i2, cint_t i3,
                          cint_t i4, cint_t i5, cint_t i6) {
  point<7> p = {{ i0, i1, i2, i3, i4, i5, i6 }};
  return p;
}

inline static point<8> PT(cint_t i0, cint_t i1, cint_t i2, cint_t i3,
                          cint_t i4, cint_t i5, cint_t i6, cint_t i7) {
  point<8> p = {{ i0, i1, i2, i3, i4, i5, i6, i7 }};
  return p;
}

inline static point<9> PT(cint_t i0, cint_t i1, cint_t i2, cint_t i3,
                          cint_t i4, cint_t i5, cint_t i6, cint_t i7,
                          cint_t i8) {
  point<9> p = {{ i0, i1, i2, i3, i4, i5, i6, i7, i8 }};
  return p;
}

