#pragma once

/* wrapper_tools.h
 * This file contains auxiliary macros, declarations, and types used
 * by the ndarray wrapper class.
 */

#include <string>

namespace upcxx {
  ////////////////////////////////////////////////////////////////////
  /* Array flags, represented as separate types */

  struct arraydefault {
    static std::string name() { return "arraydefault"; }
  };

  struct local {
    static std::string name() { return "local"; }
  };
  struct global {
    static std::string name() { return "global"; }
  };

  struct strided {
    static std::string name() { return "strided"; }
  };
  struct unstrided {
    static std::string name() { return "unstrided"; }
  };
  struct row {
    static std::string name() { return "row"; }
  };
  struct column {
    static std::string name() { return "column"; }
  };

  /* Default values for globalness and stridedness */
  typedef local default_globalness;
  typedef UPCXXA_DEFAULT_STRIDEDNESS default_stridedness;

  /* Combinations of multiple array flags */

  template<class F1, class F2> struct globalness {
    typedef default_globalness type;
  };

  template<class F1, class F2> struct stridedness {
    typedef default_stridedness type;
  };

  template<class F1, class F2> struct allow_globalness_conversion {
    enum { value = 0 };
  };

  template<class F1, class F2> struct allow_stridedness_conversion {
    enum { value = 0 };
  };

#define UPCXXA_ARRAYFLAG_COMBO(combinator, f1, f2, result)      \
  template<> struct combinator<f1, f2> {                        \
    typedef result type;                                        \
  }
#define UPCXXA_ARRAYFLAG_ALLOW_CONV(type, f1, f2)       \
  template<> struct allow_##type##_conversion<f1, f2> { \
    enum { value = 1 };                                 \
  }
#include "arrayflag_tools.h"

#define UPCXXA_STRIDEDNESS_STR(f1, f2)          \
  stridedness<f1, f2>::type::name()

#define UPCXXA_XGLOBALNESS(f1, f2)              \
  typename globalness<f1, f2>::type
#define UPCXXA_XSTRIDEDNESS(f1, f2)             \
  typename stridedness<f1, f2>::type

#define UPCXXA_ALLOWICONV(f1, f2, g1, g2)                       \
  (allow_globalness_conversion<UPCXXA_XGLOBALNESS(g1, g2),      \
   UPCXXA_XGLOBALNESS(f1, f2)>::value &                         \
   allow_stridedness_conversion<UPCXXA_XSTRIDEDNESS(g1, g2),    \
   UPCXXA_XSTRIDEDNESS(f1, f2)>::value)

#define UPCXXA_FLAG_EQUAL(f1, f2) domain_utils::is_same<f1, f2>::value

  ////////////////////////////////////////////////////////////////////
  /* Globalness and stridedness of array wrappers */

  /* Keep track of stridedness resulting from various array
   * transformations */
  template<class S> struct transform_stride {
    typedef strided slice;
    typedef strided islice;
    typedef strided translate;
    typedef strided permute;
    typedef strided project;
    typedef strided inject;
  };

  template<> struct transform_stride<unstrided> {
    typedef unstrided slice;
    typedef unstrided islice;
    typedef unstrided translate;
    typedef unstrided permute;
    typedef strided project;
    typedef strided inject;
  };

  template<> struct transform_stride<row> {
    typedef unstrided slice;
    typedef row islice;
    typedef row translate;
    typedef unstrided permute;
    typedef strided project;
    typedef strided inject;
  };

  template<> struct transform_stride<column> {
    typedef unstrided slice;
    typedef unstrided islice;
    typedef column translate;
    typedef unstrided permute;
    typedef strided project;
    typedef strided inject;
  };

#define UPCXXA_XFER_STRIDE(op) G, typename transform_stride<S>::op

  ////////////////////////////////////////////////////////////////////
  /* Relationships between wrapper type and implementation types */

  template<class T, int N, class G>
  struct array_impl_type {
    typedef UPCXXA_LOCAL_ARRAY_T<T, N> arr_type;
    typedef T *ptr_type;
    typedef T &ref_type;
    static T *localize(const ptr_type p) {
      return (T *) p;
    }
    static UPCXXA_GPTR_TO_T globalize(const ptr_type p) {
      UPCXXA_GPTR_TO_T g;
      UPCXXA_GLOBALIZE(g, (T *) p);
      return g;
    }
    static ptr_type convert(const T *p) {
      return (T *) p;
    }
#ifdef UPCXXA_MEMORY_DISTRIBUTED
    static ptr_type convert(const UPCXXA_GPTR_TO_T p) {
      return (T *) UPCXXA_TO_LOCAL(p);
    }
#endif
  };

  template<class T, int N> struct array_impl_type<T, N, global> {
    typedef UPCXXA_GLOBAL_ARRAY_T<T, N> arr_type;
    typedef UPCXXA_GPTR_TO_T ptr_type;
    typedef UPCXXA_GREF_TO_T ref_type;
    static T *localize(const ptr_type p) {
      return (T *) UPCXXA_TO_LOCAL(p);
    }
    static UPCXXA_GPTR_TO_T globalize(const ptr_type p) {
      return (UPCXXA_GPTR_TO_T) p;
    }
    static ptr_type convert(const T *p) {
      UPCXXA_GPTR_TO_T g;
      UPCXXA_GLOBALIZE(g, (T *) p);
      return g;
    }
#ifdef UPCXXA_MEMORY_DISTRIBUTED
    static ptr_type convert(const UPCXXA_GPTR_TO_T p) {
      return (UPCXXA_GPTR_TO_T) p;
    }
#endif
  };

  ////////////////////////////////////////////////////////////////////
  /* Array wrapper indexing using rebased pointer stored in the
   * wrapper */

#ifdef UPCXXA_NO_REBASE
# define UPCXXA_ARRAY_OBJ_BASE(arr, idx) - arr.base[idx]
# define UPCXXA_ARRAY_BASE(idx) - base[idx]
# define UPCXXA_PARAM_BASE - base
# define UPCXXA_BASE_PARAM(param) param,
#else
# define UPCXXA_ARRAY_OBJ_BASE(arr, idx)
# define UPCXXA_ARRAY_BASE(idx)
# define UPCXXA_PARAM_BASE
# define UPCXXA_BASE_PARAM(param)
#endif

  /* ND index computation for strided arrays */
  template<int N, class S> struct rebased_indexer {
    inline static size_t compute(const cint_t base[N],
                                 const cint_t stride[N],
                                 const cint_t side_factors[N],
                                 UPCXXA_BC_PARAM(const rectdomain<N> &dom)
                                 const char *where,
                                 const cint_t px[N]) {
      /* strided arrays are not rebased */
      return index_computer<N>::compute(base, stride, side_factors,
                                        UPCXXA_BC_PARAM(dom) where, px);
    }
  };

  /* ND index computation for unstrided arrays */
  template<int N> struct rebased_indexer<N, unstrided> {
    inline static size_t compute(const cint_t base[N],
                                 const cint_t stride[N],
                                 const cint_t side_factors[N],
                                 UPCXXA_BC_PARAM(const rectdomain<N> &dom)
                                 const char *where,
                                 const cint_t px[N]) {
      int i;
      size_t sum = 0;
      for (i = N-1; i >= 0; i--) {
        const cint_t q = px[i];
        const cint_t qm = q UPCXXA_ARRAY_BASE(i);
        size_t delta = (size_t) (qm * side_factors[i]);

        /* bounds check */
        UPCXXA_CHECK_BOUND(where, px, q, dom.lwb().x[i],
                           dom.upb().x[i], dom.stride().x[i], dom,
                           UPCXXA_ENABLE_ARR_MSG,
                           UPCXXA_DISABLE_ARR_MSG);

        sum += delta;
      }
      return sum;
    }
  };

  /* ND index computation for row arrays */
  template<int N> struct rebased_indexer<N, row> {
    inline static size_t compute(const cint_t base[N],
                                 const cint_t stride[N],
                                 const cint_t side_factors[N],
                                 UPCXXA_BC_PARAM(const rectdomain<N> &dom)
                                 const char *where,
                                 const cint_t px[N]) {
      int i;
      size_t sum = (size_t) (px[N-1] UPCXXA_ARRAY_BASE(N-1));
      for (i = N-2; i >= 0; i--) {
        const cint_t q = px[i];
        const cint_t qm = q UPCXXA_ARRAY_BASE(i);
        size_t delta = (size_t) (qm * side_factors[i]);

        /* bounds check */
        UPCXXA_CHECK_BOUND(where, px, q, dom.lwb().x[i],
                           dom.upb().x[i], dom.stride().x[i], dom,
                           UPCXXA_ENABLE_ARR_MSG,
                           UPCXXA_DISABLE_ARR_MSG);

        sum += delta;
      }
      return sum;
    }
  };

  /* ND index computation for column-major arrays */
  template<int N> struct rebased_indexer<N, column> {
    inline static size_t compute(const cint_t base[N],
                                 const cint_t stride[N],
                                 const cint_t side_factors[N],
                                 UPCXXA_BC_PARAM(const rectdomain<N> &dom)
                                 const char *where,
                                 const cint_t px[N]) {
      int i;
      size_t sum = 0;
      for (i = N-1; i >= 1; i--) {
        const cint_t q = px[i];
        const cint_t qm = q UPCXXA_ARRAY_BASE(i);
        size_t delta = (size_t) (qm * side_factors[i]);

        /* bounds check */
        UPCXXA_CHECK_BOUND(where, px, q, dom.lwb().x[i],
                           dom.upb().x[i], dom.stride().x[i], dom,
                           UPCXXA_ENABLE_ARR_MSG,
                           UPCXXA_DISABLE_ARR_MSG);

        sum += delta;
      }
      return sum + (size_t) (px[0] UPCXXA_ARRAY_BASE(0));
    }
  };

  /* Specialized versions are now generated by Python code; they are
   * included at the end of this file */

  ////////////////////////////////////////////////////////////////////

#define UPCXXA_GET_ARR_TYPE(T, N, G)            \
  typename array_impl_type<T, N, G>::arr_type
#define UPCXXA_GET_PTR_TYPE(T, N, G)            \
  typename array_impl_type<T, N, G>::ptr_type
#define UPCXXA_GET_REF_TYPE(T, N, G)            \
  typename array_impl_type<T, N, G>::ref_type

  ////////////////////////////////////////////////////////////////////

  /* Heavyweight partial indexing of arrays. Produces smaller array
   * descriptors that can be used arbitrarily. */
  template<class T, int N, class G, class S>
  struct index_slicer {
    typedef ndarray<T, N-1, UPCXXA_XFER_STRIDE(islice)> type;
    template<class F1, class F2>
    inline static type slice(ndarray<T, N, F1, F2> &x, cint_t j) {
      assert(UPCXXA_FLAG_EQUAL(UPCXXA_XGLOBALNESS(F1, F2), G) &&
             UPCXXA_FLAG_EQUAL(UPCXXA_XSTRIDEDNESS(F1, F2), S));
      return (type) x.slice(1, j);
    }
  };

  /* Heavyweight partial index specialization for 1D arrays. Performs
   * bounds checking if it's enabled. */ 
  template<class T, class G, class S>
  struct index_slicer<T, 1, G, S> {
    typedef UPCXXA_GET_REF_TYPE(T, 1, G) type;
    template<class F1, class F2>
    inline static type slice(ndarray<T, 1, F1, F2> &x, cint_t j) {
      assert(UPCXXA_FLAG_EQUAL(UPCXXA_XGLOBALNESS(F1, F2), G) &&
             UPCXXA_FLAG_EQUAL(UPCXXA_XSTRIDEDNESS(F1, F2), S));
      size_t idx =
        rebased_indexer<1,
                        S>::compute(x._array.base,
                                    x._array.stride,
                                    x._array.side_factors,
                                    UPCXXA_BC_PARAM(x._array._domain)
                                    "operator[]",
                                    &j);
      return x.rebased[idx];
    }
  };

  ////////////////////////////////////////////////////////////////////

#ifdef UPCXXA_USE_CXX11
# define UPCXXA_VAR_INDEX (N - sizeof...(is) - 1)

  /* Full indexing of strided arrays using variadic templates. */
  template<int N, class S>
  struct var_indexer {
    template<class... Is>
    static inline size_t index(const cint_t base[N],
                               const cint_t stride[N],
                               const cint_t side_factors[N],
                               UPCXXA_BC_PARAM(const rectdomain<N> &dom)
                               const char *where,
                               const cint_t i1, const Is... is) {
      UPCXXA_CHECK_BOUND(where, no_point, i1,
                         dom.lwb()[UPCXXA_VAR_INDEX+1],
                         dom.upb()[UPCXXA_VAR_INDEX+1],
                         dom.stride()[UPCXXA_VAR_INDEX+1], dom,
                         UPCXXA_DISABLE_ARR_MSG,
                         UPCXXA_ENABLE_ARR_MSG);
      // strided arrays are never rebased
      return ((size_t)
              UPCXXA_PFAST_DIVIDE((i1-base[UPCXXA_VAR_INDEX])*
                                  side_factors[UPCXXA_VAR_INDEX],
                                  stride[UPCXXA_VAR_INDEX])) +
        index(base, stride, side_factors, UPCXXA_BC_PARAM(dom) where,
              is...);
    }
    static inline size_t index(const cint_t base[N],
                               const cint_t stride[N],
                               const cint_t side_factors[N],
                               UPCXXA_BC_PARAM(const rectdomain<N> &dom)
                               const char *where,
                               const cint_t i) {
      UPCXXA_CHECK_BOUND(where, no_point, i, dom.lwb()[N],
                         dom.upb()[N], dom.stride()[N], dom,
                         UPCXXA_DISABLE_ARR_MSG,
                         UPCXXA_ENABLE_ARR_MSG);
      // strided arrays are never rebased
      return (size_t)
        UPCXXA_PFAST_DIVIDE((i-base[N-1])*side_factors[N-1],
                            stride[N-1]);
    }
  };

  /* Full indexing of unstrided arrays using variadic templates. */
  template<int N>
  struct var_indexer<N, unstrided> {
    template<class... Is>
    static inline size_t index(const cint_t base[N],
                               const cint_t stride[N],
                               const cint_t side_factors[N],
                               UPCXXA_BC_PARAM(const rectdomain<N> &dom)
                               const char *where,
                               const cint_t i1, const Is... is) {
      UPCXXA_CHECK_BOUND(where, no_point, i1,
                         dom.lwb()[UPCXXA_VAR_INDEX+1],
                         dom.upb()[UPCXXA_VAR_INDEX+1], 1, dom,
                         UPCXXA_DISABLE_ARR_MSG,
                         UPCXXA_ENABLE_ARR_MSG);
      return ((size_t)
              ((i1 UPCXXA_ARRAY_BASE(UPCXXA_VAR_INDEX))*
               side_factors[UPCXXA_VAR_INDEX])) +
        index(base, stride, side_factors, UPCXXA_BC_PARAM(dom) where,
              is...);
    }
    static inline size_t index(const cint_t base[N],
                               const cint_t stride[N],
                               const cint_t side_factors[N],
                               UPCXXA_BC_PARAM(const rectdomain<N> &dom)
                               const char *where,
                               const cint_t i) {
      UPCXXA_CHECK_BOUND(where, no_point, i, dom.lwb()[N],
                         dom.upb()[N], 1, dom, UPCXXA_DISABLE_ARR_MSG,
                         UPCXXA_ENABLE_ARR_MSG);
      return (size_t) ((i UPCXXA_ARRAY_BASE(N-1))*side_factors[N-1]);
    }
  };

  /* Full indexing of row arrays using variadic templates. */
  template<int N>
  struct var_indexer<N, row> {
    template<class... Is>
    static inline size_t index(const cint_t base[N],
                               const cint_t stride[N],
                               const cint_t side_factors[N],
                               UPCXXA_BC_PARAM(const rectdomain<N> &dom)
                               const char *where,
                               const cint_t i1, const Is... is) {
      UPCXXA_CHECK_BOUND(where, no_point, i1,
                         dom.lwb()[UPCXXA_VAR_INDEX+1],
                         dom.upb()[UPCXXA_VAR_INDEX+1], 1, dom,
                         UPCXXA_DISABLE_ARR_MSG,
                         UPCXXA_ENABLE_ARR_MSG);
      return ((size_t)
              ((i1 UPCXXA_ARRAY_BASE(UPCXXA_VAR_INDEX))*
               side_factors[UPCXXA_VAR_INDEX])) +
        index(base, stride, side_factors, UPCXXA_BC_PARAM(dom) where,
              is...);
    }
    static inline size_t index(const cint_t base[N],
                               const cint_t stride[N],
                               const cint_t side_factors[N],
                               UPCXXA_BC_PARAM(const rectdomain<N> &dom)
                               const char *where,
                               const cint_t i) {
      UPCXXA_CHECK_BOUND(where, no_point, i, dom.lwb()[N],
                         dom.upb()[N], 1, dom, UPCXXA_DISABLE_ARR_MSG,
                         UPCXXA_ENABLE_ARR_MSG);
      // last dim is contiguous
      return (size_t) (i UPCXXA_ARRAY_BASE(N-1));
    }
  };

  /* Full indexing of column arrays using variadic templates.
   */
  template<int N>
  struct var_indexer<N, column> {
    template<class... Is>
    static inline size_t index(const cint_t base[N],
                               const cint_t stride[N],
                               const cint_t side_factors[N],
                               UPCXXA_BC_PARAM(const rectdomain<N> &dom)
                               const char *where,
                               const cint_t i1, const Is... is) {
      UPCXXA_CHECK_BOUND(where, no_point, i1,
                         dom.lwb()[UPCXXA_VAR_INDEX+1],
                         dom.upb()[UPCXXA_VAR_INDEX+1], 1, dom,
                         UPCXXA_DISABLE_ARR_MSG,
                         UPCXXA_ENABLE_ARR_MSG);
      if (sizeof...(is) == N-1) {
        // first dim of column is contiguous
        return ((size_t) (i1 UPCXXA_ARRAY_BASE(UPCXXA_VAR_INDEX))) +
          index(base, stride, side_factors, UPCXXA_BC_PARAM(dom)
                where, is...);
      }
      return ((size_t)
              ((i1 UPCXXA_ARRAY_BASE(UPCXXA_VAR_INDEX))*
               side_factors[UPCXXA_VAR_INDEX])) +
        index(base, stride, side_factors, UPCXXA_BC_PARAM(dom) where,
              is...);
    }
    static inline size_t index(const cint_t base[N],
                               const cint_t stride[N],
                               const cint_t side_factors[N],
                               UPCXXA_BC_PARAM(const rectdomain<N> &dom)
                               const char *where,
                               const cint_t i) {
      UPCXXA_CHECK_BOUND(where, no_point, i, dom.lwb()[N],
                         dom.upb()[N], 1, dom, UPCXXA_DISABLE_ARR_MSG,
                         UPCXXA_ENABLE_ARR_MSG);
      return (size_t) ((i UPCXXA_ARRAY_BASE(N-1))*side_factors[N-1]);
    }
  };

  /* Framework for specialized (non-variadic) variadic indexers. The
   * generic implementation will just call the above variadic
   * indexers. */
  template<int N, class S> struct var_indexer_spec {
    template<class... Is>
    static inline size_t index(const cint_t base[N],
                               const cint_t stride[N],
                               const cint_t side_factors[N],
                               UPCXXA_BC_PARAM(const rectdomain<N> &dom)
                               const char *where,
                               const cint_t i1, const Is... is) {
      return var_indexer<N, S>::index(base, stride, side_factors,
                                      UPCXXA_BC_PARAM(dom) where, i1,
                                      is...);
    }
  };

# undef UPCXXA_VAR_INDEX
#else /* !defined(UPCXXA_USE_CXX11) */
  /* Framework for specialized (non-variadic) variadic indexers. The
   * generic implementation is not provided. */
  template<int N, class S> struct var_indexer_spec {};
#endif /* UPCXXA_USE_CXX11 */

  ////////////////////////////////////////////////////////////////////
  /* Include generated specializations. These are all
   * non-bounds-checking. */

#if !UPCXXA_BOUNDS_CHECKING
# include "wrapper_tools_specializations.h"
#endif

  ////////////////////////////////////////////////////////////////////
} // namespace upcxx

#undef UPCXXA_GET_ARR_TYPE
#undef UPCXXA_GET_PTR_TYPE
#undef UPCXXA_GET_REF_TYPE
