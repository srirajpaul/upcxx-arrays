#pragma once

#ifdef UPCXXA_MEMORY_DISTRIBUTED
# error This file should not be included in distributed builds
#endif

#define UPCXXA_PROCS 1
#define UPCXXA_MYPROC 0
#define UPCXXA_GLOBAL_PROCS 1
#define UPCXXA_GLOBAL_MYPROC 0

#define UPCXXA_DEREF                            UPCXXA_DEREF_LOCAL
#define UPCXXA_ASSIGN                           UPCXXA_ASSIGN_LOCAL
#define UPCXXA_WEAK_ASSIGN                      UPCXXA_ASSIGN
#define UPCXXA_INDEX_GLOBAL                     UPCXXA_INDEX_LOCAL

#define UPCXXA_MALLOC                           ((void *(*)(size_t)) malloc)
#define UPCXXA_FREE                             free
#define UPCXXA_SYNC()
#define UPCXXA_ARRAY_MALLOC                     ((void *(*)(size_t)) malloc)
#define UPCXXA_ARRAY_FREE                       free
#define UPCXXA_GETENV_MASTER                    getenv
#define UPCXXA_EVENT_T                          int
#define UPCXXA_DEFAULT_EVENT                    0
#define UPCXXA_EVENT_NONE                       0

#define UPCXXA_TRACE_PRINTF(parenthesized_args) ((void)0)

/* These three defs are unused in non-distributed mode */
#define UPCXXA_BULK_READ(d_addr, s_gaddr, size)
#define UPCXXA_BULK_WRITE(d_gaddr, s_addr, size)
#define UPCXXA_ASSIGN_GLOBAL_ANONYMOUS_BULK(gptr, nitems, pval)

#define UPCXXA_LOCAL_TO_GLOBAL_COPY(local, global, nitems)      \
  memcpy((global), (local), sizeof(*(local)) * (nitems))
#define UPCXXA_GPTR_TO_T T *
#define UPCXXA_GREF_TO_T T &
#define UPCXXA_GET_BOXID(gptr) 0
#define UPCXXA_BOX_T int
#define UPCXXA_TO_BOX(ptr) 0
#define UPCXXA_TO_LOCAL(ptr) (ptr)
#define UPCXXA_TO_GLOBALB(global, box, local) (global) = (local)
#define UPCXXA_EQUAL_GLOBAL(p, q) ((p) == (q))
#define UPCXXA_MYBOX 0
#define UPCXXA_IS_DIRECTLY_ADDRESSABLE(ptr) (true)
#define UPCXXA_GLOBALIZE(global, local)         \
  do { (global) = (local); } while (0)
#define UPCXXA_PROC_TO_BOXID_PROC(proc, boxid, bproc) do {      \
    boxid = bproc = 0;                                          \
  } while (0)
#define UPCXXA_BOXID_TO_BOX(boxid) 0
#define UPCXXA_BOX_TO_FIRST_PROC(box) 0

/* These six defs are unused in non-distributed mode */
#define UPCXXA_PUT_NB_BULK(event, destgaddr, srcaddr, nbytes)
#define UPCXXA_GET_NB_BULK(event, destaddr, srcgaddr, nbytes)
#define UPCXXA_GET_ARRAY(pmethod, cdesc, cdescsz, target, buffer)
#define UPCXXA_PUT_ARRAY(umethod, cdesc, cdescsz, data, datasz, target, event, newbuf)
#define UPCXXA_SPARSE_SCATTER(dsts, src, target, num, elemsz)
#define UPCXXA_SPARSE_GATHER(dst, srcs, target, num, elemsz)

#define UPCXXA_BROADCAST_RAW(dst, sender, src, size)            \
  do { if (dst != src) memcpy(dst, src, size); } while(0)
#define UPCXXA_EXCHANGE_BULK(dst, src, bytes)       \
  do {						    \
    memcpy(dst, src, bytes);			    \
  } while (0)               
