# This script generates overloads for point construction functions.
# The maximum arity of the overloads to be generated should be
# provided as a command line argument:
#   python gen_point_aux.py <max_arity>
# The default maximum arity is 9.

from sys import argv
from gen_array_wrapper_specializations import gen_list

max_arity = 9

make_point_skeleton = '''inline static point<{0}> PT({1}) {{
  point<{0}> p = {{{{ {2} }}}};
  return p;
}}
'''

def gen_make_point():
    for i in range(1, max_arity+1):
        extra = len(str(i)) - 1
        params = gen_list(i, 'i', 26 + extra, 'cint_t', 4)
        inits = gen_list(i, 'i', 18 + extra, None, 12)
        print(make_point_skeleton.format(i, params, inits))

if __name__ == '__main__':
    if len(argv) > 1:
        max_arity = int(argv[1])
    gen_make_point()
