#pragma once

#include <iostream>
#include "point.h"
#ifdef UPCXXA_USE_CXX11_INITIALIZER_LIST
# include <initializer_list>
#endif

namespace domain_utils {
  template<int N> struct n_equal_1;
  template<int N> struct n_minus_1;
  template<bool B, class T> struct enable_if;
  struct internal_type;
  template<class T, int N, int M> struct int_and_eq;
}

#define UPCXXA_SLICE_ARITY domain_utils::n_minus_1<N>::arity

namespace upcxx {

template<int N> class domain;
template<int N> class point_iter;

template<int N> class rectdomain {
 public:
  rectdomain();
  rectdomain(const point<N> &set_p0, const point<N> &set_p1);
  rectdomain(const point<N> &set_p0, const point<N> &set_p1,
             const point<N> &set_stride);
  rectdomain(const rectdomain<N> &set_rd);
  rectdomain(domain<N> &d);
  rectdomain(const domain<N> &d);
#ifdef UPCXXA_USE_CXX11
  rectdomain(domain<N> &&d);
# ifdef UPCXXA_USE_CXX11_INITIALIZER_LIST
  rectdomain(const std::initializer_list< point<N> > &ilist);
# endif
  // These constructors are only valid for N == 1; constructor
  // delegation used here
  template<class T> explicit
  rectdomain(T s,
             typename
             domain_utils::enable_if<domain_utils::int_and_eq<T, N,
             1>::value, domain_utils::internal_type>::type =
             domain_utils::internal_type()) :
    rectdomain(point<1>{{0}},
               point<1>{{s}}) {}
  template<class T>
  rectdomain(T set_p0, cint_t set_p1, cint_t set_stride=1,
             typename
             domain_utils::enable_if<domain_utils::int_and_eq<T, N,
             1>::value, domain_utils::internal_type>::type =
             domain_utils::internal_type()) :
    rectdomain(point<1>{{set_p0}},
               point<1>{{set_p1}},
               point<1>{{set_stride}}) {}
#else /* UPCXXA_USE_CXX11 */
  // These constructors are only valid for N == 1
  template<class T> explicit
  rectdomain(T s,
             typename
             domain_utils::enable_if<domain_utils::int_and_eq<T, N,
             1>::value, domain_utils::internal_type>::type =
             domain_utils::internal_type()) {
    point<1> lwb = {{0}};
    point<1> upb = {{s}};
    *this = rectdomain(lwb, upb);
  }
  template<class T>
  rectdomain(T set_p0, cint_t set_p1, cint_t set_stride=1,
             typename
             domain_utils::enable_if<domain_utils::int_and_eq<T, N,
             1>::value, domain_utils::internal_type>::type =
             domain_utils::internal_type()) {
    point<1> p0 = {{set_p0}};
    point<1> p1 = {{set_p1}};
    point<1> loop_stride = {{set_stride}};
    *this = rectdomain<N>(p0, p1, loop_stride);
  }
#endif /* UPCXXA_USE_CXX11 */

  bool is_rectangular() const;

  domain<N> operator+(const rectdomain &d) const;
  domain<N> operator-(const rectdomain &d) const;
  rectdomain operator*(const rectdomain &d) const;
  rectdomain &operator*=(const rectdomain &d);

  rectdomain operator+(const point<N> &p) const;
  rectdomain &operator+=(const point<N> &p);
  rectdomain operator-(const point<N> &p) const;
  rectdomain &operator-=(const point<N> &p);
  rectdomain operator*(const point<N> &p) const;
  rectdomain &operator*=(const point<N> &p);
  rectdomain operator/(const point<N> &p) const;
  rectdomain &operator/=(const point<N> &p);

  bool operator==(const rectdomain &d) const;
  bool operator!=(const rectdomain &d) const;
  bool operator<(const rectdomain &d) const;
  bool operator<=(const rectdomain &d) const;
  bool operator>(const rectdomain &d) const;
  bool operator>=(const rectdomain &d) const;

  domain<N> operator+(const domain<N> &d) const;
  domain<N> operator-(const domain<N> &d) const;
  domain<N> operator*(const domain<N> &d) const;

  bool operator==(const domain<N> &d) const;
  bool operator!=(const domain<N> &d) const;
  bool operator<(const domain<N> &d) const;
  bool operator<=(const domain<N> &d) const;
  bool operator>(const domain<N> &d) const;
  bool operator>=(const domain<N> &d) const;

  bool equals(const rectdomain<N> &d) const;
  bool equals(const domain<N> &d) const;

  // Shape
  rectdomain permute(const point<N> &p) const;
  rectdomain translate(const point<N> &p) const;

  // Shape information
  enum { arity = N };

  point<N> lwb() const;
  point<N> upb() const;
  point<N> min() const;
  point<N> max() const;
  point<N> stride() const;
  size_t size() const;
  bool is_empty() const;
  bool is_not_empty() const;
  bool is_adr() const;
  rectdomain accrete(cint_t k, int dir, cint_t s) const;
  rectdomain accrete(cint_t k, int dir) const; // s = 1
  rectdomain accrete(cint_t k, const point<N> &p) const;
  rectdomain accrete(cint_t k) const; // S = all(1)
  rectdomain shrink(cint_t k, int dir) const;
  rectdomain shrink(cint_t k) const;
  rectdomain border(cint_t k, int dir, cint_t shift) const;
  rectdomain border(cint_t k, int dir) const;
  rectdomain border(int dir) const;
  bool contains(const point<N> &p) const;
  rectdomain bounding_box() const;
  int get_num_side_points(int dir) const;

  rectdomain<UPCXXA_SLICE_ARITY> slice(int k) const;

  // undocumented internal methods
  size_t index(const point<N> &p) const;
  point_iter<N> iter() const { return point_iter<N>(*this); }
  point<N> raw_stride() const;
  rectdomain<UPCXXA_SLICE_ARITY> slice_internal(int k) const;

  // Print a string representation to the given stream.
  friend std::ostream& operator<<(std::ostream& os, const rectdomain& rd) {
    if (rd.is_empty())
      os << '(' << point<N>::all(0) << ',' << point<N>::all(0) << ','
         << point<N>::all(1) << ')';
    else
      os << '(' << rd.lwb() << ',' << rd.upb() << ',' << rd.stride() << ')';
    return os;
  }

  template<int N2> friend class rectdomain;
  friend class domain<N>;
  friend class point_iter<N>;

 private:
  // These two constructors are dangerous and should not be called by
  // the user
  rectdomain(const point<N> &set_p0, const point<N> &set_p1,
             float fUnused);
  rectdomain(const point<N> &set_p0, const point<N> &set_p1,
             const point<N> &set_stride, float fUnused);

  void normalize();
  rectdomain runion(const rectdomain &rd, bool &success) const;
  rectdomain unit_stride_intersect(const rectdomain<N> &d) const;
  rectdomain resize(cint_t k, int dir, cint_t s) const;
  point<N> get_stride() const;
  domain<N> promote() const;

  point<N> p0, p1, loop_stride;
};

template<> class rectdomain<0> {};

// Rectdomain construction functions
template<int N>
inline rectdomain<N> RD(const point<N> &set_p0,
                        const point<N> &set_p1) {
  return rectdomain<N>(set_p0, set_p1);
}

template<int N>
inline rectdomain<N> RD(const point<N> &set_p0,
                        const point<N> &set_p1,
                        const point<N> &set_stride) {
  return rectdomain<N>(set_p0, set_p1, set_stride);
}

template<int N>
inline rectdomain<N> RD(const rectdomain<N> &set_rd) {
  return rectdomain<N>(set_rd);
}

template<int N>
inline rectdomain<N> RD(domain<N> &d) {
  return rectdomain<N>(d);
}

template<int N>
inline rectdomain<N> RD(const domain<N> &d) {
  return rectdomain<N>(d);
}

#ifdef UPCXXA_USE_CXX11
template<int N>
inline rectdomain<N> RD(domain<N> &&d) {
  return rectdomain<N>(d);
}

#ifdef UPCXXA_USE_CXX11_INITIALIZER_LIST
template<int N>
inline rectdomain<N> RD(const std::initializer_list< point<N> > &ilist) {
  return rectdomain<N>(ilist);
}
# endif
#endif /* UPCXXA_USE_CXX11 */

static inline rectdomain<1> RD(cint_t s) {
  return rectdomain<1>(s);
}

static inline rectdomain<1> RD(cint_t set_p0, cint_t set_p1,
                               cint_t set_stride=1) {
  return rectdomain<1>(set_p0, set_p1, set_stride);
}

} // namespace upcxx

#include "rdomain.tpp"
