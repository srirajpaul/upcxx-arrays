#include <cassert>
#include "utils.h"

#define UPCXXA_OP_SKELETON(op, sighead, args, header, action, retval)   \
  template<int N> inline sighead operator op args {                     \
    header                                                              \
    for (int i = 0; i < N; i++)                                         \
      action;                                                           \
    return retval;                                                      \
  }

#define UPCXXA_POP_SKELETON(op, rettype, args, header, action, retval)  \
  UPCXXA_OP_SKELETON(op, rettype point<N>::, args, header, action,      \
                     retval)

#define UPCXXA_ARITH_OP(op)                                             \
  UPCXXA_POP_SKELETON(op, point<N>, (const point<N> &p) const,          \
                      point<N> result;, result.x[i] = x[i] op p.x[i],   \
                      result)                                           \
  UPCXXA_POP_SKELETON(op, point<N>, (cint_t y) const,                   \
                      point<N> result;, result.x[i] = x[i] op y,        \
                      result)                                           \
  UPCXXA_POP_SKELETON(op##=, point<N> &, (const point<N> &p),           \
                      ;, x[i] = x[i] op p.x[i], *this)                  \
  UPCXXA_POP_SKELETON(op##=, point<N> &, (cint_t y),                    \
                      ;, x[i] = x[i] op y, *this)

#define UPCXXA_ARITH_OP_FUNC(op, func)                                  \
  UPCXXA_POP_SKELETON(op, point<N>, (const point<N> &p) const,          \
                      point<N> result;,                                 \
                      result.x[i] = func(x[i], p.x[i]), result)         \
  UPCXXA_POP_SKELETON(op, point<N>, (cint_t y) const,                   \
                      point<N> result;,                                 \
                      result.x[i] = func(x[i], y), result)              \
  UPCXXA_POP_SKELETON(op##=, point<N> &, (const point<N> &p),           \
                      ;, x[i] = func(x[i], p.x[i]), *this)              \
  UPCXXA_POP_SKELETON(op##=, point<N> &, (cint_t y),                    \
                      ;, x[i] = func(x[i], y), *this)

#define UPCXXA_ARITH_OP_RHS(op)                                         \
  UPCXXA_OP_SKELETON(op, point<N>, (cint_t y, const point<N> &p),       \
                     point<N> result;, result.x[i] = y op p.x[i],       \
                     result)

#define UPCXXA_ARITH_OP_RHS_FUNC(op, func)                              \
  UPCXXA_OP_SKELETON(op, point<N>, (cint_t y, const point<N> &p),       \
                     point<N> result;, result.x[i] = func(y, p.x[i]),   \
                     result)

#define UPCXXA_BIT_OP(op)                                               \
  UPCXXA_POP_SKELETON(op, point<N>, (cint_t y) const,                   \
                      point<N> result;, result.x[i] = x[i] op y,        \
                      result)

#define UPCXXA_COMP_OP(op, start, combiner)                             \
  UPCXXA_POP_SKELETON(op, bool, (const point<N> &p) const,              \
                      bool result = start;,                             \
                      result = result combiner (x[i] op p.x[i]),        \
                      result)

#if UPCXXA_BOUNDS_CHECKING
# define UPCXXA_CHECK_POINT_BOUND(loc, p, q, N)                         \
  do {                                                                  \
    UPCXXA_if_pf (q < 1 || q > N) {                                     \
      std::cerr << "Point bounds violation on rank " <<                 \
        UPCXXA_DMYPROC << "\nbounds checking error at " << loc <<       \
        ":\n  failed condition: 1 <= " << q << " <= " << N <<           \
        std::endl;                                                      \
      abort();                                                          \
      exit(-2);                                                         \
    }                                                                   \
  } while (0)
#else
# define UPCXXA_CHECK_POINT_BOUND(loc, p, q, N)
#endif

namespace upcxx {

template<int N> inline point<N> point<N>::all(cint_t x) {
  point<N> result;
  for (int i = 0; i < N; i++)
    result.x[i] = x;
  return result;
}

template<int N> inline point<N> point<N>::direction(int k, cint_t x) {
  point<N> result = point<N>::all(0);
  result.x[domain_utils::direction_value(k)] = x * domain_utils::sign(k);
  return result;
}

template<int N> inline point<N> point<N>::operator-() const {
  point<N> result;
  for (int i = 0; i < N; i++)
    result.x[i] = -x[i];
  return result;
}

template<int N> inline bool point<N>::compare(const point<N> &p1,
                                              const point<N> &p2) {
  for (int i = 0; i < N; i++)
    if (p1.x[i] < p2.x[i])
      return true;
    else if (p1.x[i] != p2.x[i])
      return false;
  return false; // need to provide strict < ordering
}

template<int N> inline point<N> point<N>::permute(const point<N> &p) const {
  UPCXXA_CHECK_PERMUTE(p, *this);
  point<N> result;
  for (int i = 0; i < N; i++)
    result.x[i] = x[p.x[i]-1];
  return result;
}

template<int N> inline cint_t point<N>::operator[](int index) const {
  UPCXXA_CHECK_POINT_BOUND("operator[]", *this, index, N);
  return x[index-1];
}

template<int N> inline point<N> point<N>::lower_bound(const point<N> &p) const {
  point<N> result;
  for (int i = 0; i < N; i++)
    result.x[i] = domain_utils::min(x[i], p.x[i]);
  return result;
}

template<int N> inline point<N> point<N>::upper_bound(const point<N> &p) const {
  point<N> result;
  for (int i = 0; i < N; i++)
    result.x[i] = domain_utils::max(x[i], p.x[i]);
  return result;
}

template<int N> inline point<N> point<N>::replace(int index, cint_t value) const {
  UPCXXA_CHECK_POINT_BOUND("operator[]", *this, index, N);
  point<N> result(*this);
  result.x[index-1] = value;
  return result;
}

// undocumented methods
template<int N> inline cint_t point<N>::get0(int index) const { // 0-based indexing!!!
  return x[index];
}

// P.set0(i,v) returns new point NP with NP[i+1] == v, 
// and other components the same as P
template<int N> inline point<N> point<N>::set0(int index, cint_t value) const { // 0-based indexing!!!
  point<N> result = *this;
  result.x[index] = value;
  return result;
}

// P.getLcm(P2) returns new point NP with NP[i] == leastCommonMultiple(P[i], P2[i])
template<int N> inline point<N> point<N>::get_lcm(const point<N> &p) const {
  point<N> result;
  for (int i = 0; i < N; i++)
    result.x[i] = domain_utils::lcm(x[i], p.x[i]);
  return result;
}

UPCXXA_ARITH_OP(+)
UPCXXA_ARITH_OP(-)
UPCXXA_ARITH_OP(*)
UPCXXA_ARITH_OP_FUNC(/, domain_utils::div_round_to_minus_inf)

UPCXXA_ARITH_OP_RHS(+)
UPCXXA_ARITH_OP_RHS(-)
UPCXXA_ARITH_OP_RHS(*)
UPCXXA_ARITH_OP_RHS_FUNC(/, domain_utils::div_round_to_minus_inf)

UPCXXA_BIT_OP(<<)
UPCXXA_BIT_OP(>>)

UPCXXA_COMP_OP(==, true, &&)
UPCXXA_COMP_OP(!=, false, ||)
UPCXXA_COMP_OP(<, true, &&)
UPCXXA_COMP_OP(<=, true, &&)
UPCXXA_COMP_OP(>, true, &&)
UPCXXA_COMP_OP(>=, true, &&)

} // namespace upcxx
